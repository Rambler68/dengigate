package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/stackend/dengigate/dengi2"
	"golang.org/x/sys/windows/svc"
)

var (
	dengi dengi2.Dengi2 = dengi2.Dengi2{}
	//		Mfo1c: mfo1c.Mfo1c{},
	//	}
	//	mfo mfo1c.Mfo1c = mfo1c.Mfo1c{}
	app *cli.App =	&cli.App{
		Name:  "dengigate",
		Usage: "dengi integration 1c",	
	}
)


type myservice struct{}

func (m *myservice) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (ssec bool, errno uint32) {
	const cmdsAccepted = svc.AcceptStop | svc.AcceptShutdown | svc.AcceptPauseAndContinue
	changes <- svc.Status{State: svc.StartPending}
	fasttick := time.Tick(500 * time.Millisecond)
	slowtick := time.Tick(2 * time.Second)
	tick := fasttick
	changes <- svc.Status{State: svc.Running, Accepts: cmdsAccepted}
loop:
	for {
		select {
		case <-tick:
//			beep()
//			elog.Info(1, "beep")
		case c := <-r:
			switch c.Cmd {
			case svc.Interrogate:
				changes <- c.CurrentStatus
				// Testing deadlock from https://code.google.com/p/winsvc/issues/detail?id=4
				time.Sleep(100 * time.Millisecond)
				changes <- c.CurrentStatus
			case svc.Stop, svc.Shutdown:
				// golang.org/x/sys/windows/svc.TestExample is verifying this output.
				testOutput := strings.Join(args, "-")
				testOutput += fmt.Sprintf("-%d", c.Context)
//				elog.Info(1, testOutput)
				break loop
			case svc.Pause:
				changes <- svc.Status{State: svc.Paused, Accepts: cmdsAccepted}
				tick = slowtick
			case svc.Continue:
				changes <- svc.Status{State: svc.Running, Accepts: cmdsAccepted}
				tick = fasttick
			default:
//				elog.Error(1, fmt.Sprintf("unexpected control request #%d", c))
			}
		}
	}
	changes <- svc.Status{State: svc.StopPending}
	return
}

func (m *myservice) Run() error{
	run := svc.Run
	err := run("name", m)
	return err
}

func runService(name string, isDebug bool) {
	var err error
	run := svc.Run
//	if isDebug {
		//run = debug.Run
	//}
	err = run(name, &myservice{})
	if err != nil {
		//elog.Error(1, fmt.Sprintf("%s service failed: %v", name, err))
		return
	}
//	elog.Info(1, fmt.Sprintf("%s service stopped", name))
}

func main() {

	inService, err := svc.IsWindowsService()
	if err != nil {
		log.Fatalf("failed to determine if we are running in service: %v", err)
	}
	if inService {
		m := myservice{}
		log.Infof("Service run")
		m.Run()
		//runService("svcName", false)
		return
	}
	
	err = app.Run(os.Args)
	if !Check(err) {
		os.Exit(10)
	}
}
