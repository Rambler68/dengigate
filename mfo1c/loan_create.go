package mfo1c

import (
	"fmt"
	"net/http"
)

/*
В 1С создаются объекты: документ Займ предоставленный, документ График платежей,
элемент справочника Договоры контрагентов и если используется общий счет расчетов Договор общих расчетов,
Документ открытия счетов аналитического учета, элементы справочника Счета аналитического учета
*/

type Проценты struct {
	ДатаПлатежа          string  `json:"ДатаПлатежа,omitempty"`
	КонецПериода         string  `json:"КонецПериода,omitempty"`
	КонецПериодаРасчета  string  `json:"КонецПериодаРасчета,omitempty"`
	НачалоПериода        string  `json:"НачалоПериода,omitempty"`
	НачалоПериодаРасчета string  `json:"НачалоПериодаРасчета,omitempty"`
	Сумма                float64 `json:"Сумма"`
}

type ПрочиеНачисления struct {
	ВидНачисления        string  `json:"ВидНачисления,omitempty"`
	ДатаПлатежа          string  `json:"ДатаПлатежа,omitempty"`
	КонецПериода         string  `json:"КонецПериода,omitempty"`
	КонецПериодаРасчета  string  `json:"КонецПериодаРасчета,omitempty"`
	НачалоПериода        string  `json:"НачалоПериода,omitempty"`
	НачалоПериодаРасчета string  `json:"НачалоПериодаРасчета,omitempty"`
	Сумма                float64 `json:"Сумма,omitempty"`
}

type ОсновнойДолг struct {
	ДатаПлатежа              string  `json:"ДатаПлатежа,omitempty"`
	ПогашениеОсновногоДолга  float64 `json:"ПогашениеОсновногоДолга"`
	УвеличениеОсновногоДолга float64 `json:"УвеличениеОсновногоДолга,omitempty"`
}

type Займ struct {
	ГрафикПлатежей struct {
		Идентификатор             string             `json:"Идентификатор,omitempty"`
		ПолнаяСтоимостьЗайма      float64            `json:"ПолнаяСтоимостьЗайма,omitempty"`
		ЭффективнаяСтавкаПроцента float64            `json:"ЭффективнаяСтавкаПроцента,omitempty"`
		ОсновнойДолг              []ОсновнойДолг     `json:"ОсновнойДолг,omitempty"`
		Проценты                  []Проценты         `json:"Проценты,omitempty"`
		ПрочиеНачисления          []ПрочиеНачисления `json:"ПрочиеНачисления,omitempty"`
	} `json:"ГрафикПлатежей,omitempty"`
	Идентификатор string `json:"Идентификатор,omitempty"`
	Реквизиты     struct {
		БанковскийСчетКонтрагента                  string  `json:"БанковскийСчетКонтрагента,omitempty"`
		ДатаВыдачи                                 string  `json:"ДатаВыдачи,omitempty"`
		ДатаДоговора                               string  `json:"ДатаДоговора,omitempty"`
		ДатаОкончания                              string  `json:"ДатаОкончания,omitempty"`
		Заявка                                     string  `json:"Заявка,omitempty"`
		Контрагент                                 string  `json:"Контрагент,omitempty"`
		НомерДоговора                              string  `json:"НомерДоговора,omitempty"`
		ОбеспеченныйЗаймЗалоги                     bool    `json:"ОбеспеченныйЗаймЗалоги,omitempty"`
		ОбеспеченныйЗаймПоручительство             bool    `json:"ОбеспеченныйЗаймПоручительство,omitempty"`
		Организация                                string  `json:"Организация,omitempty"`
		ПереноситьДатуВыдачиНаСледующийРабочийДень bool    `json:"ПереноситьДатуВыдачиНаСледующийРабочийДень,omitempty"`
		ПодразделениеОрганизации                   string  `json:"ПодразделениеОрганизации,omitempty"`
		ПоказательДолговойНагрузки                 float64 `json:"ПоказательДолговойНагрузки,omitempty"`
		ПроцентнаяСтавка                           float64 `json:"ПроцентнаяСтавка,omitempty"`
		РасчетНачисленийВоВнешнейПрограмме         bool    `json:"РасчетНачисленийВоВнешнейПрограмме,omitempty"`
		СрокЗайма                                  int64   `json:"СрокЗайма,omitempty"`
		СуммаЗайма                                 float64 `json:"СуммаЗайма"`
		ФинансовыйПродукт                          string  `json:"ФинансовыйПродукт,omitempty"`
	} `json:"Реквизиты,omitempty"`
}

type API_loan_create struct {
	/*	Данные []struct {
		Идентификатор string `json:"Идентификатор,omitempty"`
		Статус        int64  `json:"Статус,omitempty"`
	} `json:"Данные,omitempty"`*/
	ИдентификаторЗапроса           string   `json:"ИдентификаторЗапроса,omitempty"`
	КодРезультата                  int64    `json:"КодРезультата,omitempty"`
	ОписаниеРезультата             string   `json:"ОписаниеРезультата,omitempty"`
	КоличествоОбработанныхОбъектов int64    `json:"КоличествоОбработанныхОбъектов,omitempty"`
	ВнешниеДанные                  string   `json:"ВнешниеДанные,omitempty"`
	Ошибки                         []string `json:"Ошибки,omitempty"`
	Params                         Займ     `json:"-"`
}

func (api *API_loan_create) Method() (string, string, string, interface{}) {
	return http.MethodPost, "loan_create", "v1.0", &api.Params
}

func (api *API_loan_create) Error() error {
	if api.КодРезультата != 1 {
		return fmt.Errorf(`%d %s`, api.КодРезультата, api.ОписаниеРезультата)
	}
	return nil
}
