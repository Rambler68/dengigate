package mfo1c

import (
	"fmt"
	"net/http"
)

/*
http://localhost/UMFO/hs/nfoapi/delete_objects/v1.0/lk
{
  "Займы": [
    "c3fc55e4-f471-11eb-8538-50e549b16b6b"
  ],
  "ВыдачиЗаймов": [
    "0f5481ce-f597-11eb-8538-50e549b16b6b"
  ],
  "ПогашенияЗаймов": [
    "06с48вce-f597-11eb-8538-50e549b16b6b"
  ]

}
*/

type API_delete_objects struct {
	Данные []struct {
		Идентификатор string `json:"Идентификатор"`
		Статус        int64  `json:"Статус"`
	} `json:"Данные"`
	ИдентификаторЗапроса           string `json:"ИдентификаторЗапроса"`
	КодРезультата                  int64  `json:"КодРезультата"`
	КоличествоОбработанныхОбъектов int64  `json:"КоличествоОбработанныхОбъектов"`
	ОписаниеРезультата             string `json:"ОписаниеРезультата"`
	Params                         struct {
		Займы           []string `json:"Займы"` //loan_issue
		ВыдачиЗаймов    []string `json:"ВыдачиЗаймов"`
		ПогашенияЗаймов []string `json:"ПогашенияЗаймов"`
	} `json:"-"`
}

func (api *API_delete_objects) Method() (string, string, string, interface{}) {
	return http.MethodPost, "delete_objects", "v1.0", &api.Params
}

func (api *API_delete_objects) Error() error {
	if api.КодРезультата != 1 {
		return fmt.Errorf(`%d %s`, api.КодРезультата, api.ОписаниеРезультата)
	}
	return nil
}
