package mfo1c

import (
	"fmt"
	"net/http"
)

/*
Создается документ Выдача займов
*/

type Выдача struct {
	ВидОперации                        string  `json:"ВидОперации,omitempty"`
	ВнешниеДанные                      string  `json:"ВнешниеДанные,omitempty"`
	Дата                               string  `json:"Дата,omitempty"`
	ДатаВходящегоДокументаОплаты       string  `json:"ДатаВходящегоДокументаОплаты,omitempty"`
	Займ                               string  `json:"Займ,omitempty"`
	ИдентификаторТранзакции            string  `json:"ИдентификаторТранзакции,omitempty"`
	Контрагент                         string  `json:"Контрагент,omitempty"`
	НомерВходящегоДокументаОплаты      string  `json:"НомерВходящегоДокументаОплаты,omitempty"`
	Организация                        string  `json:"Организация,omitempty"`
	ПодразделениеОрганизации           string  `json:"ПодразделениеОрганизации,omitempty"`
	СодержаниеВходящегоДокументаОплаты string  `json:"СодержаниеВходящегоДокументаОплаты,omitempty"`
	СуммаПлатежа                       float64 `json:"СуммаПлатежа"`
	ФормаОплаты                        string  `json:"ФормаОплаты,omitempty"`
	ФормироватьВДатуВыдачиПоДоговору   bool    `json:"ФормироватьВДатуВыдачиПоДоговору,omitempty"`
}
type API_loan_issue_create struct {
	ИдентификаторЗапроса           string   `json:"ИдентификаторЗапроса,omitempty"`
	КодРезультата                  int64    `json:"КодРезультата,omitempty"`
	ОписаниеРезультата             string   `json:"ОписаниеРезультата,omitempty"`
	КоличествоОбработанныхОбъектов int64    `json:"КоличествоОбработанныхОбъектов,omitempty"`
	ВнешниеДанные                  string   `json:"ВнешниеДанные,omitempty"`
	Ошибки                         []string `json:"Ошибки,omitempty"`
	Params                         Выдача   `json:"-"`
}

func (api *API_loan_issue_create) Method() (string, string, string, interface{}) {
	return http.MethodPost, "loan_issue_create", "v1.0", &api.Params
}

func (api *API_loan_issue_create) Error() error {
	if api.КодРезультата != 1 {
		return fmt.Errorf(`%d %s`, api.КодРезультата, api.ОписаниеРезультата)
	}
	return nil
}
