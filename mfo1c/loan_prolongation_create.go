package mfo1c

import "net/http"

/*
В 1С создаются объекты: документ Займ предоставленный со статусом дополнительное соглашение, документ График платежей
*/

type ГрафикПлатежей struct {
	ВидДополнительногоСоглашения string `json:"ВидДополнительногоСоглашения,omitempty"`
	ГрафикПлатежей               string `json:"ГрафикПлатежей,omitempty"`             //не исполь
	ДатаДопСоглашения            string `json:"ДатаДопСоглашения,omitempty"`          //дата оперц пролонгации
	ДатаНачала                   string `json:"ДатаНачала,omitempty"`                 //не исп
	ДатаОкончания                string `json:"ДатаОкончания,omitempty"`              //
	Идентификатор                string `json:"Идентификатор,omitempty"`              //ид операц. пролонгации
	НомерДоговора                string `json:"НомерДоговора,omitempty"`              //не исп
	ОсновнойЗайм                 string `json:"ОсновнойЗайм,omitempty"`               //ид займа
	ПорядковыйНомерПролонгации   int    `json:"ПорядковыйНомерПролонгации,omitempty"` //обязат
}

type API_loan_prolongation_create struct {
	РезультатЗапроса
	Данные struct {
		ЗаймыПролонгации []РезультатОперации `json:"ГрафикиПлатежей,omitempty"`
	} `json:"Данные"`
	/*	ДатаНаРассмотрении    string `json:"ДатаНаРассмотрении"`
		ДатаНовый             string `json:"ДатаНовый"`
		ДатаУтвержденОтклонен string `json:"ДатаУтвержденОтклонен"`
		ПричинаОтказа         struct {
			Идентификатор               string `json:"Идентификатор"`
			ПричинаОтказаПредставтление string `json:"ПричинаОтказаПредставтление"`
		} `json:"ПричинаОтказа"`*/
	Params struct {
		ВнешниеДанные string `json:"ВнешниеДанные"`
		ГрафикПлатежей
	} `json:"-"`
}

func (lpc *API_loan_prolongation_create) Method() (string, string, string, interface{}) {
	return http.MethodPost, "loan_prolongation_create", "v1.0", &lpc.Params
}

func (lpc *API_loan_prolongation_create) Errors(err error) map[string]string {
	Errors := map[string]string{}
	if err != nil {
		Errors["err"] = err.Error()
	}
	Errors = CollectRequestErrors(Errors, &lpc.РезультатЗапроса, "РезультатЗапроса")
	Errors = CollectResultErrors(Errors, lpc.Данные.ЗаймыПролонгации, "ЗаймыПролонгации")
	return Errors
}

func (lpc *API_loan_prolongation_create) HasData() bool {
	return false //(len(lpc.Params.Платежи) > 0)
}
