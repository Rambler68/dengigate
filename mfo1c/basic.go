package mfo1c

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
)

/*
Структура URI: {scheme}/{hostname:port}/{base-name}/hs/nfoapi/{method-name}{method-version}{node}?query-parameters.
Где scheme - http или https,
hostname - адрес хоста,
port - порт на хосте,
base-name - имя базы 1С,
method-name - имя метода из списка методов ниже,
method-version - доступная версия метода,
node - код внешней системы от которой происходит запрос (регистрируется в базе 1С при настройке, см. докумментацию по настройке API),
query-parameters - параметры запроса если используются
*/

type BasicAPI interface {
	Method() (string, string, string, interface{}) // MethodType(GET|POST), MethodName, MethodVersion, Params
	Errors(err error) map[string]string
}

type Mfo1c struct {
	INN        string
	Connection struct {
		Hostname string
		Port     int
		Username string
		Password string
		Basename string
		Node     string
	}
	LogData bool
	LogPath string
}

func (m *Mfo1c) URI(ba BasicAPI) string {
	_, mn, mv, _ := ba.Method()
	if mn == "ping" {
		//if ba.MethodName() == "ping" {
		return fmt.Sprintf(`http://%s:%d/%s/hs/nfoapi/ping`, m.Connection.Hostname, m.Connection.Port, m.Connection.Basename)
	} else {
		return fmt.Sprintf(`http://%s:%d/%s/hs/nfoapi/%s/%s/%s`, m.Connection.Hostname, m.Connection.Port, m.Connection.Basename, mn, mv, m.Connection.Node)
	}
}

func (m *Mfo1c) Ping() error {
	ping := API_Ping{}
	_, _, err := m.Execute(&ping)
	return err
}

func (m *Mfo1c) Execute(ba BasicAPI) (req []byte, res []byte, err error) {
	mt, mn, _, mp := ba.Method()
	uri := m.URI(ba)
	if m.LogData {
		req, err = json.MarshalIndent(mp, "", "\t")
	} else {
		req, err = json.Marshal(mp)
	}
	if !Check(err) {
		return
	}

	defer func() {
		errors := ba.Errors(err)
		if len(errors) > 0 {
			for k, v := range errors {
				log.Errorf("Error %s %s", k, v)
			}
			log.Errorf("Total %d Errors", len(errors))
			//errorText, _ = json.MarshalIndent(Errors, "", " ")
		}
		if m.LogData {
			f, _ := os.Create(filepath.Join(m.LogPath, fmt.Sprintf("%s %s.Response", mn, time.Now().Format("2006.01.02 15.04.05"))))
			f.Write(res)
			f.Close()
		}
	}()

	if m.LogData {
		f, _ := os.Create(filepath.Join(m.LogPath, fmt.Sprintf("%s %s.Request", mn, time.Now().Format("2006.01.02 15.04.05"))))
		f.WriteString(fmt.Sprintf("%s %s\n", mt, uri))
		/*		f.Write([]byte{'\n'})
				fmt.Fprintf(f, "%s %s\n", mt, uri)*/
		f.Write(req)
		f.Close()
	}
	client := &http.Client{}
	request, err := http.NewRequest(mt, uri, bytes.NewBuffer(req))
	if !Check(err) {
		return
	}
	request.SetBasicAuth(m.Connection.Username, m.Connection.Password)
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	if !Check(err) {
		return
	}
	defer response.Body.Close()
	res, _ = io.ReadAll(response.Body)
	if response.StatusCode != http.StatusOK {
		err = fmt.Errorf("%s\n%s\n%s\n%s", mt, uri, response.Status, string(res))
		return
	}
	if response.Header.Get("Content-Type") == "application/json" {
		//		err = json.NewDecoder(response.Body).Decode(&ba)
		err = json.Unmarshal(res, &ba)
		return
	}
	return
}

func CollectResultErrors(Errors map[string]string, Операции []РезультатОперации, Prefix string) map[string]string {
	for i := range Операции {
		switch Операции[i].Статус {
		case 1, 2:
			{
			}
		default:
			{
				Errors[fmt.Sprintf("%s_%d", Prefix, i)] = fmt.Sprintf("%d %s %s %s", Операции[i].Статус, Операции[i].ИдентификаторВладельца, Операции[i].Идентификатор, Операции[i].ОшибкиВРамкахТранзакции)
			}
		}
	}
	return Errors
}

func CollectRequestErrors(Errors map[string]string, РезультатЗапроса *РезультатЗапроса, Prefix string) map[string]string {
	if РезультатЗапроса.КодРезультата != 1 && РезультатЗапроса.КодРезультата != 2 {
		Errors[Prefix] = РезультатЗапроса.ОписаниеРезультата
	}
	for i, s := range РезультатЗапроса.Ошибки {
		Errors[fmt.Sprintf("%s_%d", Prefix, i)] = s
	}
	return Errors
}
