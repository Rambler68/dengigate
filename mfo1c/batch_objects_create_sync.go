package mfo1c

import "net/http"

type API_batch_objects_create_sync struct {
	РезультатЗапроса
	Данные struct {
		Контрагенты     []РезультатОперации `json:"Контрагенты,omitempty"`
		Займы           []РезультатОперации `json:"Займы,omitempty"`
		ЗаймыПоЗаявкам  []РезультатОперации `json:"ЗаймыПоЗаявкам,omitempty"`
		Заявки          []РезультатОперации `json:"Заявки,omitempty"`
		ВыдачиЗаймов    []РезультатОперации `json:"ВыдачиЗаймов,omitempty"`
		ГрафикиПлатежей []РезультатОперации `json:"ГрафикиПлатежей,omitempty"`
	} `json:"Данные"`
	Params struct {
		ВнешниеДанные   string           `json:"ВнешниеДанные,omitempty"`
		Контрагенты     []Контрагент     `json:"Контрагенты,omitempty"`
		Заявки          []struct{}       `json:"Заявки,omitempty"`
		ГрафикиПлатежей []ГрафикПлатежей `json:"ГрафикиПлатежей,omitempty"`
		ЗаймыПоЗаявкам  []struct{}       `json:"ЗаймыПоЗаявкам,omitempty"`
		Займы           []Займ           `json:"Займы,omitempty"`
		ВыдачиЗаймов    []Выдача         `json:"ВыдачиЗаймов,omitempty"`
		Параметры       struct {
			РасширенныйОтвет bool `json:"РасширенныйОтвет"`
		} `json:"Параметры"`
	} `json:"-"`
}

func (bocs *API_batch_objects_create_sync) Method() (string, string, string, interface{}) {
	return http.MethodPost, "batch_objects_create_sync", "v1.0", &bocs.Params
}

func (bocs *API_batch_objects_create_sync) Errors(err error) map[string]string {
	Errors := map[string]string{}
	if err != nil {
		Errors["err"] = err.Error()
	}
	Errors = CollectRequestErrors(Errors, &bocs.РезультатЗапроса, "РезультатЗапроса")
	Errors = CollectResultErrors(Errors, bocs.Данные.Контрагенты, "Контрагенты")
	Errors = CollectResultErrors(Errors, bocs.Данные.Займы, "Займы")
	Errors = CollectResultErrors(Errors, bocs.Данные.ВыдачиЗаймов, "ВыдачиЗаймов")
	Errors = CollectResultErrors(Errors, bocs.Данные.ГрафикиПлатежей, "ГрафикиПлатежей")

	return Errors
}

func (bocs *API_batch_objects_create_sync) HasData() bool {
	return (len(bocs.Params.Контрагенты) > 0) || (len(bocs.Params.Займы) > 0) || (len(bocs.Params.ВыдачиЗаймов) > 0)
}

func (bocs *API_batch_objects_create_sync) ClearData() {
	bocs.Params.Контрагенты = nil
	bocs.Params.Займы = nil
	bocs.Params.ВыдачиЗаймов = nil
}
