package mfo1c

import "net/http"

/*
Создается 2 объекта: в справочнике Физическое лицо и в справочнике Контрагенты
*/

type БанковскиеСчета struct {
	БИКБанка   string `json:"БИКБанка,omitempty"`
	Валюта     string `json:"Валюта,omitempty"`
	НомерСчета string `json:"НомерСчета,omitempty"`
}

type Документы struct {
	ВидДокумента                             string `json:"ВидДокумента,omitempty"`
	ДатаВыдачи                               string `json:"ДатаВыдачи,omitempty"`
	КемВыдан                                 string `json:"КемВыдан,omitempty"`
	КодПодразделения                         string `json:"КодПодразделения,omitempty"`
	Номер                                    string `json:"Номер,omitempty"`
	Серия                                    string `json:"Серия,omitempty"`
	ЯвляетсяДокументомУдостоверяющимЛичность bool   `json:"ЯвляетсяДокументомУдостоверяющимЛичность,omitempty"`
}

type КонтактнаяИнформацияАдрес struct {
	Вид      string `json:"Вид,omitempty"`
	Значение struct {
		ZIPcode     string `json:"ZIPcode,omitempty"`
		AddressType string `json:"addressType,omitempty"`
		Apartments  []struct {
			Number string `json:"number,omitempty"`
			Type   string `json:"type,omitempty"`
		} `json:"apartments,omitempty"`
		Area      string `json:"area,omitempty"`
		AreaCode  string `json:"areaCode,omitempty"`
		AreaType  string `json:"areaType,omitempty"`
		Buildings []struct {
			Number string `json:"number,omitempty"`
			Type   string `json:"type,omitempty"`
		} `json:"buildings,omitempty"`
		City            string `json:"city,omitempty"`
		CityType        string `json:"cityType,omitempty"`
		Country         string `json:"country,omitempty"`
		CountryCode     string `json:"countryCode,omitempty"`
		District        string `json:"district,omitempty"`
		DistrictType    string `json:"districtType,omitempty"`
		HouseType       string `json:"houseType,omitempty"`
		IfnsULCode      string `json:"ifnsULCode,omitempty"`
		Locality        string `json:"locality,omitempty"`
		LocalityType    string `json:"localityType,omitempty"`
		MunDistrict     string `json:"munDistrict,omitempty"`
		MunDistrictType string `json:"munDistrictType,omitempty"`
		Okato           string `json:"okato,omitempty"`
		Oktmo           string `json:"oktmo,omitempty"`
		SettlementType  string `json:"settlementType,omitempty"`
		Street          string `json:"street,omitempty"`
		StreetType      string `json:"streetType,omitempty"`
		Territory       string `json:"territory,omitempty"`
		TerritoryType   string `json:"territoryType,omitempty"`
		Value           string `json:"value,omitempty"`
	} `json:"Значение,omitempty"`
}

type ИсторияФИО struct {
	ДействуетС string `json:"ДействуетС,omitempty"`
	Имя        string `json:"Имя,omitempty"`
	Отчество   string `json:"Отчество,omitempty"`
	Фамилия    string `json:"Фамилия,omitempty"`
}

type КонтактнаяИнформацияАдресВСвободнойФорме struct {
	Вид      string `json:"Вид,omitempty"`
	Значение struct {
		AddressType string `json:"addressType,omitempty"`
		Country     string `json:"country,omitempty"`
		CountryCode string `json:"countryCode,omitempty"`
		Type        string `json:"type,omitempty"`
		Value       string `json:"value,omitempty"`
	} `json:"Значение,omitempty"`
	ТипЖилья string `json:"ТипЖилья,omitempty"`
}

type КонтактнаяИнформацияТелефон struct {
	Вид      string `json:"Вид,omitempty"`
	Значение struct {
		AreaCode    string `json:"areaCode,omitempty"`
		CountryCode string `json:"countryCode,omitempty"`
		Number      string `json:"number,omitempty"`
		Type        string `json:"type,omitempty"`
		Value       string `json:"value,omitempty"`
	} `json:"Значение,omitempty"`
}

type КонтактнаяИнформацияЭлектроннаяПочта struct {
	Вид      string `json:"Вид,omitempty"`
	Значение struct {
		Type  string `json:"type,omitempty"`
		Value string `json:"value,omitempty"`
	} `json:"Значение,omitempty"`
}

type КонтактнаяИнформацияПрочее struct {
	Вид      string `json:"Вид,omitempty"`
	Значение struct {
		Value string `json:"value,omitempty"`
	} `json:"Значение,omitempty"`
}

type КонтактнаяИнформацияАдресDaData struct {
	Вид            string `json:"Вид,omitempty"`
	ЗначениеDaData struct {
		Suggestions []struct {
			Data struct {
				Area                 interface{} `json:"area,omitempty"`
				AreaFiasID           interface{} `json:"area_fias_id,omitempty"`
				AreaKladrID          interface{} `json:"area_kladr_id,omitempty"`
				AreaType             interface{} `json:"area_type,omitempty"`
				AreaTypeFull         interface{} `json:"area_type_full,omitempty"`
				AreaWithType         interface{} `json:"area_with_type,omitempty"`
				BeltwayDistance      interface{} `json:"beltway_distance,omitempty"`
				BeltwayHit           string      `json:"beltway_hit,omitempty"`
				Block                interface{} `json:"block,omitempty"`
				BlockType            interface{} `json:"block_type,omitempty"`
				BlockTypeFull        interface{} `json:"block_type_full,omitempty"`
				CapitalMarker        string      `json:"capital_marker,omitempty"`
				City                 string      `json:"city,omitempty"`
				CityArea             interface{} `json:"city_area,omitempty"`
				CityDistrict         interface{} `json:"city_district,omitempty"`
				CityDistrictFiasID   interface{} `json:"city_district_fias_id,omitempty"`
				CityDistrictKladrID  interface{} `json:"city_district_kladr_id,omitempty"`
				CityDistrictType     interface{} `json:"city_district_type,omitempty"`
				CityDistrictTypeFull interface{} `json:"city_district_type_full,omitempty"`
				CityDistrictWithType interface{} `json:"city_district_with_type,omitempty"`
				CityFiasID           string      `json:"city_fias_id,omitempty"`
				CityKladrID          string      `json:"city_kladr_id,omitempty"`
				CityType             string      `json:"city_type,omitempty"`
				CityTypeFull         string      `json:"city_type_full,omitempty"`
				CityWithType         string      `json:"city_with_type,omitempty"`
				Country              string      `json:"country,omitempty"`
				CountryIsoCode       string      `json:"country_iso_code,omitempty"`
				FederalDistrict      string      `json:"federal_district,omitempty"`
				FiasActualityState   string      `json:"fias_actuality_state,omitempty"`
				FiasCode             string      `json:"fias_code,omitempty"`
				FiasID               string      `json:"fias_id,omitempty"`
				FiasLevel            string      `json:"fias_level,omitempty"`
				Flat                 interface{} `json:"flat,omitempty"`
				FlatArea             interface{} `json:"flat_area,omitempty"`
				FlatPrice            interface{} `json:"flat_price,omitempty"`
				FlatType             interface{} `json:"flat_type,omitempty"`
				FlatTypeFull         interface{} `json:"flat_type_full,omitempty"`
				GeoLat               string      `json:"geo_lat,omitempty"`
				GeoLon               string      `json:"geo_lon,omitempty"`
				HistoryValues        interface{} `json:"history_values,omitempty"`
				House                interface{} `json:"house,omitempty"`
				HouseFiasID          interface{} `json:"house_fias_id,omitempty"`
				HouseKladrID         interface{} `json:"house_kladr_id,omitempty"`
				HouseType            interface{} `json:"house_type,omitempty"`
				HouseTypeFull        interface{} `json:"house_type_full,omitempty"`
				KladrID              string      `json:"kladr_id,omitempty"`
				Metro                interface{} `json:"metro,omitempty"`
				Okato                string      `json:"okato,omitempty"`
				Oktmo                interface{} `json:"oktmo,omitempty"`
				PostalBox            interface{} `json:"postal_box,omitempty"`
				PostalCode           string      `json:"postal_code,omitempty"`
				Qc                   interface{} `json:"qc,omitempty"`
				QcComplete           interface{} `json:"qc_complete,omitempty"`
				QcGeo                string      `json:"qc_geo,omitempty"`
				QcHouse              interface{} `json:"qc_house,omitempty"`
				Region               string      `json:"region,omitempty"`
				RegionFiasID         string      `json:"region_fias_id,omitempty"`
				RegionIsoCode        string      `json:"region_iso_code,omitempty"`
				RegionKladrID        string      `json:"region_kladr_id,omitempty"`
				RegionType           string      `json:"region_type,omitempty"`
				RegionTypeFull       string      `json:"region_type_full,omitempty"`
				RegionWithType       string      `json:"region_with_type,omitempty"`
				Settlement           interface{} `json:"settlement,omitempty"`
				SettlementFiasID     interface{} `json:"settlement_fias_id,omitempty"`
				SettlementKladrID    interface{} `json:"settlement_kladr_id,omitempty"`
				SettlementType       interface{} `json:"settlement_type,omitempty"`
				SettlementTypeFull   interface{} `json:"settlement_type_full,omitempty"`
				SettlementWithType   interface{} `json:"settlement_with_type,omitempty"`
				Source               string      `json:"source,omitempty"`
				SquareMeterPrice     interface{} `json:"square_meter_price,omitempty"`
				Street               string      `json:"street,omitempty"`
				StreetFiasID         string      `json:"street_fias_id,omitempty"`
				StreetKladrID        string      `json:"street_kladr_id,omitempty"`
				StreetType           string      `json:"street_type,omitempty"`
				StreetTypeFull       string      `json:"street_type_full,omitempty"`
				StreetWithType       string      `json:"street_with_type,omitempty"`
				TaxOffice            string      `json:"tax_office,omitempty"`
				TaxOfficeLegal       string      `json:"tax_office_legal,omitempty"`
				Timezone             string      `json:"timezone,omitempty"`
				UnparsedParts        interface{} `json:"unparsed_parts,omitempty"`
			} `json:"data,omitempty"`
			UnrestrictedValue string `json:"unrestricted_value,omitempty"`
			Value             string `json:"value,omitempty"`
		} `json:"suggestions,omitempty"`
	} `json:"ЗначениеDaData,omitempty"`
	ТипЖилья string `json:"ТипЖилья,omitempty"`
}

type КонтактныеЛица struct {
	ВидКонтактногоЛица                       string                                     `json:"ВидКонтактногоЛица,omitempty"`
	ДатаРождения                             string                                     `json:"ДатаРождения,omitempty"`
	Имя                                      string                                     `json:"Имя,omitempty"`
	КонтактнаяИнформацияАдрес                []КонтактнаяИнформацияАдрес                `json:"КонтактнаяИнформацияАдрес,omitempty"`
	КонтактнаяИнформацияАдресDaData          []КонтактнаяИнформацияАдресDaData          `json:"КонтактнаяИнформацияАдресDaData,omitempty"`
	КонтактнаяИнформацияАдресВСвободнойФорме []КонтактнаяИнформацияАдресВСвободнойФорме `json:"КонтактнаяИнформацияАдресВСвободнойФорме,omitempty"`
	КонтактнаяИнформацияПрочее               []КонтактнаяИнформацияПрочее               `json:"КонтактнаяИнформацияПрочее,omitempty"`
	КонтактнаяИнформацияТелефон              []КонтактнаяИнформацияТелефон              `json:"КонтактнаяИнформацияТелефон,omitempty"`
	КонтактнаяИнформацияЭлектроннаяПочта     []КонтактнаяИнформацияЭлектроннаяПочта     `json:"КонтактнаяИнформацияЭлектроннаяПочта,omitempty"`
	Описание                                 string                                     `json:"Описание,omitempty"`
	Отчество                                 string                                     `json:"Отчество,omitempty"`
	Роль                                     string                                     `json:"Роль,omitempty"`
	Фамилия                                  string                                     `json:"Фамилия,omitempty"`
}

type Контрагент struct {
	БанковскиеСчета                          []БанковскиеСчета                          `json:"БанковскиеСчета,omitempty"`
	Документы                                []Документы                                `json:"Документы,omitempty"`
	Идентификатор                            string                                     `json:"Идентификатор,omitempty"`
	ИсторияФИО                               []ИсторияФИО                               `json:"ИсторияФИО,omitempty"`
	КонтактнаяИнформацияАдрес                []КонтактнаяИнформацияАдрес                `json:"КонтактнаяИнформацияАдрес,omitempty"`
	КонтактнаяИнформацияАдресDaData          []КонтактнаяИнформацияАдресDaData          `json:"КонтактнаяИнформацияАдресDaData,omitempty"`
	КонтактнаяИнформацияАдресВСвободнойФорме []КонтактнаяИнформацияАдресВСвободнойФорме `json:"КонтактнаяИнформацияАдресВСвободнойФорме,omitempty"`
	КонтактнаяИнформацияТелефон              []КонтактнаяИнформацияТелефон              `json:"КонтактнаяИнформацияТелефон,omitempty"`
	КонтактнаяИнформацияЭлектроннаяПочта     []КонтактнаяИнформацияЭлектроннаяПочта     `json:"КонтактнаяИнформацияЭлектроннаяПочта,omitempty"`
	КонтактныеЛица                           []КонтактныеЛица                           `json:"КонтактныеЛица,omitempty"`
	Реквизиты                                struct {
		АдресПроживанияСовпадаетСАдресомРегистрации bool   `json:"АдресПроживанияСовпадаетСАдресомРегистрации,omitempty"`
		ВидДеятельности                             string `json:"ВидДеятельности,omitempty"`
		Гражданство                                 string `json:"Гражданство,omitempty"`
		ДатаРождения                                string `json:"ДатаРождения,omitempty"`
		Должность                                   string `json:"Должность,omitempty"`
		Занятость                                   string `json:"Занятость,omitempty"`
		Инн                                         string `json:"ИНН,omitempty"`
		Имя                                         string `json:"Имя,omitempty"`
		КоличествоДетей                             int64  `json:"КоличествоДетей,omitempty"`
		МестоРаботы                                 string `json:"МестоРаботы,omitempty"`
		МестоРождения                               string `json:"МестоРождения,omitempty"`
		НеТрудоустроен                              bool   `json:"НеТрудоустроен,omitempty"`
		ОсновноеОбразование                         string `json:"ОсновноеОбразование,omitempty"`
		Отчество                                    string `json:"Отчество,omitempty"`
		Пол                                         string `json:"Пол,omitempty"`
		СостояниеВБраке                             string `json:"СостояниеВБраке,omitempty"`
		СреднемесячныйДоход                         int64  `json:"СреднемесячныйДоход,omitempty"`
		СтажНаПоследнемМестеРаботы                  string `json:"СтажНаПоследнемМестеРаботы,omitempty"`
		СтраховойНомерПФР                           string `json:"СтраховойНомерПФР,omitempty"`
		Фамилия                                     string `json:"Фамилия,omitempty"`
		ЮридическоеФизическоеЛицо                   string `json:"ЮридическоеФизическоеЛицо,omitempty"`
		МестоРегистрации                            string `json:"МестоРегистрации,omitempty"`
	} `json:"Реквизиты,omitempty"`
}
type API_contractor_create_private_customer struct {
	/*
		КодРезультата      int64    `json:"КодРезультата,omitempty"`
		ОписаниеРезультата string   `json:"ОписаниеРезультата,omitempty"`
		ВнешниеДанные      string   `json:"ВнешниеДанные,omitempty"`
		Ошибки             []string `json:"Ошибки,omitempty"`*/
	РезультатЗапроса
	Params struct {
		ВнешниеДанные string `json:"ВнешниеДанные,omitempty"`
		Контрагент
	}
}

func (ccpc *API_contractor_create_private_customer) Method() (string, string, string, interface{}) {
	return http.MethodPost, "contractor_create_private_customer", "v1.0", &ccpc.Params
}

func (ccpc *API_contractor_create_private_customer) Errors(err error) map[string]string {
	Errors := map[string]string{}
	if err != nil {
		Errors["err"] = err.Error()
	}
	CollectRequestErrors(Errors, &ccpc.РезультатЗапроса, "РезультатЗапроса")
	return Errors
}
