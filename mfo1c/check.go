package mfo1c

import (
	"encoding/json"
	"fmt"
	"log"
	"runtime"
	"strings"
)

func Check(err error, v ...interface{}) bool {
	if err != nil {
		/*_, file, line, _ := runtime.Caller(1)
		fmt.Fprintf(os.Stderr, "%v:%v\n", file, line)
		debug.PrintStack()*/
		var s []string
		s = append(s, err.Error())
		if len(v) > 0 {
			b, _ := json.MarshalIndent(v, "", "  ")
			s = append(s, string(b))
		}
		for calldepth := 5; calldepth >= 1; calldepth-- {
			var ok bool
			var file string
			var line int
			_, file, line, ok = runtime.Caller(calldepth)
			if ok {
				s = append(s, fmt.Sprintf("%s:%d", file, line))
			}
		}
		log.Output(2, strings.Join(s[:], "\n"))
		return false //NoRows(err)
	}
	return true
}
