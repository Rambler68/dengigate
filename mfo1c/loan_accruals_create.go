package mfo1c

import "net/http"

/*
Создается документ Корректировка взаиморасчетов по займам предоставленным, который порождает начисления.
Создание начислений по займу предоставленному.
*/

type Начисление struct {
	ВидНачисления                    string  `json:"ВидНачисления,omitempty"`
	ДатаПлатежа                      string  `json:"ДатаПлатежа,omitempty"`
	Займ                             string  `json:"Займ,omitempty"`
	Контрагент                       string  `json:"Контрагент,omitempty"`
	ОтраженоВРегламентированномУчете bool    `json:"ОтраженоВРегламентированномУчете,omitempty"`
	СуммаПлатежа                     float64 `json:"СуммаПлатежа,omitempty"`
}

type API_loan_accruals_create struct {
	РезультатЗапроса
	Данные struct {
		НачисленияПоЗайму []РезультатОперации `json:"НачисленияПоЗайму,omitempty"`
	} `json:"Данные"`
	Params struct {
		ВнешниеДанные             string       `json:"ВнешниеДанные,omitempty"`
		Идентификатор             string       `json:"Идентификатор,omitempty"`
		ОсновныеНачисления        []Начисление `json:"ОсновныеНачисления,omitempty"`
		ДополнительныеНачичсления []Начисление `json:"ДополнительныеНачичсления,omitempty"`
		Реквизиты                 struct {
			ВидимостьДополнительныеНачисления bool   `json:"ВидимостьДополнительныеНачисления,omitempty"`
			ВидимостьОсновныеНачисления       bool   `json:"ВидимостьОсновныеНачисления,omitempty"`
			Дата                              string `json:"Дата,omitempty"`
			Организация                       string `json:"Организация,omitempty"`
			ОтражатьВБухгалтерскомУчете       bool   `json:"ОтражатьВБухгалтерскомУчете,omitempty"`
			ОтражатьВНалоговомУчете           bool   `json:"ОтражатьВНалоговомУчете,omitempty"`
			ОтражатьПоСпециальнымРегистрам    bool   `json:"ОтражатьПоСпециальнымРегистрам,omitempty"` //true - если требуется отражение начсилений в оперативном учете. По умолчанию true.
			ПодразделениеОрганизации          string `json:"ПодразделениеОрганизации,omitempty"`
		} `json:"Реквизиты,omitempty"`
	} `json:"-"`
}

func (lac *API_loan_accruals_create) Method() (string, string, string, interface{}) {
	return http.MethodPost, "loan_accruals_create", "v1.0", &lac.Params
}

func (lac *API_loan_accruals_create) Errors(err error) map[string]string {
	Errors := map[string]string{}
	if err != nil {
		Errors["err"] = err.Error()
	}
	Errors = CollectRequestErrors(Errors, &lac.РезультатЗапроса, "РезультатЗапроса")
	Errors = CollectResultErrors(Errors, lac.Данные.НачисленияПоЗайму, "НачисленияПоЗайму")
	return Errors
}

func (lac *API_loan_accruals_create) HasData() bool {
	return (len(lac.Params.ОсновныеНачисления) > 0) || (len(lac.Params.ДополнительныеНачичсления) > 0)
}
