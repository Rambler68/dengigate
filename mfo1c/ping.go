package mfo1c

import "net/http"

type API_Ping struct {
}

func (api *API_Ping) Method() (string, string, string, interface{}) {
	return http.MethodGet, "ping", "", nil
}

func (api *API_Ping) Errors(err error) map[string]string {
	Errors := map[string]string{}
	if err != nil {
		Errors["err"] = err.Error()
	}
	return Errors
}
