package mfo1c

import "net/http"

/*
Создание проивзольного погашения по займу предоставленному (отражение в учете).
В отличие от метода loan_repayment_create использование данного метода требует передачу распределенного платежа по видам начислений,
например, общая сумма погашения составляет 1000 руб., включает 800 руб. процентов и 200 руб. основной долга. Также при произвольном погашении не контролируются остатки!
 Создаеся документ Погашение займов с видом операции Произвольное погашение
*/

type Платеж struct {
	БанковскийСчетКонтрагента          string  `json:"БанковскийСчетКонтрагента,omitempty"`
	ВидНачисления                      string  `json:"ВидНачисления,omitempty"`
	ВидПогашения                       string  `json:"ВидПогашения,omitempty"`
	ДатаВходящегоДокументаОплаты       string  `json:"ДатаВходящегоДокументаОплаты,omitempty"`
	ДатаПлатежа                        string  `json:"ДатаПлатежа,omitempty"`
	Займ                               string  `json:"Займ,omitempty"`
	Контрагент                         string  `json:"Контрагент,omitempty"`
	НомерВходящегоДокументаОплаты      string  `json:"НомерВходящегоДокументаОплаты,omitempty"`
	СодержаниеВходящегоДокументаОплаты string  `json:"СодержаниеВходящегоДокументаОплаты,omitempty"`
	СуммаПлатежа                       float64 `json:"СуммаПлатежа,omitempty"`
	ФормаОплаты                        string  `json:"ФормаОплаты,omitempty"`
	ЗакрытьДоговор                     bool    `json:"ЗакрытьДоговор,omitempty"`
	НачислитьЗадолженность             bool    `json:"НачислитьЗадолженность,omitempty"`
	ОплатаПоЗакрытомуЗайму             bool    `json:"ОплатаПоЗакрытомуЗайму,omitempty"`
	ПоСудебномуРешению                 bool    `json:"ПоСудебномуРешению,omitempty"`
}

type API_loan_repayment_arbitrary_create struct {
	РезультатЗапроса
	Данные struct {
		ПогашенияЗаймов []РезультатОперации `json:"ПогашенияЗаймов,omitempty"`
	} `json:"Данные"`
	Params struct {
		ВнешниеДанные string `json:"ВнешниеДанные,omitempty"`
		Идентификатор string `json:"Идентификатор,omitempty"`
		Реквизиты     struct {
			Дата                     string `json:"Дата,omitempty"`
			Организация              string `json:"Организация,omitempty"`
			ПодразделениеОрганизации string `json:"ПодразделениеОрганизации,omitempty"`
		} `json:"Реквизиты"`
		Платежи []Платеж `json:"Платежи,omitempty"`
	} `json:"-"`
}

func (lrac *API_loan_repayment_arbitrary_create) Method() (string, string, string, interface{}) {
	return http.MethodPost, "loan_repayment_arbitrary_create", "v1.0", &lrac.Params
}

func (lrac *API_loan_repayment_arbitrary_create) Errors(err error) map[string]string {
	Errors := map[string]string{}
	if err != nil {
		Errors["err"] = err.Error()
	}
	Errors = CollectRequestErrors(Errors, &lrac.РезультатЗапроса, "РезультатЗапроса")
	Errors = CollectResultErrors(Errors, lrac.Данные.ПогашенияЗаймов, "ПогашенияЗаймов")
	return Errors
}

func (lrac *API_loan_repayment_arbitrary_create) HasData() bool {
	return (len(lrac.Params.Платежи) > 0)
}
