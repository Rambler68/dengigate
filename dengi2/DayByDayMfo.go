package dengi2

import (
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/stackend/dengigate/mfo1c"
)

func (d *Dengi2) GetLoanSubOrg(loan *Loan) string {
	if strings.Contains(loan.Dealer, "ОВЗ-") {
		return loan.DealerId.String()
	} else 
	if strings.Contains(loan.Dealer, "Alfa") {
		return AlfaOffice
	} else {
		return CentralOffice
	}
}

func (d *Dengi2) GetLoanProduct(loan *Loan) string {
	if (strings.Contains(loan.Dealer, "ОВЗ-") && strings.Contains(loan.KjDealer, "ОВЗ-")) {
		return "Продукт"
	} else {
		return "ПродуктОнлайн"
	}
}

func (d *Dengi2) GetCashSubOrg(cash *Cash) string {
	return cash.D_Identifier.String()
	/*	if strings.Contains(cash.D_Name, "ОВЗ-") {
			return cash.D_Identifier.String()
		} else {
			return CentralOffice
		}*/
}

func (d *Dengi2) LoanToBocs(loan *Loan, bocs *mfo1c.API_batch_objects_create_sync, o_customer, o_loan, o_issue bool) error {
	if Dbd_IgnoreLoans[loan.CjID.String()] != "" {
		return nil
	}
	//Клиент
	if o_customer {
		//Передавать только уникальных клиентов
		//if bocs.Params.Контрагенты not found (loan.Customerowner.String()) {

			Контрагент := mfo1c.Контрагент{}
			Контрагент.Идентификатор = loan.Customerowner.String()
			Контрагент.Реквизиты.Имя = loan.LastName
			Контрагент.Реквизиты.Отчество = loan.PatronymicName
			Контрагент.Реквизиты.Фамилия = loan.FirstName
			Контрагент.Реквизиты.ДатаРождения = loan.Birthday.Format(time.RFC3339)
			Контрагент.Реквизиты.ЮридическоеФизическоеЛицо = mfo1c.ВидЛицо.ФизическоеЛицо
			Контрагент.Реквизиты.Гражданство = "643"
			Контрагент.Реквизиты.МестоРегистрации = loan.AddrReg
			bocs.Params.Контрагенты = append(bocs.Params.Контрагенты, Контрагент)
		//}
	}

	//Займ
	if o_loan {
		Займ := mfo1c.Займ{}
		Займ.Идентификатор = loan.CjID.String()
		Займ.Реквизиты.Контрагент = loan.Customerowner.String() //всегда берем владельца анкеты
		Займ.Реквизиты.Организация = d.Mfo1c.INN
		Займ.Реквизиты.ПодразделениеОрганизации = d.GetLoanSubOrg(loan)
		Займ.Реквизиты.ДатаДоговора = loan.Stamp.Local().Format(time.RFC3339)
		Займ.Реквизиты.ДатаВыдачи = loan.Stamp.Format(time.RFC3339)
		//Займ.Реквизиты.ДатаВыдачи = time.Date(2020, 7, 1, 8, 0, 0, 0, time.Local).Format(time.RFC3339)
		Займ.Реквизиты.ДатаОкончания = loan.Stamp.AddDate(0, 0, int(loan.Perioddays)).Format(time.RFC3339)
		//Займ.Реквизиты.ДатаОкончания = loan.Stamp.AddDate(0, 0, -91).Format(time.RFC3339)
		Займ.Реквизиты.НомерДоговора = loan.Number
		Займ.Реквизиты.СуммаЗайма = loan.Basicvalue		
		if Займ.Реквизиты.СуммаЗайма <= 0 {
			Займ.Реквизиты.СуммаЗайма = 0.01
		}
		Займ.Реквизиты.ПроцентнаяСтавка = loan.Percent
		/*
			СрокЗайма передается как кол-во истекших дней?-потом кол-во истекших дней увеличится, что делать с займом
		*/
		ОсновнойДолг := mfo1c.ОсновнойДолг{}
		ОсновнойДолг.ДатаПлатежа = loan.Stamp.Local().Format(time.RFC3339)
		ОсновнойДолг.УвеличениеОсновногоДолга = loan.Basicvalue
		Займ.ГрафикПлатежей.ОсновнойДолг = append(Займ.ГрафикПлатежей.ОсновнойДолг, ОсновнойДолг)

		ОсновнойДолг = mfo1c.ОсновнойДолг{}
		ОсновнойДолг.ДатаПлатежа = Займ.Реквизиты.ДатаОкончания
		ОсновнойДолг.ПогашениеОсновногоДолга = loan.Basicvalue
		Займ.ГрафикПлатежей.ОсновнойДолг = append(Займ.ГрафикПлатежей.ОсновнойДолг, ОсновнойДолг)

		Проценты := mfo1c.Проценты{}
		Проценты.ДатаПлатежа = Займ.Реквизиты.ДатаОкончания
		Проценты.НачалоПериода = Проценты.ДатаПлатежа
		Проценты.НачалоПериодаРасчета = Проценты.ДатаПлатежа
		Проценты.КонецПериода = Проценты.ДатаПлатежа
		Проценты.КонецПериодаРасчета = Проценты.ДатаПлатежа
		Проценты.Сумма = loan.Basicvalue * (loan.Percent / 100 * float64(loan.Perioddays))
		Займ.ГрафикПлатежей.Проценты = append(Займ.ГрафикПлатежей.Проценты, Проценты)

		Займ.ГрафикПлатежей.Идентификатор = fmt.Sprintf("ГП_%s", loan.CjID.String())
		Займ.ГрафикПлатежей.ПолнаяСтоимостьЗайма = ОсновнойДолг.ПогашениеОсновногоДолга + Проценты.Сумма
//		!!! Надо заполнить дату ГП
		Займ.Реквизиты.СрокЗайма = loan.Perioddays
		Займ.Реквизиты.ФинансовыйПродукт = d.GetLoanProduct(loan)
		Займ.Реквизиты.РасчетНачисленийВоВнешнейПрограмме = true
		Займ.Реквизиты.ПоказательДолговойНагрузки = loan.Pdn
		//Займ.Реквизиты.ПереноситьДатуВыдачиНаСледующийРабочийДень = false
		//Займ.Реквизиты.ОбеспеченныйЗаймЗалоги = false
		Займ.Реквизиты.ОбеспеченныйЗаймПоручительство = false
		//	Займ.Реквизиты.БанковскийСчетКонтрагента = ""

		//Займ.ГрафикПлатежей.ЭффективнаяСтавкаПроцента = cd.Identifier.String()
		//Займ.ГрафикПлатежей.ПолнаяСтоимостьЗайма = cd.Identifier.String()

		bocs.Params.Займы = append(bocs.Params.Займы, Займ)
	}

	//Выдача
	if o_issue {
		TransId := fmt.Sprintf("Выдача_%s", loan.CjID.String())
		PaymentForm := fmt.Sprintf("Касса_%s", d.GetLoanSubOrg(loan))

		Выдача := mfo1c.Выдача{}
		Выдача.ВнешниеДанные = TransId
		Выдача.ИдентификаторТранзакции = TransId
		Выдача.Дата = loan.Stamp.Format(time.RFC3339)
		Выдача.ВидОперации = mfo1c.ВидОперации.ВыдачаЗайма
		Выдача.Организация = d.Mfo1c.INN
		Выдача.ПодразделениеОрганизации = d.GetLoanSubOrg(loan)
		Выдача.Контрагент = loan.Customerowner.String()
		Выдача.Займ = loan.CjID.String()
		Выдача.СуммаПлатежа = loan.Basicvalue
		Выдача.ФормаОплаты = PaymentForm
		Выдача.НомерВходящегоДокументаОплаты = loan.Number
		Выдача.ДатаВходящегоДокументаОплаты = loan.Stamp.Format(time.RFC3339)
		Выдача.СодержаниеВходящегоДокументаОплаты = fmt.Sprintf("Перевод денежных средств по договору займа №%s", loan.Number)
		Выдача.ФормироватьВДатуВыдачиПоДоговору = false
		bocs.Params.ВыдачиЗаймов = append(bocs.Params.ВыдачиЗаймов, Выдача)
		/*		case "ГрафикПлатежей":
				{
					ГрафикПлатежей := mfo1c.ГрафикПлатежей{}
					ГрафикПлатежей.ДатаНачала = loan.Stamp.Format(time.RFC3339)
					ГрафикПлатежей.ДатаОкончания = loan.Stamp.AddDate(0, 0, int(loan.Perioddays)).Format(time.RFC3339)
					ГрафикПлатежей.ОсновнойЗайм = loan.CjID.String()
					ГрафикПлатежей.НомерДоговора = loan.Number
					ГрафикПлатежей.ПорядковыйНомерПролонгации = 1
					bocs.Params.ГрафикиПлатежей = append(bocs.Params.ГрафикиПлатежей, ГрафикПлатежей)
				}
			}*/
	}
	return nil
}

func (d *Dengi2) AccrualToLac(accrual *Accrual, lac *mfo1c.API_loan_accruals_create) error {
	Начисление := mfo1c.Начисление{}
	Начисление.ВидНачисления = CreditHistoryType1C[accrual.HistoryType]
	Начисление.ДатаПлатежа = accrual.Stamp.Format(time.RFC3339)
	Начисление.Контрагент = accrual.C_Owner.String()
	Начисление.Займ = accrual.CjId.String()
	Начисление.ОтраженоВРегламентированномУчете = true
	Начисление.СуммаПлатежа = accrual.Value

	lac.Params.ОсновныеНачисления = append(lac.Params.ОсновныеНачисления, Начисление)

	lac.Params.Реквизиты.Дата = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Организация = d.Mfo1c.INN
	lac.Params.Идентификатор = Dbd_Identifier("Начисление", accrual.Stamp)
	lac.Params.Реквизиты.ОтражатьВБухгалтерскомУчете = true
	lac.Params.Реквизиты.ОтражатьВНалоговомУчете = true
	lac.Params.Реквизиты.ОтражатьПоСпециальнымРегистрам = true
	//lac.Params.Реквизиты.ПодразделениеОрганизации =

	return nil
}

func (d *Dengi2) MapAccrualToLac(accrual *Accrual, map_lac map[string]*mfo1c.API_loan_accruals_create) error {
	lac := map_lac[accrual.Bundle]
	if lac == nil {
		lac = &mfo1c.API_loan_accruals_create{}
		map_lac[accrual.Bundle] = lac
	}
	Начисление := mfo1c.Начисление{}
	Начисление.ВидНачисления = CreditHistoryType1C[accrual.HistoryType]
	Начисление.ДатаПлатежа = accrual.Stamp.Format(time.RFC3339)// !!! Дата платежа исправить на последнюю пролонгация или начало займа
	Начисление.Контрагент = accrual.C_Owner.String()
	Начисление.Займ = accrual.CjId.String()
	Начисление.ОтраженоВРегламентированномУчете = true
	Начисление.СуммаПлатежа = accrual.Value

	lac.Params.ОсновныеНачисления = append(lac.Params.ОсновныеНачисления, Начисление)

	lac.Params.Реквизиты.Дата = accrual.DocDate.Format(time.RFC3339)
	lac.Params.Реквизиты.Организация = d.Mfo1c.INN
	lac.Params.Идентификатор = accrual.Bundle
	lac.Params.Реквизиты.ОтражатьВБухгалтерскомУчете = true
	lac.Params.Реквизиты.ОтражатьВНалоговомУчете = true
	lac.Params.Реквизиты.ОтражатьПоСпециальнымРегистрам = true
	//lac.Params.Реквизиты.ПодразделениеОрганизации =

	return nil
}

func (d *Dengi2) MapAccrualToLacWOBUNDLE(accrual *Accrual, map_lac map[string]*mfo1c.API_loan_accruals_create) error {
	dt_key := accrual.Stamp.Format(d.Dbd.DateFormat)
	lac := map_lac[dt_key]
	if lac == nil {
		lac = &mfo1c.API_loan_accruals_create{}
		map_lac[dt_key] = lac
	}

	Начисление := mfo1c.Начисление{}
	Начисление.ВидНачисления = CreditHistoryType1C[accrual.HistoryType]
	Начисление.ДатаПлатежа = accrual.Stamp.Format(time.RFC3339)
	Начисление.Контрагент = accrual.C_Owner.String()
	Начисление.Займ = accrual.CjId.String()
	Начисление.ОтраженоВРегламентированномУчете = true
	Начисление.СуммаПлатежа = accrual.Value

	lac.Params.ОсновныеНачисления = append(lac.Params.ОсновныеНачисления, Начисление)

	lac.Params.Идентификатор = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Дата = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Организация = d.Mfo1c.INN
	lac.Params.Идентификатор = Dbd_Identifier("Начисление", accrual.Stamp)
	lac.Params.Реквизиты.ОтражатьВБухгалтерскомУчете = true
	lac.Params.Реквизиты.ОтражатьВНалоговомУчете = true
	lac.Params.Реквизиты.ОтражатьПоСпециальнымРегистрам = true
	//lac.Params.Реквизиты.ПодразделениеОрганизации =

	return nil
}

func (d *Dengi2) MapAccrualToLacFix5(accrual *Accrual, map_lac map[string]*mfo1c.API_loan_accruals_create) error {
	dt_key := "Начисление_Fix5"
	lac := map_lac[dt_key]
	if lac == nil {
		lac = &mfo1c.API_loan_accruals_create{}
		map_lac[dt_key] = lac
	}
	Начисление := mfo1c.Начисление{}
	Начисление.ВидНачисления = CreditHistoryType1C[accrual.HistoryType]
	Начисление.ДатаПлатежа = accrual.Stamp.Format(time.RFC3339)
	Начисление.Контрагент = accrual.C_Owner.String()
	Начисление.Займ = accrual.CjId.String()
	Начисление.ОтраженоВРегламентированномУчете = true
	Начисление.СуммаПлатежа = accrual.Value

	lac.Params.ОсновныеНачисления = append(lac.Params.ОсновныеНачисления, Начисление)

	lac.Params.Идентификатор = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Дата = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Организация = d.Mfo1c.INN
	lac.Params.Идентификатор = "Начисление_Fix5"
	lac.Params.Реквизиты.ОтражатьВБухгалтерскомУчете = true
	lac.Params.Реквизиты.ОтражатьВНалоговомУчете = true
	lac.Params.Реквизиты.ОтражатьПоСпециальнымРегистрам = true
	//lac.Params.Реквизиты.ПодразделениеОрганизации =

	return nil
}

func (d *Dengi2) MapAccrualToLac1Kop(accrual *Accrual, map_lac map[string]*mfo1c.API_loan_accruals_create) error {
	dt_key := accrual.Stamp.Format(d.Dbd.DateFormat)
	lac := map_lac[dt_key]
	if lac == nil {
		lac = &mfo1c.API_loan_accruals_create{}
		map_lac[dt_key] = lac
	}
	Начисление := mfo1c.Начисление{}
	Начисление.ВидНачисления = CreditHistoryType1C[accrual.HistoryType]
	Начисление.ДатаПлатежа = accrual.Stamp.Format(time.RFC3339)
	Начисление.Контрагент = accrual.C_Owner.String()
	Начисление.Займ = accrual.CjId.String()
	Начисление.ОтраженоВРегламентированномУчете = true
	Начисление.СуммаПлатежа = accrual.Value

	lac.Params.ОсновныеНачисления = append(lac.Params.ОсновныеНачисления, Начисление)

	lac.Params.Идентификатор = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Дата = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Организация = d.Mfo1c.INN
	lac.Params.Идентификатор = Dbd_Identifier("Начисление_1Koп", accrual.Stamp)
	lac.Params.Реквизиты.ОтражатьВБухгалтерскомУчете = true
	lac.Params.Реквизиты.ОтражатьВНалоговомУчете = true
	lac.Params.Реквизиты.ОтражатьПоСпециальнымРегистрам = true
	//lac.Params.Реквизиты.ПодразделениеОрганизации =

	return nil
}

func (d *Dengi2) ProlongToLpc(prolong *Prolong, lpc *mfo1c.API_loan_prolongation_create) error {
	lpc.Params.ВнешниеДанные = prolong.P_Identifier.String()
	lpc.Params.ДатаДопСоглашения = prolong.P_Stamp.Format(time.RFC3339)
	lpc.Params.ВидДополнительногоСоглашения = mfo1c.ВидДополнительногоСоглашения.Пролонгация
	lpc.Params.ДатаНачала = prolong.P_Stamp.Format(time.RFC3339)
	lpc.Params.ДатаОкончания = prolong.P_Stamp.AddDate(0, 0, prolong.P_Period).Format(time.RFC3339)
	lpc.Params.Идентификатор = prolong.P_Identifier.String()
	lpc.Params.ОсновнойЗайм = prolong.CJ_Identifier.String()
	lpc.Params.ПорядковыйНомерПролонгации = prolong.P_Index

	return nil
}

func (d *Dengi2) CashToLrac(cash *Cash, lrac *mfo1c.API_loan_repayment_arbitrary_create) error {
	ФормаОплаты := fmt.Sprintf("Касса_%s", d.GetCashSubOrg(cash))
	ВидНачисления := CashType1C[cash.CashType]

	if ВидНачисления != "" {
		Платеж := mfo1c.Платеж{}
		Платеж.ВидНачисления = ВидНачисления
		Платеж.ДатаПлатежа = cash.Stamp.Format(time.RFC3339)
		Платеж.Контрагент = cash.C_Owner.String()
		Платеж.Займ = cash.CjId.String()
		Платеж.СуммаПлатежа = cash.CashValue
		Платеж.ФормаОплаты = ФормаОплаты
		Платеж.ВидПогашения = mfo1c.ВидПогашения.ПлановоеПогашение
		Платеж.НомерВходящегоДокументаОплаты = cash.Number
		//		Платеж.СодержаниеВходящегоДокументаОплаты = fmt.Sprintf("Погашение по договору займа %s", cash.CJ_Number)

		/*		switch cash.CJ_DocumentStatus {
					case dengi2.DocumentStatus.Close,
					dengi2.DocumentStatus.ClosePenalty,
					dengi2.DocumentStatus.
				}
			Платеж.ЗакрытьДоговор = cash.ClosedLoan*/
		//Платеж.ПоСудебномуРешению =
		//
		/*		if ВидНачисления = ВидыНачисления.ОсновнойДолг && cash.CJ_DocumentStatus = dengi3.DocumentStatus.Close
				Платеж.ЗакрытьДоговор = cash.ClosedLoan*/

		lrac.Params.Платежи = append(lrac.Params.Платежи, Платеж)
		lrac.Params.Реквизиты.Дата = cash.Stamp.Format(time.RFC3339)
		lrac.Params.Реквизиты.Организация = d.Mfo1c.INN
		lrac.Params.Идентификатор = Dbd_Identifier("Погашение", cash.Stamp)
		return nil
	}
	alias, _ := Alias(CashType, int(cash.CashType))
	log.Warnf("ignored cash type: %d (%s)", cash.CashType, alias)
	return nil
}

func (d *Dengi2) MapCashToLrac(cash *Cash, map_lrac map[string]*mfo1c.API_loan_repayment_arbitrary_create) error {
	if Dbd_IgnoreLoans[cash.CjId.String()] != "" {
		return nil
	}

	if  strings.Contains(cash.Description.String, "!!!") {
		return nil
	}

	ПоСудебномуРешению := false
	ФормаОплаты := fmt.Sprintf("Касса_%s", d.GetCashSubOrg(cash))
	/*
	  Старые платежи на RnkoOffice сажаем на подставной офис 1С
	*/
	if cash.D_Identifier.String() == "1C3EC86C-2FB6-4308-A562-4D0F6FA0D356" {
		if apr2, _ := time.Parse(time.RFC3339, "2021-04-02T00:00:00"); cash.Stamp.Before(apr2) {
			ФормаОплаты = fmt.Sprintf("Касса_%s", "1C3EC86C-AAAA-AAAA-AAAA-AAAAAAAAAAAA")
		}
	}

	if cash.D_Identifier.String() == MKK {
		code, _, _ := d.Dbd_SearchUFK(cash.Description.String)
		if code != "" {
			//ПоСудебномуРешению = true
			ФормаОплаты = fmt.Sprintf("УФК_%s", code)
		} else {
			ФормаОплаты = "Касса_ФЛ"
		}
	}

	ВидНачисления := CashType1C[cash.CashType]

	if ВидНачисления != "" {
		dt_key := cash.Stamp.Format(d.Dbd.DateFormat)
//		lrac := map_lrac[cash.Bundle]
		lrac := map_lrac[dt_key]
		if lrac == nil {
			lrac = &mfo1c.API_loan_repayment_arbitrary_create{}
			map_lrac[dt_key] = lrac
			//map_lrac[cash.Bundle] = lrac
		}
		Платеж := mfo1c.Платеж{}
		Платеж.ВидНачисления = ВидНачисления
		Платеж.ДатаПлатежа = cash.Stamp.Format(time.RFC3339)
		Платеж.Контрагент = cash.C_Owner.String()
		Платеж.Займ = cash.CjId.String()
		Платеж.СуммаПлатежа = cash.CashValue
		Платеж.ФормаОплаты = ФормаОплаты
		Платеж.ВидПогашения = mfo1c.ВидПогашения.ПлановоеПогашение
		Платеж.НомерВходящегоДокументаОплаты = cash.Number
		Платеж.СодержаниеВходящегоДокументаОплаты = cash.Description_1c.String //fmt.Sprintf("Погашение по договору займа %s", cash.CJ_Number)
		Платеж.ЗакрытьДоговор = cash.CashType == CashType.DebetBasic &&
			((cash.CJ_DocumentStatus == DocumentStatus.Close) || (cash.CJ_DocumentStatus == DocumentStatus.ClosePenalty)) &&
			cash.CJ_SaldoBasicValue <= 0 &&
			cash.CJ_LastDebetStamp.Valid &&
			cash.CJ_LastDebetStamp.Time == cash.Stamp
		Платеж.ПоСудебномуРешению = ПоСудебномуРешению

		lrac.Params.Платежи = append(lrac.Params.Платежи, Платеж)
		lrac.Params.Реквизиты.Дата = cash.Stamp.Format(time.RFC3339)
		lrac.Params.Реквизиты.Организация = d.Mfo1c.INN
		lrac.Params.Идентификатор = Dbd_Identifier("Погашение", cash.Stamp)
//		lrac.Params.Идентификатор = cash.Bundle//Dbd_Identifier("Погашение", cash.Stamp)
		return nil
	} else {
		alias, _ := Alias(CashType, int(cash.CashType))
		if false {
			log.Warnf("ignored cash type: %d (%s)", cash.CashType, alias)
		}
		return nil
	}
}

func (d *Dengi2) LoanToLrac(loan *Loan) (lrac *mfo1c.API_loan_repayment_arbitrary_create, err error) {
	if Dbd_IgnoreLoans[loan.CjID.String()] != "" {
		return nil, nil
	}
	if loan.Basicvalue != 0.01 {
		return nil, nil
	}

	lrac = &mfo1c.API_loan_repayment_arbitrary_create{}

	Платеж := mfo1c.Платеж{}
	Платеж.ВидНачисления = mfo1c.ВидНачисления.ОсновнойДолг
	Платеж.ДатаПлатежа = loan.Stamp.Format(time.RFC3339)
	Платеж.Контрагент = loan.Customerowner.String()
	Платеж.Займ = loan.CjID.String()
	Платеж.СуммаПлатежа = loan.Basicvalue
	Платеж.ФормаОплаты = fmt.Sprintf("Касса_%s", d.GetLoanSubOrg(loan))
	Платеж.ВидПогашения = mfo1c.ВидПогашения.ПлановоеПогашение
	Платеж.НомерВходящегоДокументаОплаты = loan.Number
	//Платеж.СодержаниеВходящегоДокументаОплаты = fmt.Sprintf("Погашение по договору займа %s", cash.CJ_Number)
	Платеж.ЗакрытьДоговор = true
	Платеж.ПоСудебномуРешению = false

	lrac.Params.Платежи = append(lrac.Params.Платежи, Платеж)
	lrac.Params.Реквизиты.Дата = loan.Stamp.Format(time.RFC3339)
	lrac.Params.Реквизиты.Организация = d.Mfo1c.INN
	lrac.Params.Идентификатор = fmt.Sprintf("Погашение_%s", loan.CjID.String()) //loan.Stamp.Format(d.Dbd.DateFormat)
	return lrac, nil
}

/*
	Создание поступления на счет текущих расчетов.
	Поступление на счет текущих расчетов по договору займа.
	Создается документ "Поступление на счет текущих (прочих) расчетов по займам" с видом операции "На текущий счет по договору займа"
*/
func (d *Dengi2) MapCashToLroac(cash *Cash, map_lroac map[string]*mfo1c.API_loan_receipt_on_account_create) error {
	if cash.CashType != CashType.DebetExcessive {
		return nil
	}

	lroac := &mfo1c.API_loan_receipt_on_account_create{}
	ФормаОплаты := fmt.Sprintf("Касса_%s", d.GetCashSubOrg(cash))
	/*
	  Старые платежи на RnkoOffice сажаем на подставной офис 1С
	*/
	if cash.D_Identifier.String() == "1C3EC86C-2FB6-4308-A562-4D0F6FA0D356" {
		if apr2, _ := time.Parse(time.RFC3339, "2021-04-02T00:00:00"); cash.Stamp.Before(apr2) {
			ФормаОплаты = fmt.Sprintf("Касса_%s", "1C3EC86C-AAAA-AAAA-AAAA-AAAAAAAAAAAA")
		}
	}

	if cash.D_Identifier.String() == MKK {
		code, _, _ := d.Dbd_SearchUFK(cash.Description.String)
		if code != "" {
			ФормаОплаты = fmt.Sprintf("УФК_%s", code)
		} else {
			ФормаОплаты = "Касса_ФЛ"
		}
	}

	lroac.Params.ВнешниеДанные = cash.Identifier.String()
	lroac.Params.Дата = cash.Stamp.Format(time.RFC3339)
	lroac.Params.ДатаВходящегоДокументаОплаты = cash.Stamp.Format(time.RFC3339)
	lroac.Params.Займ = cash.CjId.String()
	lroac.Params.ИдентификаторТранзакции = fmt.Sprintf("Переплата_%s", cash.Identifier.String())
	lroac.Params.Контрагент = cash.C_Owner.String()
	lroac.Params.НомерВходящегоДокументаОплаты = cash.Number
	lroac.Params.СодержаниеВходящегоДокументаОплаты = cash.Description_1c.String
	lroac.Params.Организация = d.Mfo1c.INN
	lroac.Params.ПодразделениеОрганизации = cash.CJ_Dealer.String()
	lroac.Params.СуммаПлатежа = cash.CashValue
	lroac.Params.ФормаОплаты = ФормаОплаты

	map_lroac[cash.Identifier.String()] = lroac

	return nil
}


/*
	Создание поступления на счет текущих расчетов.
	Поступление на счет текущих расчетов по договору займа.
	Создается документ "Поступление на счет текущих (прочих) расчетов по займам" с видом операции "На текущий счет по договору займа"
*/
/*
func (d *Dengi2) MapAccrualToLroac(accrual *Accrual, map_lroac map[string]*mfo1c.API_loan_receipt_on_account_create) error {
	if accrual.HistoryType != CreditHistoryType.CreditExcessive {
		return nil
	}

	lroac := &mfo1c.API_loan_receipt_on_account_create{}
	ФормаОплаты := fmt.Sprintf("Касса_%s", d.GetCashSubOrg(cash))

	//  Старые платежи на RnkoOffice сажаем на подставной офис 1С
	if cash.D_Identifier.String() == "1C3EC86C-2FB6-4308-A562-4D0F6FA0D356" {
		if apr2, _ := time.Parse(time.RFC3339, "2021-04-02T00:00:00"); cash.Stamp.Before(apr2) {
			ФормаОплаты = fmt.Sprintf("Касса_%s", "1C3EC86C-AAAA-AAAA-AAAA-AAAAAAAAAAAA")
		}
	}

	if cash.D_Identifier.String() == MKK {
		code, _, _ := d.Dbd_SearchUFK(cash.Description.String)
		if code != "" {
			ФормаОплаты = fmt.Sprintf("УФК_%s", code)
		} else {
			ФормаОплаты = "Касса_ФЛ"
		}
	}

	lroac.Params.ВнешниеДанные = accrual.Identifier.String()
	lroac.Params.Дата = accrual.Stamp.Format(time.RFC3339)
	lroac.Params.ДатаВходящегоДокументаОплаты = accrual.Stamp.Format(time.RFC3339)
	lroac.Params.Займ = accrual.CjId.String()
	lroac.Params.ИдентификаторТранзакции = fmt.Sprintf("Переплата_%s", cash.Identifier.String())
	lroac.Params.Контрагент = accrual.C_Owner.String()
	lroac.Params.НомерВходящегоДокументаОплаты = cash.Number
	lroac.Params.СодержаниеВходящегоДокументаОплаты = cash.Description_1c.String
	lroac.Params.Организация = d.Mfo1c.INN
	lroac.Params.ПодразделениеОрганизации = cash.CJ_Dealer.String()
	lroac.Params.СуммаПлатежа = - accrual.Value
	lroac.Params.ФормаОплаты = ФормаОплаты

	map_lroac[accrual.Identifier.String()] = lroac

	return nil
}
*/