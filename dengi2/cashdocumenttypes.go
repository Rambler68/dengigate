package dengi2

var CashType = struct {
	Debet             uint8 `alias:"Приход пополнение"`
	DebetSystem       uint8 `alias:"Приход перерасчет"`
	DebetBasic        uint8 `alias:"Приход осн."`
	DebetPercent      uint8 `alias:"Приход проц."`
	DebetPenaltyOnce  uint8 `alias:"Приход штр."`
	DebetPenalty      uint8 `alias:"Приход неуст."`
	DebetService      uint8 `alias:"Приход обслуж."`
	Credit            uint8 `alias:"Расход инкассация"`
	CreditSystem      uint8 `alias:"Расход перерасчет"`
	CreditBasic       uint8 `alias:"Расход осн."`
	CreditCash        uint8 `alias:"Расход(малая касса)"`
	CreditManager     uint8 `alias:"Расход(подотчет)"`
	DebetManager      uint8 `alias:"Приход(подотчет)"`
	DebetCashless     uint8 `alias:"Приход(безналичный)"`
	CreditCashless    uint8 `alias:"Расход(безналичный)"`
	DebetShortage     uint8 `alias:"Приход (излишек)"`
	CreditShortage    uint8 `alias:"Расход(недостача)"`
	DebetExcessive    uint8 `alias:"Приход переплата"`
	DebetInsurance    uint8 `alias:"Приход (страховка)"`
	DebetPostage      uint8 `alias:"Приход (почтовые расходы)"`
	DebetDutyPrik     uint8 `alias:"Приход (госпошлина приказная)"`
	DebetDutyIsk      uint8 `alias:"Приход (госпошлина исковая)"`
	DebetDutyApel     uint8 `alias:"Приход (госпошлина апелляционная)"`
	DebetDutyKass1    uint8 `alias:"Приход (госпошлина кассационная 1)"`
	DebetDutyKass2    uint8 `alias:"Приход (госпошлина кассационная 2)"`
	DebetDutyNadz     uint8 `alias:"Приход (госпошлина надзорная)"`
	DebetPredPrik     uint8 `alias:"Приход предст.расходы(Заявление о выдаче судебного приказа)"`
	DebetPredIsk      uint8 `alias:"Приход предст.расходы(Исковое заявление)"`
	DebetPredOtzIsk1  uint8 `alias:"Приход предст.расходы(отзыв на Встречное исковое заявление)"`
	DebetPredOtzIsk2  uint8 `alias:"Приход предст.расходы(отзыв на Исковое заявление, поданное должником по месту жительства)"`
	DebetPredCompl    uint8 `alias:"Приход предст.расходы(Частная жалоба)"`
	DebetPredOtzCompl uint8 `alias:"Приход предст.расходы(отзыв на Частную жалобу)"`
	DebetPredApel     uint8 `alias:"Приход предст.расходы(Апелляционная жалоба)"`
	DebetPredOtzApel  uint8 `alias:"Приход предст.расходы(отзыв на Апелляционную жалобу)"`
	DebetPredKass     uint8 `alias:"Приход предст.расходы(Кассационная жалоба)"`
	DebetPredNadz     uint8 `alias:"Приход предст.расходы(Надзорная жалоба)"`
	DebetPredOther    uint8 `alias:"Приход предст.расходы(другое)"`
	DebetPredDover    uint8 `alias:"Приход представ.-Дов-сть"`
	DebetPredOtzKass  uint8 `alias:"Приход предст.расходы(отзыв на Кассационную жалобу)"`
}{
	Debet:             0,
	DebetSystem:       1,
	DebetBasic:        2,
	DebetPercent:      3,
	DebetPenaltyOnce:  4,
	DebetPenalty:      5,
	DebetService:      6,
	Credit:            7,
	CreditSystem:      8,
	CreditBasic:       9,
	CreditCash:        10,
	CreditManager:     11,
	DebetManager:      12,
	DebetCashless:     13,
	CreditCashless:    14,
	DebetShortage:     15,
	CreditShortage:    16,
	DebetExcessive:    17,
	DebetInsurance:    19,
	DebetPostage:      20,
	DebetDutyPrik:     35,
	DebetDutyIsk:      36,
	DebetDutyApel:     37,
	DebetDutyKass1:    38,
	DebetDutyKass2:    39,
	DebetDutyNadz:     40,
	DebetPredPrik:     41,
	DebetPredIsk:      42,
	DebetPredOtzIsk1:  43,
	DebetPredOtzIsk2:  44,
	DebetPredCompl:    45,
	DebetPredOtzCompl: 46,
	DebetPredApel:     47,
	DebetPredOtzApel:  48,
	DebetPredKass:     49,
	DebetPredNadz:     50,
	DebetPredOther:    51,
	DebetPredDover:    53,
	DebetPredOtzKass:  54,
}
