package dengi2

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/stackend/dengigate/mfo1c"
)

//exec dengi_report.dbo.Report_ReserveView_DO_v6 '{304DF4C9-6F54-400A-A190-7843B280D461}', 'reserve', 0, 1

var (
	Dbd_ReserveIdentifier2022 string = "BD174B51-6B44-440D-B736-616E39719E27"
	Dbd_MFK                   string = "47F7A021-7D4C-4A18-9705-2CA7AFAA7056"
	Dbd_OnlineOffice          string = "BD251868-EACA-483D-91F3-954548436F93"
	Dbd_TestData                     = map[string]string{}
	Dbd_RLoans                []Loan = []Loan{}	
	Dbd_IgnoreLoans  = map[string]string{
		"50B3EE98-C068-48DB-BB07-814B687BDBCD": "НЧ/001-0008",
	}
)

type Loan struct {
	AllPenaltydays        int64          `db:"ALL_PENALTYDAYS"`
	Basicvalue            float64        `db:"BASICVALUE"`
	CjID                  GUID           `db:"CJ_ID"`
	Customerid            GUID           `db:"CUSTOMERID"`
	Customerowner         GUID           `db:"CUSTOMEROWNER"`
	Dealer                string         `db:"DEALER"`
	DealerId              GUID           `db:"DEALERID"`
	KjDealer              string         `db:"KJD_DEALER"`
	KjDealerId            GUID           `db:"KJD_DEALERID"`
	Firstfixstamp         sql.NullTime   `db:"FIRSTFIXSTAMP"`
	Penaltydays           int64          `db:"PENALTYDAYS"`
	Percent               float64        `db:"percent"`
	Birthday              time.Time      `db:"birthday"`
	Debetbasicvalue       float64        `db:"debetbasicvalue"`
	Debetpercentlue       float64        `db:"debetpercentlue"`
	F                     int64          `db:"f"`
	IsProlongation        bool           `db:"ISPROLONGATION"`
	Fio                   string         `db:"fio"`
	FirstName             string         `db:"firstname"`
	LastName              string         `db:"lastname"`
	PatronymicName        string         `db:"patronymicname"`
	AddrReg               string         `db:"addrreg"`
	IsBankrupt            bool           `db:"isBankrupt"`
	Issue                 string         `db:"issue"`
	IssueName             string         `db:"issue_name"`
	LoanName              string         `db:"loan_name"`
	Number                string         `db:"number"`
	P5_2                  sql.NullString `db:"p5_2"`
	P5_3                  float64        `db:"p5_3"`
	P5_4                  float64        `db:"p5_4"`
	P5_5                  float64        `db:"p5_5"`
	P5_6                  float64        `db:"p5_6"`
	P5ID                  sql.NullInt64  `db:"p5_id"`
	P6Okato               int64          `db:"p6_okato"`
	Pdn                   float64        `db:"pdn"`
	Period                string         `db:"period"`
	Perioddays            int64          `db:"perioddays"`
	ResB1                 float64        `db:"res_b_1"`
	ResB2                 float64        `db:"res_b_2"`
	ResP1                 float64        `db:"res_p_1"`
	ResP2                 float64        `db:"res_p_2"`
	SaldoBasic            float64        `db:"saldo_basic"`
	SaldoBasic3030        float64        `db:"saldo_basic_3030"`
	SaldoBasic3030Other   float64        `db:"saldo_basic_3030_other"`
	SaldoPercent          float64        `db:"saldo_percent"`
	SaldoPercent3030      float64        `db:"saldo_percent_3030"`
	SaldoPercent3030Other float64        `db:"saldo_percent_3030_other"`
	Val1                  float64        `db:"val1"`
	Val2                  float64        `db:"val2"`
	Stamp                 time.Time      `db:"stamp"`
	Internal_Begin        time.Time      `db:"-"`
	Internal_End          time.Time      `db:"-"`
}

type Accrual struct {
	Bundle      string    `db:"BUNDLE"`
	CjId        GUID      `db:"CJ_IDENTIFIER"`
	C_Owner     GUID      `db:"C_OWNER"`
	DocDate     time.Time `db:"DOCDATE"`
	Stamp       time.Time `db:"STAMP"`
	Value       float64   `db:"VALUE"`
	HistoryType uint16    `db:"HISTORYTYPE"`
}

type Prolong struct {
	CJ_Identifier GUID      `db:"CJ_IDENTIFIER"`
	CJ_Number     string    `db:"CJ_NUMBER"`
	P_Identifier  GUID      `db:"P_IDENTIFIER"`
	P_Stamp       time.Time `db:"P_STAMP"`
	P_Period      int       `db:"P_PERIOD"`
	P_Index       int       `db:"P_INDEX"`
}

type Cash struct {
	Bundle             string         `db:"BUNDLE"`
	CjId               GUID           `db:"CJ_IDENTIFIER"`
	CJ_Number          string         `db:"CJ_NUMBER"`
	CJ_DocumentStatus  uint8         `db:"CJ_DOCUMENTSTATUS"`
	CJ_SaldoBasicValue float64        `db:"CJ_SALDOBASICVALUE"`
	CJ_LastDebetStamp  sql.NullTime   `db:"CJ_LASTDEBETSTAMP"`
	CJ_Dealer          GUID           `db:"CJ_DEALER"`
	C_Owner            GUID           `db:"C_OWNER"`
	D_Identifier       GUID           `db:"D_IDENTIFIER"`
	D_Name             string         `db:"D_NAME"`
	Identifier         GUID           `db:"IDENTIFIER"`
	Stamp              time.Time      `db:"STAMP"`
	CashValue          float64        `db:"CASHVALUE"`
	CashType           uint8          `db:"CASHTYPE"`
	Number             string         `db:"NUMBER"`
	Description        sql.NullString `db:"DESCRIPTION"`	
	Description_1c     sql.NullString `db:"DESCRIPTION_1C"`	
}

func Dbd_Identifier(Prefix string, Stamp time.Time) string {
	if len(Dbd_TestData) > 0 {
		Prefix += "_Test"
	}
	return fmt.Sprintf("%s_%s", Prefix, Stamp.Format("2.1.2006"))
}

func Dbd_WhereClause(Identifier, Stamp string, Year, Month, Day int) string {
	if len(Dbd_TestData) > 0 {
		r := ""
		for s := range Dbd_TestData {
			if r != "" {
				r += ", "
			}
			r += fmt.Sprintf("'%s'", s)
		}
		return fmt.Sprintf("%s IN (%s) AND DATEPART(YEAR, %s)>=2021", Identifier, r, Stamp)
	} else {
		return fmt.Sprintf("DATEPART(YEAR, %s)=%d AND DATEPART(MONTH, %s)=%d AND DATEPART(DAY, %s)=%d", Stamp, Year, Stamp, Month, Stamp, Day)
	}
}

func (d *Dengi2) SetupTestData(b bool) {
	if b {
		Dbd_TestData = map[string]string{
			/*			"7186596E-438D-4431-98A9-1CB711AE24A1": "ОВЗ-069/01098-2020",
															"EF672A2D-4884-45AD-9C89-B659BA80315A": "O-012/02090-2020",
															"BD634338-E1C1-4B8B-97CD-6FF35E82CA71": "O-039/01270-2020",
															"F90751F8-A91F-4EE8-8AD4-B97683F21586": "O-240/02114-2020",
															"6AD2812D-481C-45AC-AE74-2FB7C7BB7D44": "ОВЗ-003/01684-2020",
												"B1E41B10-1ADE-4832-90FE-0C54C22C613A": "ОВЗ-243/00564-2020",
												"E68B3302-B160-49BB-91EA-CAE1703D0AB4": "ОВЗ-161/01672-2020",
															"7DC4ACA8-DC34-4C4D-83E8-9BB3BEC35AA3": "O-003/01685-2020",
															"F9A52E5E-AE9E-46DC-AF1B-6C9B85775D3A": "ОВЗ-029/01678-2020",
															"4C61ED7E-0E81-4AD1-9FB6-6234110AD9E9": "ОВЗ-038/01865-2020",
															"02181256-C7C9-4755-9DEE-F900FC279520": "O-061/00157-2020",
									"B76EDB85-AB9F-4C73-A51E-F0559B08B089": "O-163/01694-2020",
																		"18F1B046-753D-4016-AD3F-51A19D643BC1": "ОВЗ-200/00762-2020",
						"F7978799-5472-4831-BF3C-1C310439A5DF": "O-075/02110-2020",
						"B76EDB85-AB9F-4C73-A51E-F0559B08B089": "O-163/01694-2020	Ельцова Валентина Александровна",*/
			"D5C3161C-BD69-42C6-8B31-A3D68AEC0557": "O-055/01578-2020	Панова Ирина Геннадьевна",

			/*"25784C39-B0BE-47AF-B0D2-18349ABC5595": "O-115/00756-2020",
			"9F40850B-3D83-44EA-A665-EDD36139AC1C": "O-084/02065-2020",
			"D5C3161C-BD69-42C6-8B31-A3D68AEC0557": "O-055/01578-2020",
			"DAAB01F7-3862-462E-8378-35B241E271F9": "ОВЗ-022/00791-2020",
			//			"07B69FB1-01EF-4D67-807A-EF70681ED751": "O-038/00401-2020",*/
		}
	} else {
		Dbd_TestData = map[string]string{}
	}
}

func (d *Dengi2) Dbd_GetRLoans() (*[]Loan, error) {
	log.Infof("Loading reserve loans from `%s`...", Dbd_ReserveIdentifier2022)
	var err error = nil
	if len(Dbd_RLoans) == 0 {
		err = d.Database.Db.Select(&Dbd_RLoans,
			fmt.Sprintf(`
			exec dengi_report.dbo.Report_ReserveView_DO_v9 '%s', 'reserve', 0, 1
		`, Dbd_ReserveIdentifier2022))
	}
	log.Infof("Loaded %d loans", len(Dbd_RLoans))
	return &Dbd_RLoans, err
}

func (d *Dengi2) Dbd_GetLoans_Old(Year, Month, Day int) ([]Loan, error) {
	Loans := []Loan{}
	if (Year < 2022) || (len(Dbd_TestData) != 0) {
		RLoans, err := d.Dbd_GetRLoans()
		if !d.Check(err) {
			return nil, err
		}
		//Исправить дата и сумма выдачи
		for _, loan := range *RLoans {
			if (len(Dbd_TestData) == 0) || (Dbd_TestData[loan.CjID.String()] != "") {
				//займ выдан 31.12.2021
				/*
					loan.Stamp = time.Date(2020, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -loan.Penaltydays)
					loan.Basicvalue = loan.SaldoBasic
				*/
				loan.Stamp = time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -int(loan.Penaltydays)-1)
				loan.Perioddays = 1

				loan.Basicvalue = loan.SaldoBasic
				Loans = append(Loans, loan)
				//какой период должен быть у просроченных займов???
				//loan.Perioddays = to-do ???
				//Loans[i].Basicvalue -= loan.Debetbasicvalue
				//Loans[i].Basicvalue = loan.SaldoBasic
			}
		}
		return Loans, nil
	}

	err := d.Database.Db.Select(&Loans, `
		SELECT 
		CJ.IDENTIFIER CJ_ID,
		CJ.STAMP stamp,
		CJ.PERIOD perioddays,
		CJ.[PERCENT] [percent],
		CJ.NUMBERPUBLIC number,
		C.OWNER CUSTOMEROWNER,
		D.NAME DEALER,
		D.IDENTIFIER DEALERID,
		CONCAT(C.INFO_NAME1, ' ', C.INFO_NAME2, ' ', C.INFO_NAME3) fio,
		'"'+C.INFO_NAME1+'"' firstname,
		C.INFO_NAME2 lastname,
		C.INFO_NAME3 patronymicname,
		C.INFO_STAMP1 birthday,
		CASE WHEN CJ.BASICVALUE>=10000 THEN 
		ISNULL((SELECT top(1) p.pdn 
				FROM dengi.dbo.eqf_PDN_log p(nolock)
				WHERE p.customer = C.IDENTIFIER
				AND CAST(p.stamp_requiest as date) <= CAST(CJ.STAMP as date)
				ORDER BY p.stamp_response desc),0)
				ELSE 0 END pdn,		   
		CJ.BASICVALUE,
		CJ.DEBETBASICVALUE debetbasicvalue,
		CJ.DEBETTOTALVALUE - CJ.DEBETBASICVALUE debetpercentlue
		FROM CREDIT_JOURNAL CJ(NOLOCK)
		INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER
		INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
		INNER JOIN DENGI.DBO.CASH_JOURNAL KJ(NOLOCK) ON CJ.IDENTIFIER=JJ.IDENTIFIER
		WHERE `+Dbd_WhereClause("CJ.IDENTIFIER", "CJ.STAMP", Year, Month, Day))

	if !d.Check(err) {
		return nil, err
	}

	return Loans, err
}

func (d *Dengi2) Dbd_GetLoans(Year, Month, Day int) ([]Loan, error) {
	Loans := []Loan{}
	if (Year < 2022) || (len(Dbd_TestData) != 0) {
		RLoans, err := d.Dbd_GetRLoans()
		if !d.Check(err) {
			return nil, err
		}
		//Исправить дата и сумма выдачи
		for _, loan := range *RLoans {
			if (len(Dbd_TestData) == 0) || (Dbd_TestData[loan.CjID.String()] != "") {
				//займ выдан 31.12.2021
				/*
					loan.Stamp = time.Date(2020, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -loan.Penaltydays)
					loan.Basicvalue = loan.SaldoBasic
				*/
				if (loan.Perioddays > 30) { 
					loan.Stamp = time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -int(loan.Penaltydays)-31)
				} else {
					loan.Stamp = time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -int(loan.Penaltydays)-1)
				}
				loan.Perioddays = 1

				loan.Basicvalue = loan.SaldoBasic
				Loans = append(Loans, loan)
				//какой период должен быть у просроченных займов???
				//loan.Perioddays = to-do ???
				//Loans[i].Basicvalue -= loan.Debetbasicvalue
				//Loans[i].Basicvalue = loan.SaldoBasic
			}
		}
		return Loans, nil
	}

	CreditHistoryKeys := []string{}
	for k, _ := range CreditHistoryType1C {
		CreditHistoryKeys = append(CreditHistoryKeys, fmt.Sprintf("%d", k))
	}

	err := d.Database.Db.Select(&Loans, `
		SELECT
			CJ.IDENTIFIER CJ_ID,
			CASE 
				WHEN CJ.STAMP<PL.pend 
				THEN '31.12.2013'
				ELSE CJ.STAMP 
			END stamp,
			CJ.PERIOD perioddays,
			CJ.[PERCENT] [percent],
			CJ.NUMBERPUBLIC number,
			C.OWNER CUSTOMEROWNER,
			D.NAME DEALER,
			D.IDENTIFIER DEALERID,
			CONCAT(C.INFO_NAME1, ' ', C.INFO_NAME2, ' ', C.INFO_NAME3) fio,
			C.INFO_NAME1 firstname,
			C.INFO_NAME2 lastname,
			C.INFO_NAME3 patronymicname,
			C.INFO_STAMP1 birthday,
			CASE 
				WHEN CJ.BASICVALUE>=10000 THEN 
					ISNULL((SELECT top(1) p.pdn 
						FROM dengi.dbo.eqf_PDN_log p(nolock)
						inner join customer ci on p.customer = ci.IDENTIFIER
						inner join customer co on co.owner=ci.owner
						WHERE co.IDENTIFIER=c.IDENTIFIER
						AND CAST(p.stamp_requiest as date) <= CAST(CJ.STAMP as date)
						ORDER BY p.stamp_response desc),
					0)
				ELSE 0 END * 100 pdn,		   

				WHEN CJ.BASICVALUE>=10000 THEN 
				SELECT PDN 	FROM [dengi].[dbo].[PDN]
				ELSE 1 ) * 100

			CASE
				WHEN CJ.STAMP<PL.pend
				THEN 0.01
				ELSE BASICVALUE
			END BASICVALUE,
			CJ.DEBETBASICVALUE debetbasicvalue,
			CJ.DEBETTOTALVALUE - CJ.DEBETBASICVALUE debetpercentlue
			FROM DENGI.DBO.CREDIT_JOURNAL CJ(NOLOCK) 
			CROSS APPLY(
				SELECT
					CH.OWNER
				FROM CREDIT_HISTORY CH(NOLOCK)
				WHERE 
					CH.HISTORYTYPE IN (`+strings.Join(CreditHistoryKeys[:], ",")+`)
					AND `+Dbd_WhereClause("CH.IDENTIFIER", "CH.STAMP", Year, Month, Day)+`
				GROUP BY CH.OWNER
			) CH
			INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER
			INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
			INNER JOIN dengi_report.dbo.DM_RESERVE_PERIOD_LIST PL(NOLOCK) ON PL.identifier='`+Dbd_ReserveIdentifier2022+`'
			LEFT JOIN dengi_report.dbo.DM_RESERVE_TABLE_LIST RL(NOLOCK) ON RL.CJ_ID=KJ.OWNER AND RL.pid=PL.identifier
			WHERE CH.OWNER=CJ.IDENTIFIER AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')`)

	if !d.Check(err) {
		return nil, err
	}

	return Loans, err
}

func (d *Dengi2) Dbd_GetLoans_Triple(Year, Month, Day int) ([]Loan, error) {
	Loans := []Loan{}
	if (Year < 2022) || (len(Dbd_TestData) != 0) {
		RLoans, err := d.Dbd_GetRLoans()
		if !d.Check(err) {
			return nil, err
		}
		//Исправить дата и сумма выдачи
		for _, loan := range *RLoans {
			if true || (loan.CjID.String() == "DED66008-8D01-4C60-BEEC-3CEAC915FC72") && ((len(Dbd_TestData) == 0) || (Dbd_TestData[loan.CjID.String()] != "")) {
					//займ выдан 31.12.2021
				/*
					loan.Stamp = time.Date(2020, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -loan.Penaltydays)
					loan.Basicvalue = loan.SaldoBasic

			Дата выдачи займа = (Дата 31.12.21) - период - срок просрочки на 31.12.21 + Если срок просрочки = 0 то + DATEDIFF( дата окончания , 31.12.21 )

				*/
				/*
				dd := 0
				if (loan.Penaltydays == 0) {
					dd = -int(loan.Perioddays)-int(loan.Penaltydays)+1
				} else{
					dd = -int(loan.Perioddays)-int(loan.Penaltydays)
				}
*/

				dd := -int(loan.Perioddays)-int(loan.Penaltydays)
				if (loan.Penaltydays == 0) {
					dur := loan.Stamp.AddDate(0, 0, int(loan.Perioddays)).Sub(time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local))
					dd += int(dur.Hours() / 24)+1
				}

				loan.Stamp = time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, dd)
				loan.Basicvalue = loan.SaldoBasic
				Loans = append(Loans, loan)
				//какой период должен быть у просроченных займов???
				//loan.Perioddays = to-do ???
				//Loans[i].Basicvalue -= loan.Debetbasicvalue
				//Loans[i].Basicvalue = loan.SaldoBasic
			}
		}
		return Loans, nil
	}

	CreditHistoryKeys := []string{}
	for k, _ := range CreditHistoryType1C {
		CreditHistoryKeys = append(CreditHistoryKeys, fmt.Sprintf("%d", k))
	}

	err := d.Database.Db.Select(&Loans, `
	SELECT
		CJ.IDENTIFIER CJ_ID,
		CAST(CJ.STAMP AS DATE) stamp,
		CJ.PERIOD perioddays,
		CJ.[PERCENT] [percent],
		CJ.NUMBERPUBLIC number,
		C.OWNER CUSTOMEROWNER,
		D.NAME DEALER,
		D.IDENTIFIER DEALERID,
		KJD.NAME KJD_DEALER,
		KJD.IDENTIFIER KJD_DEALERID,
		CONCAT(C.INFO_NAME1, ' ', C.INFO_NAME2, ' ', C.INFO_NAME3) fio,
		C.INFO_NAME1 firstname,
		C.INFO_NAME2 lastname,
		C.INFO_NAME3 patronymicname,
		C.INFO_STAMP1 birthday,
		C.INFO_ADDRESS1 addrreg,
		CASE WHEN CJ.BASICVALUE>=10000 THEN 
			ISNULL(
				(
					SELECT TOP 1 ПДН
					FROM PDN
					WHERE CreditID=CJ.IDENTIFIER
				), 1)
		ELSE 0 END * 100 pdn,
		CJ.BASICVALUE,
		CJ.DEBETBASICVALUE debetbasicvalue,
		CJ.DEBETTOTALVALUE - CJ.DEBETBASICVALUE debetpercentlue
	FROM CREDIT_JOURNAL CJ(NOLOCK)
	INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER
	INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
	INNER JOIN DENGI.DBO.CASH_JOURNAL KJ(NOLOCK) ON CJ.IDENTIFIER=KJ.IDENTIFIER
	INNER JOIN DENGI.DBO.DEALER KJD(NOLOCK) ON KJD.IDENTIFIER=KJ.DEALER
	WHERE `+Dbd_WhereClause("CJ.IDENTIFIER", "CJ.STAMP", Year, Month, Day)+`
		AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')

	UNION
	SELECT
		CJ.IDENTIFIER CJ_ID,
		'31.12.2013' stamp,
		CJ.PERIOD perioddays,
		CJ.[PERCENT] [percent],
		CJ.NUMBERPUBLIC number,
		C.OWNER CUSTOMEROWNER,
		D.NAME DEALER,
		D.IDENTIFIER DEALERID,
		KJD.NAME KJD_DEALER,
		KJD.IDENTIFIER KJD_DEALERID,
		CONCAT(C.INFO_NAME1, ' ', C.INFO_NAME2, ' ', C.INFO_NAME3) fio,
		C.INFO_NAME1 firstname,
		C.INFO_NAME2 lastname,
		C.INFO_NAME3 patronymicname,
		C.INFO_STAMP1 birthday,
		C.INFO_ADDRESS1 addrreg,
		CASE WHEN CJ.BASICVALUE>=10000 THEN 
			ISNULL(
				(
					SELECT TOP 1 ПДН
					FROM PDN
					WHERE CreditID=CJ.IDENTIFIER
				), 0)
		ELSE 0 END * 100 pdn,
		0.01 BASICVALUE,
		CJ.DEBETBASICVALUE debetbasicvalue,
		CJ.DEBETTOTALVALUE - CJ.DEBETBASICVALUE debetpercentlue
		FROM DENGI.DBO.CASH_JOURNAL KJ(NOLOCK)
		INNER JOIN DENGI.DBO.CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=KJ.OWNER
		INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER
		INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
		INNER JOIN dengi_report.dbo.DM_RESERVE_PERIOD_LIST PL(NOLOCK) ON PL.identifier='`+Dbd_ReserveIdentifier2022+`'
		INNER JOIN DENGI.DBO.DEALER KJD(NOLOCK) ON KJD.IDENTIFIER=KJ.DEALER
		LEFT JOIN dengi_report.dbo.DM_RESERVE_TABLE_LIST RL(NOLOCK) ON RL.CJ_ID=KJ.OWNER AND RL.pid=PL.identifier
		WHERE `+Dbd_WhereClause("KJ.IDENTIFIER", "KJ.STAMP", Year, Month, Day)+`
			AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
			AND CJ.STAMP<PL.pend
			AND RL.identifier IS NULL

	UNION
	SELECT DISTINCT
		CJ.IDENTIFIER CJ_ID,
		'31.12.2013' stamp,
		CJ.PERIOD perioddays,
		CJ.[PERCENT] [percent],
		CJ.NUMBERPUBLIC number,
		C.OWNER CUSTOMEROWNER,
		D.NAME DEALER,
		D.IDENTIFIER DEALERID,
		KJD.NAME KJD_DEALER,
		KJD.IDENTIFIER KJD_DEALERID,
		CONCAT(C.INFO_NAME1, ' ', C.INFO_NAME2, ' ', C.INFO_NAME3) fio,
		C.INFO_NAME1 firstname,
		C.INFO_NAME2 lastname,
		C.INFO_NAME3 patronymicname,
		C.INFO_STAMP1 birthday,
		C.INFO_ADDRESS1 addrreg,
		CASE WHEN CJ.BASICVALUE>=10000 THEN 
			ISNULL(
				(
					SELECT TOP 1 ПДН
					FROM PDN
					WHERE CreditID=CJ.IDENTIFIER
				), 0)
		ELSE 0 END * 100 pdn,
		0.01 BASICVALUE,
		CJ.DEBETBASICVALUE debetbasicvalue,
		CJ.DEBETTOTALVALUE - CJ.DEBETBASICVALUE debetpercentlue
		FROM (
			SELECT
				DISTINCT CH.OWNER
			FROM CREDIT_HISTORY CH(NOLOCK)
			WHERE 
				CH.HISTORYTYPE IN (`+strings.Join(CreditHistoryKeys[:], ",")+`)
				AND `+Dbd_WhereClause("CH.IDENTIFIER", "CH.STAMP", Year, Month, Day)+`
			--GROUP BY CH.OWNER
		) CH
		INNER JOIN DENGI.DBO.CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=CH.OWNER
		INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER
		INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
		INNER JOIN DENGI.DBO.CASH_JOURNAL KJ(NOLOCK) ON CJ.IDENTIFIER=KJ.IDENTIFIER
		INNER JOIN dengi_report.dbo.DM_RESERVE_PERIOD_LIST PL(NOLOCK) ON PL.identifier='`+Dbd_ReserveIdentifier2022+`'
		INNER JOIN DENGI.DBO.DEALER KJD(NOLOCK) ON KJD.IDENTIFIER=KJ.DEALER
		LEFT JOIN dengi_report.dbo.DM_RESERVE_TABLE_LIST RL(NOLOCK) ON RL.CJ_ID=CJ.IDENTIFIER AND RL.pid=PL.identifier
		WHERE 
			(D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
			AND CJ.STAMP<PL.pend
			AND RL.identifier IS NULL
		`)

	if !d.Check(err) {
		return nil, err
	}

	return Loans, err
}




func (d *Dengi2) Dbd_GetCashies(Year, Month, Day int) ([]Cash, error) {
	Cashies := []Cash{}
	if Year < 2022 {
		//Погашения до 31.12.2021 не загружать
		return Cashies, nil
	}
	err := d.Database.Db.Select(&Cashies, `
		SELECT 
		    CONCAT_WS('_', 'Погашение', FORMAT(KJ.STAMP, 'd.M.yyyy')) BUNDLE,
			CJ.IDENTIFIER CJ_IDENTIFIER,
			CJ.NUMBERPUBLIC CJ_NUMBER,
			CJ.DOCUMENTSTATUS CJ_DOCUMENTSTATUS,
			CJ.SALDOBASICVALUE CJ_SALDOBASICVALUE,
			CAST(CJ.LASTDEBETSTAMP AS DATE) CJ_LASTDEBETSTAMP,
			CJ.DEALER CJ_DEALER,
			C.OWNER C_OWNER,
			D.IDENTIFIER D_IDENTIFIER,
			D.NAME D_NAME,
			KJ.IDENTIFIER,
			CAST(KJ.STAMP AS DATE) STAMP,
			KJ.CASHVALUE,
			KJ.CASHTYPE,
			KJ.NUMBER,
			KJ.DESCRIPTION,
			KJ.DESCRIPTION_1C
		FROM CASH_JOURNAL KJ(NOLOCK)
		INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON KJ.DEALER=D.IDENTIFIER
		INNER JOIN DENGI.DBO.CREDIT_JOURNAL CJ(NOLOCK) ON KJ.OWNER=CJ.IDENTIFIER
		INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER		
		WHERE `+Dbd_WhereClause("KJ.OWNER", "KJ.STAMP", Year, Month, Day)+`	AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')

		UNION 

		SELECT 
		    CONCAT_WS('_', 'Погашение', FORMAT(CH.STAMP, 'd.M.yyyy')) BUNDLE,
			CJ.IDENTIFIER CJ_IDENTIFIER,
			CJ.NUMBERPUBLIC CJ_NUMBER,
			CJ.DOCUMENTSTATUS CJ_DOCUMENTSTATUS,
			CJ.SALDOBASICVALUE CJ_SALDOBASICVALUE,
			CAST(CJ.LASTDEBETSTAMP AS DATE) CJ_LASTDEBETSTAMP,
			CJ.DEALER CJ_DEALER,
			C.OWNER C_OWNER,
			D.IDENTIFIER D_IDENTIFIER,
			D.NAME D_NAME,
			CH.IDENTIFIER,
			CAST(CH.STAMP AS DATE) STAMP,
			- CH.RETURNVALUE CASHVALUE,
			CASE CH.HISTORYTYPE WHEN 97 THEN 3 ELSE 250 END CASHTYPE,
		    CONCAT_WS('_', 'Погашение', 'Возврат', CJ.IDENTIFIER, FORMAT(CH.STAMP, 'd.M.yyyy')) NUMBER,
			CH.DESCRIPTION,
			CH.DESCRIPTION DESCRIPTION_1C
		FROM CREDIT_HISTORY CH(NOLOCK)
		INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON CH.DEALER=D.IDENTIFIER
		INNER JOIN DENGI.DBO.CREDIT_JOURNAL CJ(NOLOCK) ON CH.OWNER=CJ.IDENTIFIER
		INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER		
		WHERE `+Dbd_WhereClause("KJ.OWNER", "CH.STAMP", Year, Month, Day)+`	AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
		AND NOT CH.RETURNVALUE IS NULL --OR CH.tReturnPenalty

			`)
//			AND CJ.STAMP<'31.03.2022'
//			AND CJ.DOCUMENTSTATUS IN (5,6)
// AND CJ.STAMP>'01.12.2022'
//AND CJ.STAMP<'31.03.2022'
if !d.Check(err) {
		return nil, err
	}
	return Cashies, err
}


func (d *Dengi2) Dbd_GetAccruals(Year, Month, Day int) ([]Accrual, error) {
	Accruals := []Accrual{}
	if Year < 2022 {
		RLoans, err := d.Dbd_GetRLoans()
		if !d.Check(err) {
			return nil, err
		}
		for _, loan := range *RLoans {
			if (len(Dbd_TestData) == 0) || (Dbd_TestData[loan.CjID.String()] != "") {
				/*if loan.SaldoPercent > 0 {
					Accrual := Accrual{}
					Accrual.CjId = loan.CjID
					Accrual.C_Owner = loan.Customerowner
					//Accrual.Stamp = time.Date(2020, 12, 31, 0, 0, 1, 0, time.Local)
					Accrual.Stamp = time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -int(loan.Penaltydays)-1)
					Accrual.Value = loan.SaldoPercent //задаем как сальдо начисленных (не оплаченных) процентов на 31.12.2021
					Accrual.HistoryType = CreditHistoryType.Credit
					Accruals = append(Accruals, Accrual)
				} else*/if loan.SaldoPercent <= 0 {
					Accrual := Accrual{}
					Accrual.CjId = loan.CjID
					Accrual.C_Owner = loan.Customerowner
					//Accrual.Stamp = time.Date(2020, 12, 31, 0, 0, 1, 0, time.Local)
					Accrual.Stamp = time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -int(loan.Penaltydays)-1)
					Accrual.Value = 0.01
					Accrual.HistoryType = CreditHistoryType.Credit
					Accruals = append(Accruals, Accrual)
				}
			}
		}
		return Accruals, nil
	}

	err := d.Database.Db.Select(&Accruals, `
		SELECT
  	   		CONCAT_WS('_', 'Начисление', FORMAT(CH.STAMP, 'd.M.yyyy')) BUNDLE,		
    		CH.OWNER CJ_IDENTIFIER,
			C.OWNER C_OWNER,
			CAST(CH.STAMP AS DATE) DOCDATE,
			--CJ.BEGINSTAMP,
			--DATEADD(day, -R.Penaltydays-1, '31.12.2021') RSTAMP,
			--P.STAMP PSTAMP,
			CAST(DATEADD(day, CJ.PERIOD, COALESCE(
				P.STAMP, 
				CJ.BEGINSTAMP)) AS DATE) STAMP,
			ISNULL(CH.PERPERIODVALUE, 0)
			+ ISNULL(CH.ADVANCEDUTYVALUE, 0)
			+ ISNULL(CH.ADVANCEPOSTAGEVALUE, 0)
			+ ISNULL(CH.ADVANCEREPRESENTVALUE, 0)
			- ISNULL(CH.RETURNVALUE, 0) VALUE,
			CH.HISTORYTYPE
		FROM CREDIT_HISTORY CH(NOLOCK)
		INNER JOIN CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=CH.OWNER
		INNER JOIN DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
		INNER JOIN DP_CREDITHISTORYTYPE CHT(NOLOCK) ON CH.HISTORYTYPE=CHT.ID AND CHT.NAME LIKE '%%КРЕДИТ%%'
		INNER JOIN CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER
		OUTER APPLY(
			SELECT 
				MAX(P.STAMP) STAMP
			FROM CREDIT_HISTORY P(NOLOCK) 
			WHERE P.HISTORYTYPE=9 AND P.STAMP<=CH.STAMP AND P.OWNER=CH.OWNER
		) P
		LEFT JOIN DENGI_REPORT.dbo.DM_RESERVE_TABLE_LIST R ON R.CJ_ID=CH.OWNER AND R.PID='`+Dbd_ReserveIdentifier2022+`'
		WHERE `+Dbd_WhereClause("CH.OWNER", "CH.STAMP", Year, Month, Day)+`
			AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
		ORDER BY CH.STAMP`)
	if !d.Check(err) {
		return nil, err
	}
	return Accruals, err
//	DATEADD(day, -R.Penaltydays-1, '31.12.2021'),
	//			CAST(CH.STAMP AS DATE) STAMP,
// AND CJ.STAMP>'01.12.2022'
//CAST(DATEADD(day, CJ.PERIOD, COALESCE(P.STAMP, CJ.BEGINSTAMP)) AS DATE) STAMP,
}


func (d *Dengi2) Dbd_GetActionAccruals(Year, Month, Day int) ([]Accrual, error) {
	Accruals := []Accrual{}
	err := d.Database.Db.Select(&Accruals, `
		SELECT
			CONCAT_WS('_', 'Начисление', AC.Id, AC.action, CJ.IDENTIFIER) BUNDLE,
			CH.OWNER CJ_IDENTIFIER,
			C.OWNER C_OWNER,
			CAST(CH.STAMP AS DATE) STAMP,
			ISNULL(CH.PERPERIODVALUE, 0)
			+ ISNULL(CH.ADVANCEDUTYVALUE, 0)
			+ ISNULL(CH.ADVANCEPOSTAGEVALUE, 0)
			+ ISNULL(CH.ADVANCEREPRESENTVALUE, 0) VALUE,
			CH.HISTORYTYPE
		FROM CREDIT_HISTORY CH(NOLOCK)
		INNER JOIN CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=CH.OWNER
		INNER JOIN DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
		INNER JOIN DP_CREDITHISTORYTYPE CHT(NOLOCK) ON CH.HISTORYTYPE=CHT.ID AND CHT.NAME LIKE '%%КРЕДИТ%%'
		INNER JOIN CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER		
		INNER JOIN ACTION_CREDIT AC(NOLOCK) ON AC.f_credit_journal=CJ.IDENTIFIER
		WHERE `+Dbd_WhereClause("CH.OWNER", "AC.STAMP", Year, Month, Day)+`
			AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
		ORDER BY CH.STAMP`)
	if !d.Check(err) {
		return nil, err
	}
	return Accruals, err
	// AND CJ.STAMP>'01.12.2022'
}



func (d *Dengi2) Dbd_GetRawAccruals(Year, Month, Day int) ([]Accrual, error) {
	Accruals := []Accrual{}
	err := d.Database.Db.Select(&Accruals, `
		SELECT
			CONCAT_WS('_', 'Начисление', 'Изменений', AC.Id, AC.action, CJ.IDENTIFIER) BUNDLE,
			CH.OWNER CJ_IDENTIFIER,
			C.OWNER C_OWNER,
			CAST(CH.STAMP AS DATE) STAMP,
			ISNULL(CH.PERPERIODVALUE, 0)
			+ ISNULL(CH.ADVANCEDUTYVALUE, 0)
			+ ISNULL(CH.ADVANCEPOSTAGEVALUE, 0)
			+ ISNULL(CH.ADVANCEREPRESENTVALUE, 0) VALUE,
			CH.HISTORYTYPE
		FROM CREDIT_HISTORY CH(NOLOCK)
		INNER JOIN CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=CH.OWNER
		INNER JOIN DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
		INNER JOIN DP_CREDITHISTORYTYPE CHT(NOLOCK) ON CH.HISTORYTYPE=CHT.ID AND CHT.NAME LIKE '%%КРЕДИТ%%'
		INNER JOIN CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER		
		INNER JOIN ACTION_CREDIT AC(NOLOCK) ON AC.f_credit_journal=CJ.IDENTIFIER
		WHERE `+Dbd_WhereClause("CH.OWNER", "AC.STAMP", Year, Month, Day)+`
			AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
		ORDER BY CH.STAMP`)
	if !d.Check(err) {
		return nil, err
	}
	return Accruals, err
	// AND CJ.STAMP>'01.12.2022'
}


func (d *Dengi2) Dbd_GetProlongs(Year, Month, Day int) ([]Prolong, error) {
	Prolongs := []Prolong{}
	if Year < 2022 {
		RLoans, err := d.Dbd_GetRLoans()
		if !d.Check(err) {
			return nil, err
		}
		//если были пролонгации, то добавить только одну
		for _, loan := range *RLoans {
			//			if (len(Dbd_TestData) == 0) || (Dbd_TestData[loan.CjID.String()] != "") && loan.IsProlongation {
			//if (len(Dbd_TestData) == 0) || (Dbd_TestData[loan.CjID.String()] != "") && (loan.SaldoPercent == 0) {
			if loan.IsProlongation {
				Prolong := Prolong{}
				Prolong.CJ_Identifier = loan.CjID
				Prolong.CJ_Number = loan.Number
				Prolong.P_Identifier = loan.CjID
				Prolong.P_Period = int(loan.Perioddays)
				Prolong.P_Index = 1
				Prolong.P_Stamp = time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -int(loan.Penaltydays)-1)
				Prolongs = append(Prolongs, Prolong)
			}
		}
		return Prolongs, nil
	}
	err := d.Database.Db.Select(&Prolongs, `
		SELECT 
		CH.OWNER CJ_IDENTIFIER,
		CJ.NUMBERPUBLIC CJ_NUMBER,
		CH.IDENTIFIER P_IDENTIFIER,
		CAST(CH.STAMP AS DATE) P_STAMP,
		CH.INFOPERIOD P_PERIOD,
		ROW_NUMBER() OVER (PARTITION by CH.OWNER order by CH.STAMP) + CASE WHEN R.ISPROLONGATION = 1 THEN 1 ELSE 0 END P_INDEX
		FROM CREDIT_HISTORY CH(NOLOCK)
		INNER JOIN CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=CH.OWNER 
		LEFT JOIN DENGI_REPORT.dbo.DM_RESERVE_TABLE_LIST R ON R.CJ_ID=CH.OWNER AND R.PID='`+Dbd_ReserveIdentifier2022+`'
		WHERE CH.HISTORYTYPE=9 AND `+Dbd_WhereClause("CH.OWNER", "CH.STAMP", Year, Month, Day))
	/*
		AND CJ.STAMP>'01.12.2022'

			(SELECT COUNT(*) + 1
			  FROM CREDIT_HISTORY CHP(NOLOCK)
			  WHERE CHP.OWNER=CH.OWNER AND CHP.HISTORYTYPE=9 AND CHP.STAMP<CH.STAMP) P_INDEX
	*/
	if !d.Check(err) {
		return nil, err
	}
	return Prolongs, err
}

func (d *Dengi2) Dbd_SaveResult(Method string, Year, Month, Day int, Text []byte, ResultError bool, Request, Response []byte) error {
	res, err := d.Database.Db.NamedExec(`INSERT INTO DBD_RESULT(METHOD, STAMP, ADAY, ERR, ERRTEXT, REQ, RES) VALUES (:METHOD, :STAMP, :ADAY, :ERR, :ERRTEXT, :REQ, :RES)`,
		map[string]interface{}{
			"METHOD":  Method,
			"STAMP":   time.Now(),
			"ADAY":    time.Date(Year, time.Month(Month), Day, 0, 0, 0, 0, time.Local),
			"ERR":     ResultError,
			"ERRTEXT": string(Text),
			"REQ": func() string {
				if ResultError {
					return string(Request)
				}
				return "{}"
			}(),
			"RES": func() string {
				if ResultError {
					return string(Response)
				}
				return "{}"
			}(),
		})
	if !d.Check(err) {
		return err
	}
	if v, err := res.RowsAffected(); v > 0 {
		return nil
	} else {
		return err
	}
}

func (d *Dengi2) ErrorsDescribe(Errors map[string]string) (map[string]string) {
	type ErrorDescription struct {
		KYE 	string    `db:"KYE"`
		Info	string    `db:"INFO"`
	}

	if len(Errors) == 0 {
		return Errors
	}

	Values := []string{}
	for k, v := range Errors {
		Values = append(Values, fmt.Sprintf("('%s', '%s')", k, v))
	}
	
	ErrorsD := []ErrorDescription{}
	err := d.Database.Db.Select(&ErrorsD, `
		SELECT
			KYE = I.KYE,
			INFO = CONCAT_WS(' ', C.INFO_NAME1, C.INFO_NAME2, C.INFO_NAME3, CJ.NUMBERPUBLIC)
		FROM
		(
			VALUES ` + strings.Join(Values, ",") + `
		) S(K,V)
		OUTER APPLY
		(
			SELECT 
				KYE = S.K,
				ID = TRY_CAST(VALUE AS UNIQUEIDENTIFIER)
			FROM STRING_SPLIT(S.V, ' ') 
		) I
		LEFT JOIN CREDIT_JOURNAL CJ ON CJ.IDENTIFIER=I.ID
		LEFT JOIN CUSTOMER C ON C.IDENTIFIER=COALESCE(CJ.CUSTOMER, I.ID)
		WHERE NOT C.STAMP IS NULL OR NOT CJ.STAMP IS NULL
	`)

	if !d.Check(err){
		Errors["errd"] = err.Error()
	}

	for _, ed := range ErrorsD {
		Errors[ed.KYE] = fmt.Sprintf("%s (%s)", Errors[ed.KYE], ed.Info)
	}

	return Errors
}

func (d *Dengi2) Dbd_Process(dt time.Time, obj string) (error, map[string]string) {
	 
	var err error = nil
	if obj == "*" {
		obj = "customer,loan,issue,accrual,prolongation,payment,excessive"
	}

	objs := strings.Split(obj, ",")
	o_customer, o_loan, o_issue, o_prolongation, o_accrual, o_payment, o_excessive := false, false, false, false, false, false, false
	for i, s := range objs {
		objs[i] = strings.ToLower(s)
		o_customer = o_customer || objs[i] == "customer"
		o_loan = o_loan || objs[i] == "loan"
		o_issue = o_issue || objs[i] == "issue"
		o_prolongation = o_prolongation || objs[i] == "prolongation"
		o_accrual = o_accrual || objs[i] == "accrual"
		o_payment = o_payment || objs[i] == "payment"
		o_excessive = o_excessive || objs[i] == "excessive"
	}

	log.Warnf("Start: %s, `%s`", dt.Format(time.RFC850), strings.Join(objs, ","))

	var year, month, day int = dt.Year(), int(dt.Month()), dt.Day()
	mn, errorResult, errorText, req, res := "", false, []byte{}, []byte{}, []byte{}

	for !d.Check(d.Connect()) {
		log.Warnf("Waiting db connection...")
		time.Sleep(5 * time.Second)
	}

	Loans := []Loan{}
	if o_customer || o_loan || o_accrual || o_issue{
		//		Loans, err = d.Dbd_GetLoans(year, month, day)
		Loans, err = d.Dbd_GetLoans_Triple(year, month, day)
		//Loans, err = d.Loans_OVZ_Online()
		if !d.Check(err) {
			return err, nil
		}
	}
	//Клиентов передавать по 1 запросу как ФЛ
	/*	if false || year >= 202100 {
			cсpc := mfo1c.API_contractor_create_private_customer{}
			_, mn, _, _ = cсpc.Method()

			log.Infof("Customers: %d total", len(Loans))
			for i, loan := range Loans {
				if err = d.LoanToCсpc(&loan, &cсpc); d.Check(err) {
					req, res, _, errorResult, errorText = d.Mfo1c.Execute(&cсpc)
					err = d.Dbd_SaveResult(mn, year, month, day, errorText, errorResult, req, res)
					d.Check(err)
				} else {
					return err
				}
				log.Infof("Customer %d/%d: %s %s %s",
					i+1, len(Loans),
					cсpc.Params.Контрагент.Реквизиты.Фамилия,
					cсpc.Params.Контрагент.Реквизиты.Имя,
					cсpc.Params.Контрагент.Реквизиты.Отчество,
				)
			}
			log.Infof("Customers: completed %d total", len(Loans))
		}
	*/

	//Клиент, займ, выдача
	bocs := mfo1c.API_batch_objects_create_sync{}
	_, mn, _, _ = bocs.Method()
	for i, loan := range Loans {
		//		if loan.CjID.String() == "CB5957C9-FE77-4C22-BBA7-187658D01C85" {
		if err = d.LoanToBocs(&loan, &bocs, o_customer, o_loan, o_issue); !d.Check(err) {
			return err, nil
		}
		//		}
		if len(bocs.Params.Займы) >= 1000 {
			if (i > 0) && bocs.HasData() {
				log.Infof("Batch: Position=%d, Customers=%d, Loans=%d, Issue=%d", i, len(bocs.Params.Контрагенты), len(bocs.Params.Займы), len(bocs.Params.ВыдачиЗаймов))
				req, res, err := d.Mfo1c.Execute(&bocs)

				errors := d.ErrorsDescribe(bocs.Errors(err))								
				if !d.Check(err) || (len(errors) > 0){
					return err, errors
				}
				for _, err := range errors{
					log.Error(err)
				}
				err = d.Dbd_SaveResult(mn, year, month, day, errorText, errorResult, req, res)
				if !d.Check(err) {
					return err, errors
				}
				log.Infof("Batch completed: Position=%d, Customers=%d, Loans=%d, Issue=%d", i, len(bocs.Params.Контрагенты), len(bocs.Params.Займы), len(bocs.Params.ВыдачиЗаймов))
			}
			bocs = mfo1c.API_batch_objects_create_sync{}
		}
	}
	if bocs.HasData() {
		log.Infof("Batch: Customers=%d, Loans=%d, Issue=%d", len(bocs.Params.Контрагенты), len(bocs.Params.Займы), len(bocs.Params.ВыдачиЗаймов))
		req, res, err := d.Mfo1c.Execute(&bocs)
		errors := d.ErrorsDescribe(bocs.Errors(err))
		if !d.Check(err) || (len(errors) > 0){
			return err, errors
		}
		err = d.Dbd_SaveResult(mn, year, month, day, errorText, errorResult, req, res)
		if !d.Check(err) {
			return err, errors
		}
		log.Infof("Batch completed: Customers=%d, Loans=%d, Issue=%d", len(bocs.Params.Контрагенты), len(bocs.Params.Займы), len(bocs.Params.ВыдачиЗаймов))
	}

	//Закроем номинальные(1 коп) займы
	if o_loan {
		for i, loan := range Loans {
			if lrac, err := d.LoanToLrac(&loan); d.Check(err) && (lrac != nil) {
				_, mn, _, _ = lrac.Method()
				log.Infof("Payment 0.01 %d: %s", i, lrac.Params.Идентификатор)
				req, res, err  = d.Mfo1c.Execute(lrac)
				errors := d.ErrorsDescribe(bocs.Errors(err))
				if !d.Check(err) || (len(errors) > 0){
					return err, errors
				}
				err = d.Dbd_SaveResult(mn, year, month, day, errorText, errorResult, req, res)
				if !d.Check(err) {
					return err, errors
				}
			}
		}
	}

	/*	result, text = d.Mfo1c.APIErrors(bocs, err)
		err = d.Dbd_SaveResult(mn, year, month, day, errorText, errorResult, req, res)
		d.Check(err)
		log.Infof("Batch: completed Customers=%d, Loans=%d, Issue=%d", len(bocs.Params.Контрагенты), len(bocs.Params.Займы), len(bocs.Params.ВыдачиЗаймов))
	*/

	//Начисления
	/*
		// Одиночные передачи (старый способ)

		lac := mfo1c.API_loan_accruals_create{}
		_, mn, _, _ = lac.Method()
		lac.Params.Идентификатор = time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Format(time.RFC3339)
		Acruals, err := dengi.Dbd_GetAccruals(year, month, day)
		if !dengi.Check(err) {
			return err
		}

		for _, acrual := range Acruals {
			if err = transfer.AccrualToLac(&acrual, &lac); !dengi.Check(err) {
				return err
			}
		}

		if lac.HasData() {
			log.Infof("%s %s ОсновныеНачисления=%d, ДополнительныеНачичсления=%d", procdate, mn, len(lac.Params.ОсновныеНачисления), len(lac.Params.ДополнительныеНачичсления))
			req, res, err = mfo.Process(&lac)
			Errors = lac.Errors(err)
		}

		result, text = HandleErrors()
		err = dengi.Dbd_SaveResult(mn, year, month, day, text, result, req, res)
		dengi.Check(err)
		log.Infof("%s %s Завершено", procdate, mn)
	*/

	if false /*o_loan_accrual*/{
		log.Infof("Loan accruals load ...")
		map_lac := map[string]*mfo1c.API_loan_accruals_create{}
		Acruals, err := d.Dbd_GetLoanAccruals("?????????")
		if !d.Check(err) {
			return err, nil
		}
		for _, accrual := range Acruals {
			if err = d.MapAccrualToLac(&accrual, map_lac); !d.Check(err) {
				//if err = d.MapAccrualToLacFix5(&accrual, map_lac); !d.Check(err) {
				return err, nil
			}
		}
		day_key := []string{}
		for k, _ := range map_lac {
			day_key = append(day_key, k)
		}
		log.Infof("Loan accruals: %d total, %d total days", len(Acruals), len(day_key))
		sort.Strings(day_key)
		//		for k, lac := range map_lac {
		for i, k := range day_key {
			lac := map_lac[k]
			f_ok := filepath.Join(d.Mfo1c.LogPath, "accrual", lac.Params.Идентификатор+".ok")
			f_err := filepath.Join(d.Mfo1c.LogPath, "accrual", k+".err")

			log.Infof("Accrual %d/%d processing", i, len(day_key))

			if d.FileExists(f_ok) {
				log.Warnf("Accrual %s already loaded", k)
				continue
			}

			if lac.HasData() {
				dt, _ := time.Parse(d.Dbd.DateFormat, k)
				req, res, err = d.Mfo1c.Execute(lac)
				if !d.Check(err) {
					//return err
					f, _ := os.Create(f_err)
					f.Close()
				} else {
					f, _ := os.Create(f_ok)
					f.Close()
				}
				errors := d.ErrorsDescribe(lac.Errors(err))
				if !d.Check(err) || (len(errors) > 0){
					return err, errors
				}
				err = d.Dbd_SaveResult(mn, dt.Year(), int(dt.Month()), dt.Day(), errorText, errorResult, req, res)
				if !d.Check(err) {
					return err, errors
				}
				log.Infof("Accrual %s Basic=%d, Addon=%d completed", k, len(lac.Params.ОсновныеНачисления), len(lac.Params.ДополнительныеНачичсления))
			}
		}
		log.Infof("Loan accruals: completed %d total", len(Acruals))
	}

	if o_accrual {
		log.Infof("Accruals load ...")
		map_lac := map[string]*mfo1c.API_loan_accruals_create{}
		Acruals, err := d.Dbd_GetAccruals(year, month, day)
//		Acruals, err := d.GetAccruals_01_07_2021()
		if !d.Check(err) {
			return err, nil
		}
		for _, accrual := range Acruals {
			if err = d.MapAccrualToLac(&accrual, map_lac); !d.Check(err) {
				//if err = d.MapAccrualToLacFix5(&accrual, map_lac); !d.Check(err) {
				return err, nil
			}
		}
		day_key := []string{}
		for k, _ := range map_lac {
			day_key = append(day_key, k)
		}
		log.Infof("Accruals: %d total, %d total days", len(Acruals), len(day_key))
		sort.Strings(day_key)
		//		for k, lac := range map_lac {
		for i, k := range day_key {
			lac := map_lac[k]
			f_ok := filepath.Join(d.Mfo1c.LogPath, "accrual", lac.Params.Идентификатор+".ok")
			f_err := filepath.Join(d.Mfo1c.LogPath, "accrual", k+".err")

			log.Infof("Accrual %d/%d processing", i, len(day_key))

			if d.FileExists(f_ok) {
				log.Warnf("Accrual %s already loaded", k)
				continue
			}

			if lac.HasData() {
				dt, _ := time.Parse(d.Dbd.DateFormat, k)
				req, res, err = d.Mfo1c.Execute(lac)
				if !d.Check(err) {
					//return err
					f, _ := os.Create(f_err)
					f.Close()
				} else {
					f, _ := os.Create(f_ok)
					f.Close()
				}
				errors := d.ErrorsDescribe(lac.Errors(err))
				for _, err := range errors{
					log.Error(err)
				}
				if !d.Check(err) || (len(errors) > 0){
					return err, errors
				}
				err = d.Dbd_SaveResult(mn, dt.Year(), int(dt.Month()), dt.Day(), errorText, errorResult, req, res)
				if !d.Check(err) {
					return err, errors
				}
				log.Infof("Accrual %s Basic=%d, Addon=%d completed", k, len(lac.Params.ОсновныеНачисления), len(lac.Params.ДополнительныеНачичсления))
			}
		}
		log.Infof("Accruals: completed %d total", len(Acruals))
	}

	//Пролонгации
	if o_prolongation {
		log.Infof("Prolongations load ...")
		lpc := mfo1c.API_loan_prolongation_create{}
		_, mn, _, _ = lpc.Method()
		Prolongs, err := d.Dbd_GetProlongs(year, month, day)
		if !d.Check(err) {
			return err, nil
		}
		log.Infof("Prolongations: %d total", len(Prolongs))
		for i, prolong := range Prolongs {
			if err = d.ProlongToLpc(&prolong, &lpc); d.Check(err) {
				f_ok := filepath.Join(d.Mfo1c.LogPath, "prolong", prolong.P_Identifier.String()+".ok")
				f_err := filepath.Join(d.Mfo1c.LogPath, "prolong", prolong.P_Identifier.String()+".err")

				log.Infof("Prolongation %d/%d processing", i, len(Prolongs))
				if d.FileExists(f_ok) {
					log.Warnf("Prolongation %s already loaded", i)
					continue
				}

				req, res, err = d.Mfo1c.Execute(&lpc)
				if !d.Check(err) {
					//return err
					f, _ := os.Create(f_err)
					f.Close()
				} else {
					f, _ := os.Create(f_ok)
					f.Close()
				}
				errors := d.ErrorsDescribe(lpc.Errors(err))
				if !d.Check(err) || (len(errors) > 0){
					return err, errors
				}
				/*				if !d.Check(err) {
								return err
							}*/

				/*err = d.Dbd_SaveResult(mn, year, month, day, errorText, errorResult, req, res)
				if !d.Check(err) {
					continue
					//					return err
				}*/
				log.Infof("Prolongation %d/%d", i+1, len(Prolongs))
			} else {
				return err, nil
			}
		}
		log.Infof("Prolongations: completed %d total", len(Prolongs))
	}

	//Платежи
	if o_payment {
		// Одиночные передачи (старый способ)
		/*		Errors = map[string]string{}
				lrac := mfo1c.API_loan_repayment_arbitrary_create{}
				_, mn, _, _ = lrac.Method()
				Cashies, err := dengi.Dbd_GetCashies(year, month, day)
				if !dengi.Check(err) {
					return err
				}
				for _, cash := range Cashies {
					if err = transfer.CashToLrac(&cash, &lrac); !dengi.Check(err) {
						//return err
					}
				}
				if lrac.HasData() {
					log.Infof("%s %s Платежи=%d", procdate, mn, len(lrac.Params.Платежи))
					req, res, err = mfo.Process(&lrac)
				}
				Errors = lrac.Errors(err)
				result, text = HandleErrors()
				err = dengi.Dbd_SaveResult(mn, year, month, day, text, result, req, res)
				dengi.Check(err)
				log.Infof("%s %s Завершено", procdate, mn)
		*/
		log.Infof("Payments load ...")
		map_lrac := map[string]*mfo1c.API_loan_repayment_arbitrary_create{}
		Cashies, err := d.Dbd_GetCashies(year, month, day)
		if !d.Check(err) {
			return err, nil
		}
		log.Infof("Payments: %d total", len(Cashies))
		for _, cash := range Cashies {
			if err = d.MapCashToLrac(&cash, map_lrac); !d.Check(err) {
				//return err
			}
		}
		for k, lrac := range map_lrac {
			if lrac.HasData() {
				_, mn, _, _ = lrac.Method()
				log.Infof("%s %s Платежи=%d", k, mn, len(lrac.Params.Платежи))
				req, res, err = d.Mfo1c.Execute(lrac)
				errors := d.ErrorsDescribe(lrac.Errors(err))
				if !d.Check(err) || (len(errors) > 0){
					return err, errors
				}
				err = d.Dbd_SaveResult(mn, dt.Year(), int(dt.Month()), dt.Day(), errorText, errorResult, req, res)
				if !d.Check(err) {
					return err, errors
				}
				log.Infof("Payment %s %d completed", k, len(lrac.Params.Платежи))
			}
		}
		log.Infof("Payments: completed %d total", len(Cashies))
	}

	//Переплаты
	if o_excessive {
		log.Infof("Excessive payments load ...")
		map_lroac := map[string]*mfo1c.API_loan_receipt_on_account_create{}
		Cashies, err := d.Dbd_GetCashies(year, month, day)
		if !d.Check(err) {
			return err, nil
		}
		for _, cash := range Cashies {
			if err = d.MapCashToLroac(&cash, map_lroac); !d.Check(err) {
				//return err
			}
		}
		log.Infof("Excessive payments: %d total", len(map_lroac))
		for _, lroac := range map_lroac {
			_, mn, _, _ = lroac.Method()
			req, res, err = d.Mfo1c.Execute(lroac)
			errors := d.ErrorsDescribe(lroac.Errors(err))
			if !d.Check(err) || (len(errors) > 0){
				return err, errors
			}
			err = d.Dbd_SaveResult(mn, dt.Year(), int(dt.Month()), dt.Day(), errorText, errorResult, req, res)
			if !d.Check(err) {
				return err, errors
			}
		}
		log.Infof("Excessive payments completed")
	}

	log.Warnf("Finish: %s", dt.Format(time.RFC850))
	return err, nil
}
