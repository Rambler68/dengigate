package dengi2

import "errors"

func (d *Dengi2) LoadManager(Identifier GUID, m *Manager) error {
	rows, err := d.Database.Db.NamedQuery("select * from MANAGER where IDENTIFIER=:identifier", map[string]interface{}{
		"identifier": Identifier,
	})
	if !d.Check(err) {
		return err
	}
	defer rows.Close()
	if rows.Next() {
		err = rows.StructScan(m)
	}
	return err
}

func (d *Dengi2) SaveManager(m *Manager) error {
	return errors.New("not implemented")
}
