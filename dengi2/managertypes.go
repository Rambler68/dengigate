package dengi2

var AccessType = struct {
	Denied        int
	Local         int
	Dealer        int
	Administrator int
}{
	Denied:        0,
	Local:         1,
	Dealer:        2,
	Administrator: 3,
}
