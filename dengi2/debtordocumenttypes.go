package dengi2

var DebtorType = struct {
	Disable int `alias:"Отключено"`
	Open    int `alias:"Открыт"`
	Close   int `alias:"Закрыт"`
	Wait    int `alias:"Ожидание"`
}{
	Disable: 0,
	Open:    1,
	Close:   2,
	Wait:    3,
}

var DebtorHistoryType = struct {
	CreditHistory   int `alias:"Внесение платежа"`
	ChangeStatus    int `alias:"Изменение статуса"`
	Call            int `alias:"Звонок"`
	Reminder        int `alias:"Напоминание"`
	Note            int `alias:"Заметка"`
	SMS             int `alias:"Сообщение"`
	Parameter       int `alias:"Изменение параметра"`
	BlacklistAdd    int `alias:"Черный список добавить"`
	BlacklistRemove int `alias:"Черный список изъять"`
	RecordCall      int `alias:"Запись звонка"`
}{
	CreditHistory:   0,
	ChangeStatus:    1,
	Call:            2,
	Reminder:        3,
	Note:            4,
	SMS:             5,
	Parameter:       6,
	BlacklistAdd:    7,
	BlacklistRemove: 8,
	RecordCall:      9,
}
