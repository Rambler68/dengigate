package dengi2

var PercentType = struct {
	Day   uint8 `alias:"День"`
	Week  uint8 `alias:"Неделю"`
	Month uint8 `alias:"Месяц"`
	Year  uint8 `alias:"Год"`
}{
	Day:   0,
	Week:  1,
	Month: 2,
	Year:  3,
}

var PeriodType = struct {
	Day   uint8 `alias:"День"`
	Week  uint8 `alias:"Неделя"`
	Month uint8 `alias:"Месяц"`
	Year  uint8 `alias:"Год"`
}{
	Day:   0,
	Week:  1,
	Month: 2,
	Year:  3,
}

var PenaltyType = struct {
	Disabled    uint8 `alias:"Отключено"`
	Basic       uint8 `alias:"От основной суммы займа"`
	ActiveBasic uint8 `alias:"От активной основной суммы займа"`
	Remains     uint8 `alias:"От суммы остатка"`
}{
	Disabled:    0,
	Basic:       1,
	ActiveBasic: 2,
	Remains:     3,
}

var ProlongationType = struct {
	ProlongationDisabled  uint8 `alias:"Пролонгация невозможна"`
	ProlongationDecrement uint8 `alias:"Пролонгация с уменьшением базовой суммы"`
	ProlongationIncrement uint8 `alias:"Пролонгация с увеличением базовой суммы за счет процентов, штрафов, неустоек"`
}{
	ProlongationDisabled:  0,
	ProlongationDecrement: 1,
	ProlongationIncrement: 2,
}

var DocumentStatus = struct {
	Unknown          uint8 `alias:"Неизвестно"`
	Wait             uint8 `alias:"Ожидается"`
	Open             uint8 `alias:"Открыт"`
	OpenPenalty      uint8 `alias:"Открыт(просрочка)"`
	OpenProlongation uint8 `alias:"Открыт(пролонгация)"`
	Close            uint8 `alias:"Закрыт"`
	ClosePenalty     uint8 `alias:"Закрыт(просрочка)"`
	OpenExcessive    uint8 `alias:"Закрыт(переплата)"`
}{
	Unknown:          0,
	Wait:             1,
	Open:             2,
	OpenPenalty:      3,
	OpenProlongation: 4,
	Close:            5,
	ClosePenalty:     6,
	OpenExcessive:    7,
}

var CreditHistoryType = struct {
	Credit                uint16 `alias:"Кредит(график)"`
	CreditPenaltyOnce     uint16 `alias:"Кредит(штраф)"`
	CreditPenalty         uint16 `alias:"Кредит(просроч.%)"`
	CreditService         uint16 `alias:"Кредит(обслуж)"`
	DebetBasic            uint16 `alias:"Дебет(осн)"`
	DebetPercent          uint16 `alias:"Дебет(проц)"`
	DebetPenaltyOnce      uint16 `alias:"Дебет(штраф)"`
	DebetPenalty          uint16 `alias:"Дебет(просроч.%)"`
	DebetService          uint16 `alias:"Дебет(обслуж)"`
	ProlongationDecrement uint16 `alias:"Пролонгация(умен)"`
	ProlongationIncrement uint16 `alias:"Пролонгация(увел)"`
	FreezePermanent       uint16 `alias:"Заморозка(пост)"`
	FreezePeriod          uint16 `alias:"Заморозка(врем)"`
	Fixation              uint16 `alias:"Фиксация"`
	CreditDuty            uint16 `alias:"Кредит(госпошлина)"`
	DebetDuty             uint16 `alias:"Дебет(госпошлина)"`
	CreditExcessive       uint16 `alias:"Кредит(переплата)"`
	DebetExcessive        uint16 `alias:"Дебет(переплата)"`
	CreditDutyPrik        uint16 `alias:"Кредит Гос.пошлина-Приказ"`
	CreditDutyIsk         uint16 `alias:"Кредит Гос.пошлина-Иск"`
	CreditDutyApel        uint16 `alias:"Кредит Гос.пошлина-Апел."`
	CreditDutyKass1       uint16 `alias:"Кредит Гос.пошлина-Касс.1"`
	CreditDutyKass2       uint16 `alias:"Кредит Гос.пошлина-Касс.2"`
	CreditDutyNadz        uint16 `alias:"Кредит госпошлина надзорная"`
	CreditPredPrik        uint16 `alias:"Кредит Представ.-Приказ"`
	CreditPredIsk         uint16 `alias:"Кредит Представ.-Иск"`
	CreditPredOtzIsk1     uint16 `alias:"Кредит Представ.-Отзыв"`
	CreditPredOtzIsk2     uint16 `alias:"Кредит Представ.-Отзыв м.ж."`
	CreditPredCompl       uint16 `alias:"Кредит предст. расх. (Частная жалоба)"`
	CreditPredOtzCompl    uint16 `alias:"Кредит предст. расх. (отзыв на Частную жалобу)"`
	CreditPredApel        uint16 `alias:"Кредит Представ.-Апел.ж."`
	CreditPredOtzApel     uint16 `alias:"Кредит Представ.-Отзыв Апел."`
	CreditPredKass        uint16 `alias:"Кредит Представ.-Касс."`
	CreditPredNadz        uint16 `alias:"Кредит предст. расх. (Надзорная жалоба)"`
	CreditPredOther       uint16 `alias:"Кредит предст. расх. (другое)"`
	DebetDutyPrik         uint16 `alias:"Дебет Гос.пошлина-Приказ"`
	DebetDutyIsk          uint16 `alias:"Дебет Гос.пошлина-Иск"`
	DebetDutyApel         uint16 `alias:"Дебет Гос.пошлина-Апел."`
	DebetDutyKass1        uint16 `alias:"Дебет Гос.пошлина-Касс.1"`
	DebetDutyKass2        uint16 `alias:"Дебет Гос.пошлина-Касс.2"`
	DebetDutyNadz         uint16 `alias:"Дебет госпошлина надзорная"`
	DebetPredPrik         uint16 `alias:"Дебет Представ.-Приказ"`
	DebetPredIsk          uint16 `alias:"Дебет Представ.-Иск"`
	DebetPredOtzIsk1      uint16 `alias:"Дебет Представ.-Отзыв"`
	DebetPredOtzIsk2      uint16 `alias:"Дебет Представ.-Отзыв м.ж."`
	DebetPredCompl        uint16 `alias:"Дебет предст. расх. (Частная жалоба)"`
	DebetPredOtzCompl     uint16 `alias:"Дебет предст. расх. (отзыв на Частную жалобу)"`
	DebetPredApel         uint16 `alias:"Дебет Представ.-Апел.ж."`
	DebetPredOtzApel      uint16 `alias:"Дебет Представ.-Отзыв Апел."`
	DebetPredKass         uint16 `alias:"Дебет Представ.-Касс."`
	DebetPredNadz         uint16 `alias:"Дебет предст. расх. (Надзорная жалоба)"`
	DebetPredOther        uint16 `alias:"Дебет предст. расх. (другое)"`
	OffsBasic             uint16 `alias:"Списание(осн)"`
	OffsPercent           uint16 `alias:"Списание(проц)"`
	OffsPenaltyOnce       uint16 `alias:"Списание(штраф)"`
	OffsPenalty           uint16 `alias:"Списание(просроч.%)"`
	OffsService           uint16 `alias:"Списание(обслуж)"`
	OffsDutyPrik          uint16 `alias:"Списание Гос.пошлина-Приказ"`
	OffsDutyIsk           uint16 `alias:"Списание Гос.пошлина-Иск"`
	OffsDutyApel          uint16 `alias:"Списание Гос.пошлина-Апел."`
	OffsDutyKass1         uint16 `alias:"Списание Гос.пошлина-Касс.1"`
	OffsDutyKass2         uint16 `alias:"Списание Гос.пошлина-Касс.2"`
	OffsDutyNadz          uint16 `alias:"Списание госпошлина надзорная"`
	OffsPredPrik          uint16 `alias:"Списание Представ.-Приказ"`
	OffsPredIsk           uint16 `alias:"Списание Представ.-Иск"`
	OffsPredOtzIsk1       uint16 `alias:"Списание Представ.-Отзыв"`
	OffsPredOtzIsk2       uint16 `alias:"Списание Представ.-Отзыв м.ж."`
	OffsPredCompl         uint16 `alias:"Списание предст. расх. (Частная жалоба)"`
	OffsPredOtzCompl      uint16 `alias:"Списание предст. расх. (отзыв на Частную жалобу)"`
	OffsPredApel          uint16 `alias:"Списание Представ.-Апел.ж."`
	OffsPredOtzApel       uint16 `alias:"Списание Представ.-Отзыв Апел."`
	OffsPredKass          uint16 `alias:"Списание Представ.-Касс."`
	OffsPredNadz          uint16 `alias:"Списание предст. расх. (Надзорная жалоба)"`
	OffsPredOther         uint16 `alias:"Списание предст. расх. (другое)"`
	ReturnBasic           uint16 `alias:"Возврат(осн)"`
	ReturnPercent         uint16 `alias:"Возврат(проц)"`
	ReturnPenaltyOnce     uint16 `alias:"Возврат(штраф)"`
	ReturnPenalty         uint16 `alias:"Возврат(просроч.%)"`
	ReturnService         uint16 `alias:"Возврат(обслуж)"`
	ReturnDutyPrik        uint16 `alias:"Возврат Гос.пошлина-Приказ"`
	ReturnDutyIsk         uint16 `alias:"Возврат Гос.пошлина-Иск"`
	ReturnDutyApel        uint16 `alias:"Возврат Гос.пошлина-Апел."`
	ReturnDutyKass1       uint16 `alias:"Возврат Гос.пошлина-Касс.1"`
	ReturnDutyKass2       uint16 `alias:"Возврат Гос.пошлина-Касс.2"`
	F_106                 uint16 `alias:"Возврат госпошлина надзорная"`
	ReturnPredPrik        uint16 `alias:"Возврат Представ.-Приказ"`
	ReturnPredIsk         uint16 `alias:"Возврат Представ.-Иск"`
	ReturnPredOtzIsk1     uint16 `alias:"Возврат Представ.-Отзыв"`
	ReturnPredOtzIsk2     uint16 `alias:"Возврат Представ.-Отзыв м.ж."`
	F_111                 uint16 `alias:"Возврат предст. расх. (Частная жалоба)"`
	F_112                 uint16 `alias:"Возврат предст. расх. (отзыв на Частную жалобу)"`
	ReturnPredApel        uint16 `alias:"Возврат Представ.-Апел.ж."`
	ReturnPredOtzApel     uint16 `alias:"Возврат Представ.-Отзыв Апел."`
	ReturnPredKass        uint16 `alias:"Возврат Представ.-Касс."`
	F_116                 uint16 `alias:"Возврат предст. расх. (Надзорная жалоба)"`
	F_117                 uint16 `alias:"Возврат предст. расх. (другое)"`
	F_119                 uint16 `alias:"Кредит Представ.-Отзыв Касс."`
	DebetPredOtzKass      uint16 `alias:"Дебет Представ.-Отзыв Касс."`
	F_121                 uint16 `alias:"Списание Представ.-Отзыв Касс."`
	ReturnPredOtzKass     uint16 `alias:"Возврат Представ.-Отзыв Касс."`
	OffsExcessive         uint16 `alias:"Списание(перепл)"`
	ReturnExcessive       uint16 `alias:"Возврат(перепл)"`
	F_125                 uint16 `alias:"Кредит Представ.-Дов-сть"`
	DebetPredDov          uint16 `alias:"Дебет Представ.-Дов-сть"`
	F_127                 uint16 `alias:"Списание Представ.-Дов-сть"`
	ReturnPredDov         uint16 `alias:"Возврат Представ.-Дов-сть"`
	CreditPostage         uint16 `alias:"Кредит Почт.расходы - Иск"`
	DebetPostage          uint16 `alias:"Дебет Почт.расходы - Иск"`
	OffsPostage           uint16 `alias:"Списание Почт.расходы - Иск"`
	ReturnPostage         uint16 `alias:"Возврат Почт.расходы - Иск"`
	CreditHolidays        uint16 `alias:"Льготный период"`
}{
	Credit:                0,
	CreditPenaltyOnce:     1,
	CreditPenalty:         2,
	CreditService:         3,
	DebetBasic:            4,
	DebetPercent:          5,
	DebetPenaltyOnce:      6,
	DebetPenalty:          7,
	DebetService:          8,
	ProlongationDecrement: 9,
	ProlongationIncrement: 10,
	FreezePermanent:       11,
	FreezePeriod:          12,
	Fixation:              13,
	CreditDuty:            14,
	DebetDuty:             15,
	CreditExcessive:       16,
	DebetExcessive:        17,
	CreditDutyPrik:        18,
	CreditDutyIsk:         19,
	CreditDutyApel:        20,
	CreditDutyKass1:       21,
	CreditDutyKass2:       22,
	CreditDutyNadz:        23,
	CreditPredPrik:        24,
	CreditPredIsk:         25,
	CreditPredOtzIsk1:     26,
	CreditPredOtzIsk2:     27,
	CreditPredCompl:       28,
	CreditPredOtzCompl:    29,
	CreditPredApel:        30,
	CreditPredOtzApel:     31,
	CreditPredKass:        32,
	CreditPredNadz:        33,
	CreditPredOther:       34,
	DebetDutyPrik:         35,
	DebetDutyIsk:          36,
	DebetDutyApel:         37,
	DebetDutyKass1:        38,
	DebetDutyKass2:        39,
	DebetDutyNadz:         40,
	DebetPredPrik:         41,
	DebetPredIsk:          42,
	DebetPredOtzIsk1:      43,
	DebetPredOtzIsk2:      44,
	DebetPredCompl:        45,
	DebetPredOtzCompl:     46,
	DebetPredApel:         47,
	DebetPredOtzApel:      48,
	DebetPredKass:         49,
	DebetPredNadz:         50,
	DebetPredOther:        51,
	OffsBasic:             52,
	OffsPercent:           53,
	OffsPenaltyOnce:       54,
	OffsPenalty:           55,
	OffsService:           56,
	OffsDutyPrik:          57,
	OffsDutyIsk:           58,
	OffsDutyApel:          59,
	OffsDutyKass1:         60,
	OffsDutyKass2:         61,
	OffsDutyNadz:          62,
	OffsPredPrik:          63,
	OffsPredIsk:           64,
	OffsPredOtzIsk1:       65,
	OffsPredOtzIsk2:       66,
	OffsPredCompl:         67,
	OffsPredOtzCompl:      68,
	OffsPredApel:          69,
	OffsPredOtzApel:       70,
	OffsPredKass:          71,
	OffsPredNadz:          72,
	OffsPredOther:         73,
	ReturnBasic:           96,
	ReturnPercent:         97,
	ReturnPenaltyOnce:     98,
	ReturnPenalty:         99,
	ReturnService:         100,
	ReturnDutyPrik:        101,
	ReturnDutyIsk:         102,
	ReturnDutyApel:        103,
	ReturnDutyKass1:       104,
	ReturnDutyKass2:       105,
	F_106:                 106,
	ReturnPredPrik:        107,
	ReturnPredIsk:         108,
	ReturnPredOtzIsk1:     109,
	ReturnPredOtzIsk2:     110,
	F_111:                 111,
	F_112:                 112,
	ReturnPredApel:        113,
	ReturnPredOtzApel:     114,
	ReturnPredKass:        115,
	F_116:                 116,
	F_117:                 117,
	F_119:                 119,
	DebetPredOtzKass:      120,
	F_121:                 121,
	ReturnPredOtzKass:     122,
	OffsExcessive:         123,
	ReturnExcessive:       124,
	F_125:                 125,
	DebetPredDov:          126,
	F_127:                 127,
	ReturnPredDov:         128,
	CreditPostage:         129,
	DebetPostage:          130,
	OffsPostage:           131,
	ReturnPostage:         132,
	CreditHolidays:        333,
}
