package dengi2

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/stackend/dengigate/mfo1c"
)

func (d *Dengi2) Dbd_GetLoans_Triple_old_PDN(Year, Month, Day int) ([]Loan, error) {
	Loans := []Loan{}
	if (Year < 2022) || (len(Dbd_TestData) != 0) {
		RLoans, err := d.Dbd_GetRLoans()
		if !d.Check(err) {
			return nil, err
		}
		//Исправить дата и сумма выдачи
		for _, loan := range *RLoans {
			if true || (loan.CjID.String() == "DED66008-8D01-4C60-BEEC-3CEAC915FC72") && ((len(Dbd_TestData) == 0) || (Dbd_TestData[loan.CjID.String()] != "")) {
					//займ выдан 31.12.2021
				/*
					loan.Stamp = time.Date(2020, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, -loan.Penaltydays)
					loan.Basicvalue = loan.SaldoBasic

			Дата выдачи займа = (Дата 31.12.21) - период - срок просрочки на 31.12.21 + Если срок просрочки = 0 то + DATEDIFF( дата окончания , 31.12.21 )

				*/
				/*
				dd := 0
				if (loan.Penaltydays == 0) {
					dd = -int(loan.Perioddays)-int(loan.Penaltydays)+1
				} else{
					dd = -int(loan.Perioddays)-int(loan.Penaltydays)
				}
*/

				dd := -int(loan.Perioddays)-int(loan.Penaltydays)
				if (loan.Penaltydays == 0) {
					dur := loan.Stamp.AddDate(0, 0, int(loan.Perioddays)).Sub(time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local))
					dd += int(dur.Hours() / 24)+1
				}

				loan.Stamp = time.Date(2021, 12, 31, 8, 0, 0, 0, time.Local).AddDate(0, 0, dd)
				loan.Basicvalue = loan.SaldoBasic
				Loans = append(Loans, loan)
				//какой период должен быть у просроченных займов???
				//loan.Perioddays = to-do ???
				//Loans[i].Basicvalue -= loan.Debetbasicvalue
				//Loans[i].Basicvalue = loan.SaldoBasic
			}
		}
		return Loans, nil
	}

	CreditHistoryKeys := []string{}
	for k, _ := range CreditHistoryType1C {
		CreditHistoryKeys = append(CreditHistoryKeys, fmt.Sprintf("%d", k))
	}

	err := d.Database.Db.Select(&Loans, `
	SELECT
		CJ.IDENTIFIER CJ_ID,
		CAST(CJ.STAMP AS DATE) stamp,
		CJ.PERIOD perioddays,
		CJ.[PERCENT] [percent],
		CJ.NUMBERPUBLIC number,
		C.OWNER CUSTOMEROWNER,
		D.NAME DEALER,
		D.IDENTIFIER DEALERID,
		CONCAT(C.INFO_NAME1, ' ', C.INFO_NAME2, ' ', C.INFO_NAME3) fio,
		C.INFO_NAME1 firstname,
		C.INFO_NAME2 lastname,
		C.INFO_NAME3 patronymicname,
		C.INFO_STAMP1 birthday,
		CASE WHEN CJ.BASICVALUE>=10000 THEN 
		ISNULL((SELECT top(1) p.pdn 
				FROM dengi.dbo.eqf_PDN_log p(nolock)
				WHERE p.customer = C.IDENTIFIER
				AND CAST(p.stamp_requiest as date) <= CAST(CJ.STAMP as date)
				ORDER BY p.stamp_response desc),0)
				ELSE 0 END * 100 pdn,		   
		CJ.BASICVALUE,
		CJ.DEBETBASICVALUE debetbasicvalue,
		CJ.DEBETTOTALVALUE - CJ.DEBETBASICVALUE debetpercentlue
	FROM CREDIT_JOURNAL CJ(NOLOCK)
	INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER
	INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
	INNER JOIN DENGI.DBO.CASH_JOURNAL KJ(NOLOCK) ON CJ.IDENTIFIER=KJ.IDENTIFIER
	WHERE `+Dbd_WhereClause("CJ.IDENTIFIER", "CJ.STAMP", Year, Month, Day)+`
		AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')

	UNION
	SELECT
		CJ.IDENTIFIER CJ_ID,
		'31.12.2013' stamp,
		CJ.PERIOD perioddays,
		CJ.[PERCENT] [percent],
		CJ.NUMBERPUBLIC number,
		C.OWNER CUSTOMEROWNER,
		D.NAME DEALER,
		D.IDENTIFIER DEALERID,
		CONCAT(C.INFO_NAME1, ' ', C.INFO_NAME2, ' ', C.INFO_NAME3) fio,
		C.INFO_NAME1 firstname,
		C.INFO_NAME2 lastname,
		C.INFO_NAME3 patronymicname,
		C.INFO_STAMP1 birthday,
		CASE WHEN CJ.BASICVALUE>=10000 THEN 
		ISNULL((SELECT top(1) p.pdn 
				FROM dengi.dbo.eqf_PDN_log p(nolock)
				WHERE p.customer = C.IDENTIFIER
				AND CAST(p.stamp_requiest as date) <= CAST(CJ.STAMP as date)
				ORDER BY p.stamp_response desc),0)
				ELSE 0 END * 100 pdn,		   
		0.01 BASICVALUE,
		CJ.DEBETBASICVALUE debetbasicvalue,
		CJ.DEBETTOTALVALUE - CJ.DEBETBASICVALUE debetpercentlue
		FROM DENGI.DBO.CASH_JOURNAL KJ(NOLOCK)
		INNER JOIN DENGI.DBO.CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=KJ.OWNER
		INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER
		INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
		INNER JOIN dengi_report.dbo.DM_RESERVE_PERIOD_LIST PL(NOLOCK) ON PL.identifier='`+Dbd_ReserveIdentifier2022+`'
		LEFT JOIN dengi_report.dbo.DM_RESERVE_TABLE_LIST RL(NOLOCK) ON RL.CJ_ID=KJ.OWNER AND RL.pid=PL.identifier
		WHERE `+Dbd_WhereClause("KJ.IDENTIFIER", "KJ.STAMP", Year, Month, Day)+`
			AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
			AND CJ.STAMP<PL.pend
			AND RL.identifier IS NULL

	UNION
	SELECT DISTINCT
		CJ.IDENTIFIER CJ_ID,
		'31.12.2013' stamp,
		CJ.PERIOD perioddays,
		CJ.[PERCENT] [percent],
		CJ.NUMBERPUBLIC number,
		C.OWNER CUSTOMEROWNER,
		D.NAME DEALER,
		D.IDENTIFIER DEALERID,
		CONCAT(C.INFO_NAME1, ' ', C.INFO_NAME2, ' ', C.INFO_NAME3) fio,
		C.INFO_NAME1 firstname,
		C.INFO_NAME2 lastname,
		C.INFO_NAME3 patronymicname,
		C.INFO_STAMP1 birthday,
		CASE WHEN CJ.BASICVALUE>=10000 THEN 
		ISNULL((SELECT top(1) p.pdn 
				FROM dengi.dbo.eqf_PDN_log p(nolock)
				WHERE p.customer = C.IDENTIFIER
				AND CAST(p.stamp_requiest as date) <= CAST(CJ.STAMP as date)
				ORDER BY p.stamp_response desc),0)
				ELSE 0 END * 100 pdn,		   
		0.01 BASICVALUE,
		CJ.DEBETBASICVALUE debetbasicvalue,
		CJ.DEBETTOTALVALUE - CJ.DEBETBASICVALUE debetpercentlue
		FROM (
			SELECT
				DISTINCT CH.OWNER
			FROM CREDIT_HISTORY CH(NOLOCK)
			WHERE 
				CH.HISTORYTYPE IN (`+strings.Join(CreditHistoryKeys[:], ",")+`)
				AND `+Dbd_WhereClause("CH.IDENTIFIER", "CH.STAMP", Year, Month, Day)+`
			--GROUP BY CH.OWNER
		) CH
		INNER JOIN DENGI.DBO.CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=CH.OWNER
		INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER
		INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
		INNER JOIN dengi_report.dbo.DM_RESERVE_PERIOD_LIST PL(NOLOCK) ON PL.identifier='`+Dbd_ReserveIdentifier2022+`'
		LEFT JOIN dengi_report.dbo.DM_RESERVE_TABLE_LIST RL(NOLOCK) ON RL.CJ_ID=CJ.IDENTIFIER AND RL.pid=PL.identifier
		WHERE 
			(D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
			AND CJ.STAMP<PL.pend
			AND RL.identifier IS NULL
		`)

	if !d.Check(err) {
		return nil, err
	}

	return Loans, err
}




func (d *Dengi2) Dbd_LoanToAccruals_Fix5() ([]Accrual, error) {
	Accruals := []Accrual{}
	err := d.Database.Db.Select(&Accruals, fmt.Sprintf(`
		SELECT
		CH.OWNER CJ_IDENTIFIER,
		C.OWNER C_OWNER,
		CAST(CH.STAMP AS DATE) STAMP,
		ISNULL(CH.PERPERIODVALUE, 0)
		+ ISNULL(CH.ADVANCEDUTYVALUE, 0)
		+ ISNULL(CH.ADVANCEPOSTAGEVALUE, 0)
		+ ISNULL(CH.ADVANCEREPRESENTVALUE, 0) VALUE,
		CH.HISTORYTYPE
		FROM CREDIT_HISTORY CH(NOLOCK)
		INNER JOIN CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=CH.OWNER
		INNER JOIN DP_CREDITHISTORYTYPE CHT(NOLOCK) ON CH.HISTORYTYPE=CHT.ID AND CHT.NAME LIKE '%%КРЕДИТ%%'
		INNER JOIN CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER		
		WHERE CJ.NUMBERPUBLIC IN (%s)
		ORDER BY CH.STAMP`, `'O-243/00917-2021', 'ОВЗ-037/01433-2021', 'ОВЗ-057/01173-2021', 'ОВЗ-124/00952-2021', 'ОВЗ-124/00952-2021', 'ОВЗ-037/00024-2022'`))
	if !d.Check(err) {
		return nil, err
	}
	return Accruals, err
}



func (d *Dengi2) LoanToCсpc(loan *Loan, cсpc *mfo1c.API_contractor_create_private_customer) error {
	cсpc.Params.Идентификатор = loan.Customerowner.String()
	cсpc.Params.Реквизиты.Имя = loan.LastName
	cсpc.Params.Реквизиты.Отчество = loan.PatronymicName
	cсpc.Params.Реквизиты.Фамилия = loan.FirstName
	cсpc.Params.Реквизиты.ДатаРождения = loan.Birthday.Format(time.RFC3339)
	return nil
}


func (d *Dengi2) Dbd_GetLoanCashies(LoanIdentifier string) ([]Cash, error) {
	Cashies := []Cash{}
	err := d.Database.Db.Select(&Cashies, `
		SELECT 
			CONCAT_WS('_', 'Погашение', FORMAT(GETDATE(), 'd.M.yyyy'), CJ.IDENTIFIER) BUNDLE,		
			CJ.NUMBERPUBLIC CJ_NUMBER,
			CJ.DOCUMENTSTATUS CJ_DOCUMENTSTATUS,
			CJ.SALDOBASICVALUE CJ_SALDOBASICVALUE,
			CAST(CJ.LASTDEBETSTAMP AS DATE) CJ_LASTDEBETSTAMP,
			CJ.DEALER CJ_DEALER,
			C.OWNER C_OWNER,
			D.IDENTIFIER D_IDENTIFIER,
			D.NAME D_NAME,
			KJ.IDENTIFIER,
			CAST(KJ.STAMP AS DATE) STAMP,
			KJ.CASHVALUE,
			KJ.CASHTYPE,
			KJ.NUMBER,
			KJ.DESCRIPTION,
			KJ.DESCRIPTION_1C
		FROM CASH_JOURNAL KJ(NOLOCK)
		INNER JOIN DENGI.DBO.DEALER D(NOLOCK) ON KJ.DEALER=D.IDENTIFIER
		INNER JOIN DENGI.DBO.CREDIT_JOURNAL CJ(NOLOCK) ON KJ.OWNER=CJ.IDENTIFIER
		INNER JOIN DENGI.DBO.CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER		
		WHERE KJ.STAMP BETWEEN '01.01.2022' AND '31.03.2022'
			AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
			AND KJ.OWNER=`+LoanIdentifier+`'
			`)
//			AND CJ.STAMP<'31.03.2022'
//			AND CJ.DOCUMENTSTATUS IN (5,6)
// AND CJ.STAMP>'01.12.2022'
	if !d.Check(err) {
		return nil, err
	}
	return Cashies, err
}



func (d *Dengi2) Dbd_GetLoanAccruals(LoanIdentifier string) ([]Accrual, error) {
	Accruals := []Accrual{}
	err := d.Database.Db.Select(&Accruals, `
		SELECT
			CONCAT_WS('_', 'Начисление', FORMAT(GETDATE(), 'd.M.yyyy'), CJ.IDENTIFIER) BUNDLE,		
			CH.OWNER CJ_IDENTIFIER,
			C.OWNER C_OWNER,
			CAST(CH.STAMP AS DATE) STAMP,
			ISNULL(CH.PERPERIODVALUE, 0)
			+ ISNULL(CH.ADVANCEDUTYVALUE, 0)
			+ ISNULL(CH.ADVANCEPOSTAGEVALUE, 0)
			+ ISNULL(CH.ADVANCEREPRESENTVALUE, 0) VALUE,
			CH.HISTORYTYPE
		FROM CREDIT_HISTORY CH(NOLOCK)
		INNER JOIN CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=CH.OWNER
		INNER JOIN DEALER D(NOLOCK) ON D.IDENTIFIER=CJ.DEALER
		INNER JOIN DP_CREDITHISTORYTYPE CHT(NOLOCK) ON CH.HISTORYTYPE=CHT.ID AND CHT.NAME LIKE '%%КРЕДИТ%%'
		INNER JOIN CUSTOMER C(NOLOCK) ON C.IDENTIFIER=CJ.CUSTOMER		
		WHERE CH.STAMP BETWEEN '01.01.2022' AND '31.03.2022 23:59'
		    AND CJ.IDENTIFIER='`+LoanIdentifier+`'
			AND (D.PARENTITEM='`+Dbd_MFK+`' OR D.IDENTIFIER='`+Dbd_MFK+`')
		ORDER BY CH.STAMP`)
	if !d.Check(err) {
		return nil, err
	}
	return Accruals, err
}
