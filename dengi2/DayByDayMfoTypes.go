package dengi2

import (
	"gitlab.com/stackend/dengigate/mfo1c"
)

/*
  CashType -> ВидНачисления
*/
var CashType1C = map[uint8]string{
	CashType.Debet:             "",
	CashType.DebetSystem:       "",
	CashType.DebetBasic:        mfo1c.ВидНачисления.ОсновнойДолг,
	CashType.DebetPercent:      mfo1c.ВидНачисления.Проценты, //mfo1c.ВидНачисления.Проценты,
	CashType.DebetPenaltyOnce:  mfo1c.ВидНачисления.Проценты, //mfo1c.ВидНачисления.Штраф,
	CashType.DebetPenalty:      mfo1c.ВидНачисления.Проценты, //mfo1c.ВидНачисления.Пени,
	CashType.DebetService:      "",                           //mfo1c.ВидНачисления.Обслуживание,
	CashType.DebetManager:      "",
	CashType.DebetCashless:     "",
	CashType.DebetShortage:     "",
	CashType.DebetExcessive:    "",//обрабатывается отдельно в lroac
	CashType.DebetInsurance:    "", //mfo1c.ВидНачисления.Страховка,
	CashType.DebetPostage:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetDutyPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetDutyIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetDutyApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetDutyKass1:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetDutyKass2:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetDutyNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredOtzIsk1:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredOtzIsk2:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredCompl:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredOtzCompl: mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredOtzApel:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredKass:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredOther:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredDover:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	CashType.DebetPredOtzKass:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	250:                        mfo1c.ВидНачисления.ТекущийСчетРасчетов,
}

var CreditHistoryType1C = map[uint16]string{
	CreditHistoryType.Credit:             mfo1c.ВидНачисления.Проценты,
	CreditHistoryType.CreditPenaltyOnce:  mfo1c.ВидНачисления.Штраф,
	CreditHistoryType.CreditPenalty:      mfo1c.ВидНачисления.Проценты,
	CreditHistoryType.CreditService:      mfo1c.ВидНачисления.Проценты,
	CreditHistoryType.CreditDuty:         mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditExcessive:    mfo1c.ВидНачисления.ПереплатаПоМикрозаймам,
	CreditHistoryType.CreditDutyPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditDutyIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditDutyApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditDutyKass1:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditDutyKass2:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditDutyNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredOtzIsk1:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredOtzIsk2:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredCompl:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredOtzCompl: mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredOtzApel:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredKass:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.CreditPredOther:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.F_119:              mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.F_125:              mfo1c.ВидНачисления.КомиссииГоспошлина,
	CreditHistoryType.ReturnExcessive:    "обрабатывается отдельно в lroac",
	CreditHistoryType.ReturnBasic:		  "обрабатывается отдельно в lroac",
	CreditHistoryType.CreditPostage:      mfo1c.ВидНачисления.КомиссииГоспошлина,
}
