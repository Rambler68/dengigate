package dengi2

func (d *Dengi2) Loans_OVZ_Online() ([]Loan, error) {
	Loans := []Loan{}

	err := d.Database.Db.Select(&Loans, `
	SELECT
		CJ.IDENTIFIER CJ_ID,
		CAST(CJ.STAMP AS DATE) stamp,
		CJ.PERIOD perioddays,
		CJ.[PERCENT] [percent],
		CJ.NUMBERPUBLIC number,
		C.OWNER CUSTOMEROWNER,
		D.NAME DEALER,
		D.IDENTIFIER DEALERID,
		KJD.NAME KJD_DEALER,
		KJD.IDENTIFIER KJD_DEALERID,
		CONCAT(C.INFO_NAME1, ' ', C.INFO_NAME2, ' ', C.INFO_NAME3) fio,
		C.INFO_NAME1 firstname,
		C.INFO_NAME2 lastname,
		C.INFO_NAME3 patronymicname,
		C.INFO_STAMP1 birthday,
		CASE WHEN CJ.BASICVALUE>=10000 THEN 
			ISNULL(
				(
					SELECT TOP 1 ПДН
					FROM PDN
					WHERE CreditID=CJ.IDENTIFIER
				), 1)
		ELSE 0 END * 100 pdn,
		RL.SALDOBASICVALUE BASICVALUE,
		CJ.DEBETBASICVALUE debetbasicvalue,
		CJ.DEBETTOTALVALUE - CJ.DEBETBASICVALUE debetpercentlue
	FROM CREDIT_JOURNAL CJ(NOLOCK)
	inner join customer c on c.identifier=cj.customer
	inner join cash_journal kj on kj.identifier=cj.identifier
	inner join dealer d on cj.dealer=d.identifier
	inner join dealer kjd on kj.dealer=kjd.identifier
	INNER JOIN dengi_report.dbo.DM_RESERVE_PERIOD_LIST PL(NOLOCK) ON PL.identifier='`+Dbd_ReserveIdentifier2022+`'	
	LEFT JOIN dengi_report.dbo.DM_RESERVE_TABLE_LIST RL(NOLOCK) ON RL.CJ_ID=KJ.OWNER AND RL.pid=PL.identifier
	where 
	d.name<>kjd.name and not rl.CJ_ID is null
	order by cj.stamp
	`)

	if !d.Check(err) {
		return nil, err
	}

	return Loans, err
}