package dengi2

type Manager struct {
	Accesstype           int64       `db:"ACCESSTYPE"`
	Dealeritems          string      `db:"DEALERITEMS"`
	Description          string      `db:"DESCRIPTION"`
	Digest               string      `db:"DIGEST"`
	Documentnumber       int64       `db:"DOCUMENTNUMBER"`
	Documentorganization string      `db:"DOCUMENTORGANIZATION"`
	Documentserial       string      `db:"DOCUMENTSERIAL"`
	Documentstamp        string      `db:"DOCUMENTSTAMP"`
	GroupID              string      `db:"GROUP_ID"`
	Identifier           string      `db:"IDENTIFIER"`
	Name                 string      `db:"NAME"`
	NAME1                string      `db:"NAME1"`
	NAME2                string      `db:"NAME2"`
	NAME3                string      `db:"NAME3"`
	NAME4                string      `db:"NAME4"`
	NoRewriteInCash      interface{} `db:"NoRewriteInCash"`
	Parameters           string      `db:"PARAMETERS"`
	PHONE1               string      `db:"PHONE1"`
	PHONE2               string      `db:"PHONE2"`
	PHONE3               string      `db:"PHONE3"`
	PHONE4               string      `db:"PHONE4"`
	Position             int64       `db:"POSITION"`
	Stamp                string      `db:"STAMP"`
	DebtorManagerCode    string      `db:"debtor_manager_code"`
}
