package dengi2

import (
	"fmt"
	"os"
	"reflect"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"gitlab.com/stackend/dengigate/mfo1c"
)

type Dengi2 struct {
	Dbd struct {
		Testmode   bool
		BeginDate  string
		EndDate    string
		Objects    string
		DateFormat string
	}
	Database struct {
		Db       *sqlx.DB
		Hostname string
		Port     int
		Name     string
		Username string
		Password string
	}
	Http struct {
		Address  string
		Username string
		Password string
	}
	Mfo1c mfo1c.Mfo1c
}

func Alias(i interface{}, value int) (string, error) {
	v := reflect.Indirect(reflect.ValueOf(i))
	t := v.Type()
	r := ""
	for i := 0; i < v.NumField(); i++ {
		k := v.Field(i).Kind()
		switch k {
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32,
			reflect.Int64:
			{
				if v.Field(i).Int() == int64(value) {
					r = t.Field(i).Tag.Get("alias")
					break
				}
			}
		case reflect.Uint,
			reflect.Uint8,
			reflect.Uint16,
			reflect.Uint32,
			reflect.Uint64:
			{
				if v.Field(i).Uint() == uint64(value) {
					r = t.Field(i).Tag.Get("alias")
					break
				}
			}
		}
	}
	if r == "" {
		return r, fmt.Errorf(`unknown alias for "%d" value`, value)
	}
	return r, nil
}

func (d *Dengi2) Connect() error {
	var err error
	if d.Database.Db != nil {
		return nil
	}
	log.Infof("Connecting to %s ...", d.Database.Hostname)
	d.Database.Db, err = sqlx.Connect(`sqlserver`, fmt.Sprintf(`server=%s;database=%s;user id=%s;Password=%s;dial timeout=60;`, d.Database.Hostname, d.Database.Name, d.Database.Username, d.Database.Password))
	//d.Database.Db.SetConnMaxLifetime(time.Duration(300) * time.Second)
	return err
}

func (d *Dengi2) FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
