package dengi2

import (
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	log "github.com/sirupsen/logrus"
)

type TransferDataRequest struct {
	DummyBind
	Date	string	`json:"date"`
	Objects	string	`json:"objects"`
}

// GetArticle returns the specific Article. You'll notice it just
// fetches the Article right off the context, as its understood that
// if we made it this far, the Article must be on the context. In case
// its not due to a bug, then it will panic, and our Recoverer will save us.
func (d *Dengi2) TransferData(w http.ResponseWriter, r *http.Request) {
	tdr := &TransferDataRequest{}
	if !d.CheckBind(w, r, tdr) {
		return
	}
	dt, err := time.Parse(d.Dbd.DateFormat, tdr.Date)
	if !d.CheckRender(w, r, err) {
		return 
	}
	err, errors := d.Dbd_Process(dt, tdr.Objects)
	if !d.CheckRender(w, r, err) {
		return
	}
	if (err != nil) || (len(errors) > 0) {
		render.Render(w, r, d.RenderErrors(err, errors))
		return
	}
	render.Render(w, r, d.RenderData(nil))
}

func (d *Dengi2) Router() *chi.Mux {
	r := chi.NewRouter()

	r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		render.Render(w, r, d.RenderData("pong"))
	})

	
	r.Post("/transfer-data", d.TransferData)
	
	return r
}

func (d *Dengi2) Dbd_StartHttpServer() error {
	log.Infof("Starting http server on %s", d.Http.Address)
	/*
		r.Route("/sync", func(r chi.Router) {
			r.Get("/{save}", e.Sync)
		})

		s := &http.Server{
			Addr:    d.Http.Address,
			Handler: myHandler,

			ReadTimeout:    10 * time.Second,
			WriteTimeout:   10 * time.Second,
			MaxHeaderBytes: 1 << 20,
		}

		srv := &graceful.Server{
			Timeout: 20 * time.Second,
			Server: &http.Server{
				Addr:    d.Http.Address,
				Handler: chi.ServerBaseContext(baseCtx, envStruct.Routes()),
			},
		}
	*/
	r := d.Router()
	err := http.ListenAndServe(d.Http.Address, r)
	return err
}
