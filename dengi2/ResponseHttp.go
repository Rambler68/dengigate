package dengi2

import (
	"net/http"

	"github.com/go-chi/render"
)

type DummyBind struct {
}

func (db *DummyBind) Bind(r *http.Request) error {
	return nil
}

type Response struct {
	//	Auth *JwtResponse `json:"jwt,omitempty"`
	HTTPStatusCode int   `json:"-"` // http response status code
	Status         string `json:"status,omitempty"`            // person-level status message
	Data           interface{} `json:"data,omitempty"`
	Error          error       `json:"error,omitempty"` // low-level runtime error
	Errors         map[string]string `json:"errors,omitempty"`
}

func (re *Response) Render(w http.ResponseWriter, r *http.Request) error {
	/*	login := r.Context().Value("login")
		if login != nil {
			mr.Auth = login.(*JwtResponse)
		}*/
	re.Status = http.StatusText(re.HTTPStatusCode)
	render.Status(r, re.HTTPStatusCode)
	return nil
}

func (d *Dengi2) RenderData(data interface{}) render.Renderer {
	return &Response{
		HTTPStatusCode: http.StatusOK,
		Data:           data,
	}
}

func (d *Dengi2) RenderBad(err error) render.Renderer {
	return &Response{
		HTTPStatusCode: http.StatusUnprocessableEntity,
		Error:          err,
	}
}


func (d *Dengi2) RenderError(err error) render.Renderer {
	return &Response{
		HTTPStatusCode: http.StatusConflict,
		Error:          err,
	}
}

func (d *Dengi2) RenderErrors(err error, errors map[string]string) render.Renderer {
	return &Response{
		HTTPStatusCode: http.StatusConflict,
		Error:          err,
		Errors:         errors,
	}
}


func (d *Dengi2) RenderUnauthorized(err error) render.Renderer {
	return &Response{
		Error:          err,
		HTTPStatusCode: http.StatusUnauthorized,
	}
}

func (d *Dengi2) RenderForbidden(err error) render.Renderer {
	return &Response{
		Error:         err,
		HTTPStatusCode: http.StatusForbidden,
	}
}

func (d *Dengi2) RenderInternalError(err error) render.Renderer {
	return &Response{
		Error:          err,
		HTTPStatusCode: http.StatusInternalServerError,
	}
}

func (d *Dengi2) CheckRender(w http.ResponseWriter, r *http.Request, err error) bool {
	if !d.Check(err) {
		render.Render(w, r, d.RenderError(err))
		return false
	}
	return true
}

func (d *Dengi2) CheckBind(w http.ResponseWriter, r *http.Request, v render.Binder) bool {
	err := render.Bind(r, v)
	if !d.Check(err) {
		render.Render(w, r, d.RenderBad(err))
		return false
	}
	return true
}
