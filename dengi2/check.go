package dengi2

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"runtime"
	"strings"

	log "github.com/sirupsen/logrus"
)

var (
	ErrNoFieldRow error = errors.New("no field in row")
)

func (m *Dengi2) Check(err error, v ...interface{}) bool {
	if err != nil {
		var s []string
		s = append(s, err.Error())
		if len(v) > 0 {
			b, _ := json.MarshalIndent(v, "", "  ")
			s = append(s, string(b))
		}
		for calldepth := 5; calldepth >= 1; calldepth-- {
			var ok bool
			var file string
			var line int
			_, file, line, ok = runtime.Caller(calldepth)
			if ok {
				s = append(s, fmt.Sprintf("%s:%d", file, line))
			}
		}
		log.Debugln(strings.Join(s[:], "\n"))
		return NoRows(err)
	}
	return true
}

//NoRows on query result
func NoRows(err error) bool {
	//for pqSql use:
	//if err != nil && err.Error() == pg.ErrNoRows.Error() {
	if err != nil && err == sql.ErrNoRows {
		return true
	}
	return false
}
