package main

import (
	"os"
	"path/filepath"
	"time"

	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/stackend/dengigate/syslog"
)

func init() {
	LogPath, Syslog := "", ""
	dengi.Dbd.DateFormat = "2.1.2006"

	app = &cli.App{
		Name:  "dengigate",
		Usage: "dengi integration 1c",
		/*Action: func(c *cli.Context) error {
			fmt.Println("Hello default action")
			return nil
		},*/
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "db_hostname",
				Value:       "dengi-srv2",
				EnvVars:     []string{"dengi1c_db_hostname"},
				Usage:       "Database host name",
				Destination: &dengi.Database.Hostname,
			},
			&cli.IntFlag{
				Name:        "db_port",
				Value:       1433,
				EnvVars:     []string{"dengi1c_db_port"},
				Usage:       "Database port",
				Destination: &dengi.Database.Port,
			},
			&cli.StringFlag{
				Name:        "db_name",
				Value:       "dengi",
				EnvVars:     []string{"dengi1c_db_name"},
				Usage:       "Database name",
				Destination: &dengi.Database.Name,
			},
			&cli.StringFlag{
				Name:        "db_username",
				Value:       "dengi",
				EnvVars:     []string{"dengi1c_db_username"},
				Usage:       "Database user name",
				Destination: &dengi.Database.Username,
			},
			&cli.StringFlag{
				Name:        "db_password",
				Value:       "aXFR7abeQGZBc",
				EnvVars:     []string{"dengi1c_db_password"},
				Usage:       "Database user password",
				Destination: &dengi.Database.Password,
			},
			&cli.StringFlag{
				Name:        "mfo_hostname",
				Value:       "1c-srv-new",
				EnvVars:     []string{"dengi1c_mfo_hostname"},
				Usage:       "Mfo host name",
				Destination: &dengi.Mfo1c.Connection.Hostname,
			},
			&cli.IntFlag{
				Name:        "mfo_port",
				Value:       80,
				EnvVars:     []string{"dengi1c_mfo_port"},
				Usage:       "Mfo host port",
				Destination: &dengi.Mfo1c.Connection.Port,
			},
			&cli.StringFlag{
				Name:        "mfo_name",
				Value:       "eps",
				EnvVars:     []string{"dengi1c_mfo_name"},
				Usage:       "Mfo database name",
				Destination: &dengi.Mfo1c.Connection.Basename,
			},
			&cli.StringFlag{
				Name:        "mfo_username",
				Value:       "api",
				EnvVars:     []string{"dengi1c_mfo_username"},
				Usage:       "Mfo user name",
				Destination: &dengi.Mfo1c.Connection.Username,
			},
			&cli.StringFlag{
				Name:        "mfo_password",
				Value:       "api",
				EnvVars:     []string{"dengi1c_mfo_password"},
				Usage:       "Mfo user password",
				Destination: &dengi.Mfo1c.Connection.Password,
			},
			&cli.StringFlag{
				Name:        "mfo_node",
				Value:       "DM", //"mfo_node",
				EnvVars:     []string{"dengi1c_mfo_node"},
				Usage:       "Mfo node",
				Destination: &dengi.Mfo1c.Connection.Node,
			},
			&cli.StringFlag{
				Name:        "mfo_inn",
				Value:       "1650244278",
				EnvVars:     []string{"dengi1c_mfo_inn"},
				Usage:       "Mfo organization INN",
				Destination: &dengi.Mfo1c.INN,
			},
			&cli.BoolFlag{
				Name:        "mfo_logdata",
				Value:       true, //false, //true,
				EnvVars:     []string{"mfo_logdata"},
				Usage:       "Mfo log request and response data ",
				Destination: &dengi.Mfo1c.LogData,
			},
			&cli.StringFlag{
				Name:        "mfo_logpath",
				Value:       "c:\\temp",
				EnvVars:     []string{"mfo_logpath"},
				Usage:       "Log path for request and response files",
				Destination: &dengi.Mfo1c.LogPath,
			},
			&cli.StringFlag{
				Name:        "syslog",
				Value:       "localhost:514",
				EnvVars:     []string{"syslog"},
				Usage:       "Syslog server",
				Destination: &Syslog,
			},
			&cli.StringFlag{
				Name:        "logpath",
				Value:       "c:\\temp",
				EnvVars:     []string{"logpath"},
				Usage:       "Common log path ",
				Destination: &LogPath,
			},
		},
		Before: func(c *cli.Context) error {
			log.SetFormatter(&log.TextFormatter{
				//DisableColors: true,
				ForceColors:   true,
				FullTimestamp: true,
			})
			log.SetOutput(os.Stdout)
			log.SetLevel(log.DebugLevel)

			if LogPath != "" {
				pl := filepath.Join(LogPath, app.Name)
				writer, err := rotatelogs.New(
					pl+"-%d-%m-%Y-%H-%M.log",
					rotatelogs.WithLinkName(pl),
					rotatelogs.WithMaxAge(time.Duration(86400)*time.Second),
					rotatelogs.WithRotationTime(time.Duration(604800)*time.Second),
				)
				if Check(err) {
					//nop
				}
				log.StandardLogger().Hooks.Add(lfshook.NewHook(
					lfshook.WriterMap{
						logrus.InfoLevel:  writer,
						logrus.ErrorLevel: writer,
						logrus.DebugLevel: writer,
					},
					&log.TextFormatter{},
				))
			}
			if Syslog != "" {
				hook, err := syslog.NewSyslogHook("udp", Syslog, syslog.LOG_DEBUG, "dengi3")
				if Check(err) {
					log.StandardLogger().Hooks.Add(hook)
				}
			}
			return nil
		},
		Commands: []*cli.Command{
			{
				Name:    "transfer-data",
				Aliases: []string{""},
				Usage:   "Run day by day transfer process. Error exit code non zero",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:        "test-mode",
						Value:       false,
						EnvVars:     []string{"dengi1c_test_mode"},
						Usage:       "Enable test mode",
						Destination: &dengi.Dbd.Testmode,
					},
					&cli.StringFlag{
						Name:        "begin-date",
						Value:       "", //time.Date(2021, 12, 31, 0, 0, 0, 0, time.Local).Format(dengi.Dbd.DateFormat),
						EnvVars:     []string{"dengi1c_begin_date"},
						Usage:       "Begin date",
						Required:    true,
						Destination: &dengi.Dbd.BeginDate,
					},
					&cli.StringFlag{
						Name:        "end-date",
						Value:       "", //time.Date(2021, 12, 31, 0, 0, 0, 0, time.Local).Format(dengi.Dbd.DateFormat),
						EnvVars:     []string{"dengi1c_end_date"},
						Usage:       "End date",
						Required:    false,
						Destination: &dengi.Dbd.EndDate,
					},
					&cli.StringFlag{
						Name:        "objects",
						Value:       "*",
						EnvVars:     []string{"objects"},
						Usage:       "List of objects, separated by comma: *,customer,loan,issue,accrual,prolongation,payment,excessive",
						Required:    false,
						Destination: &dengi.Dbd.Objects,
					},
				},
				Action: func(c *cli.Context) error {
					dengi.SetupTestData(dengi.Dbd.Testmode)
					bd, err := time.Parse(dengi.Dbd.DateFormat, dengi.Dbd.BeginDate)
					if !dengi.Check(err) {
						return err
					}
					if dengi.Dbd.EndDate == "" {
						dengi.Dbd.EndDate = dengi.Dbd.BeginDate
					}
					ed, err := time.Parse(dengi.Dbd.DateFormat, dengi.Dbd.EndDate)
					if !dengi.Check(err) {
						return err
					}
					for d := bd; d.After(ed) == false; d = d.AddDate(0, 0, 1) {
						err, _ = dengi.Dbd_Process(d, dengi.Dbd.Objects)
						if !dengi.Check(err) {
							return err
						}
					}
					return err
				},
			},
			{
				Name:    "http-server",
				Aliases: []string{""},
				Usage:   "Start http server",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "http_address",
						Value:       "localhost:88",
						EnvVars:     []string{"http_address"},
						Usage:       "bind http address",
						Required:    false,
						Destination: &dengi.Http.Address,
					},
					&cli.StringFlag{
						Name:        "http-username",
						Value:       "hello",
						EnvVars:     []string{"http_username"},
						Usage:       "http username",
						Required:    false,
						Destination: &dengi.Http.Username,
					},
					&cli.StringFlag{
						Name:        "http-password",
						Value:       "ehlo",
						EnvVars:     []string{"http_password"},
						Usage:       "http password",
						Required:    false,
						Destination: &dengi.Http.Password,
					},
				},
				Action: func(c *cli.Context) error {
					err := dengi.Dbd_StartHttpServer()
					return err
				},
			},
		},
	}
}
