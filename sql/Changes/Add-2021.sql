
declare 
	@ctDealer int = 0,
	@ctManager int = 1,
	@ctCustomer int = 2,
	@ctCreditDocument int = 3,
	@ctCashDocument int = 4,
	@ctDebtorDocument int = 5,
	@ctDeletedCashDocument int = 6

declare
	@cjid table (id uniqueidentifier)

delete from changes where changetype in (@ctCreditDocument, @ctCashDocument, @ctDebtorDocument)

insert into @cjid
select cj.identifier
from credit_journal cj(nolock)
inner join dealer d(nolock) on cj.dealer=d.identifier
where d.parentitem='47F7A021-7D4C-4A18-9705-2CA7AFAA7056'
and dateadd(day, cj.daystatus1, cj.stamp) > '01.01.2021'
AND NOT EXISTS (select 1 from dengi.dbo.CREDIT_FAKE f where f.numberpublic=cj.numberpublic)

declare @cjcount int

select @cjcount = count(*) from @cjid

print 'loans loaded: ' + cast(@cjcount as varchar(max))


select 
--top 100
--cj.stamp, cj.numberpublic, d.name 
d.name, count(*)
from @cjid c
inner join credit_journal cj(nolock) on cj.identifier=c.id--owner
inner join dealer d(nolock) on cj.dealer=d.identifier

--where c.changetype=3

group by d.name
order by d.name

insert into changes (identifier, stamp, changetype, owner, scheduled)
select 
newid(), cj.stamp, @ctCreditDocument, cj.identifier, getdate()
from credit_journal cj(nolock)
inner join @cjid cjid on cj.identifier=cjid.id

/*
insert into changes (identifier, stamp, changetype, owner, scheduled)
select 
newid(), kj.stamp, @ctCashDocument, kj.identifier, getdate()
from cash_journal kj
inner join @cjid cjid on kj.owner=cjid.id
*/

/*
insert into changes (identifier, stamp, changetype, owner, scheduled)
select newid(), c.stamp, @ctCustomer, c.owner, getdate()
from customer c
inner join dealer d on c.dealer=d.identifier

where c.owner=c.identifier and d.PARENTITEM='47F7A021-7D4C-4A18-9705-2CA7AFAA7056' or d.identifier='47F7A021-7D4C-4A18-9705-2CA7AFAA7056'
and not c.owner in (select cc.owner from changes cc where cc.owner=c.owner)

*/