
CREATE TRIGGER dbo.TRIGGER_CASH_JOURNAL_CHANGES ON dbo.CASH_JOURNAL AFTER INSERT, UPDATE AS
BEGIN
  DECLARE @MKK INT = 0

  SELECT @MKK=1 FROM DEALER D(NOLOCK)
  INNER JOIN INSERTED I ON I.DEALER=D.IDENTIFIER
  WHERE D.PARENTITEM='47F7A021-7D4C-4A18-9705-2CA7AFAA7056'

  IF @MKK=0 RETURN

  DELETE CA FROM CHANGES CA
  INNER JOIN INSERTED I ON I.IDENTIFIER=CA.OWNER AND CA.COMPLETED IS NULL AND CA.ERRORED IS NULL AND CA.CHANGETYPE=4

  INSERT INTO CHANGES (IDENTIFIER, STAMP, CHANGETYPE, OWNER, SCHEDULED)
  SELECT NEWID(), I.STAMP, 4, I.IDENTIFIER, GETDATE() FROM INSERTED I
END
