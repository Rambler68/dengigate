--delete from changes

/*
--������
insert into changes (identifier, stamp, changetype, owner)
select newid(), getdate(), 2, qc.owner 
from (
	select distinct c.owner
	from customer c(nolock) 
	where c.stamp>'01.01.2018'	
) qc
*/
/*
--����
insert into changes (identifier, stamp, changetype, owner)
select newid(), getdate(), 3, cj.identifier
from credit_journal cj 
where cj.stamp>'01.01.2018'	

--�����
insert into changes (identifier, stamp, changetype, owner)
select newid(), getdate(), 4, kj.identifier
from credit_journal cj
inner join cash_journal kj on cj.identifier=kj.owner
where cj.stamp>'01.01.2018'	
*/