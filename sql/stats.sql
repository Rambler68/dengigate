select customer.*, loan.*, cash.* from(
	select 
	datepart(year, stamp) ayear,
	sum(case when completed is null then 0 else 1 end) customer_completed,
	sum(case when completed is null then 1 else 0 end) customer_remain,
	sum(case when errored is null then 0 else 1 end) customer_errored,
	count(*) customer_total,
	sum(case when completed is null then 0 else 1.0 end) * 100 / count(*) customer_done
	from changes(nolock) where changetype=2 group by datepart(year, stamp)
) customer
inner join (
	select 
	datepart(year, stamp) ayear
	,
	sum(case when completed is null then 0 else 1 end) loan_completed,
	sum(case when completed is null then 1 else 0 end) loan_remain,
	sum(case when not errored is null then 1 else 0 end) loan_errored,
	count(*) loan_total,
	sum(case when completed is null then 0 else 1.0 end) * 100 / count(*) loan_done
	from changes(nolock) where changetype=3 and scheduled<'22.10.2021' 
	group by datepart(year, stamp) 
) loan on customer.ayear=loan.ayear
inner join (
	select 
	datepart(year, stamp) ayear
	,
	sum(case when completed is null then 0 else 1 end) cash_completed,
	sum(case when completed is null then 1 else 0 end) cash_remain,
	sum(case when not errored is null then 1 else 0 end) cash_errored,
	count(*) cash_total,
	sum(case when completed is null then 0 else 1.0 end) * 100 / count(*) cash_done
	from changes(nolock) where changetype=4 group by datepart(year, stamp)
) cash on customer.ayear=cash.ayear

order by customer.ayear

/*
select top 100 c.stamp, cj.numberpublic, cj.dealer, d.name from changes c
inner join credit_journal cj on cj.identifier=c.owner
inner join dealer d on cj.dealer=d.identifier
where c.changetype=3 and datepart(year, c.stamp)=2010
*/