package mfo1c

/*
Возвращает графики платежей по переданным идентификаторам
*/

type API_loan_schedule_request struct {
	Данные []struct {
		Идентификатор string `json:"Идентификатор"`
		Статус        int64  `json:"Статус"`
	} `json:"Данные"`
	ИдентификаторЗапроса           string `json:"ИдентификаторЗапроса"`
	КодРезультата                  int64  `json:"КодРезультата"`
	КоличествоОбработанныхОбъектов int64  `json:"КоличествоОбработанныхОбъектов"`
	ОписаниеРезультата             string `json:"ОписаниеРезультата"`

	Params struct {
		ВнешниеДанные string   `json:"ВнешниеДанные"`
		Идентификатор []string `json:"Идентификатор"`
	} `json:"-"`
}

func (api *API_loan_schedule_request) MethodVersion() string {
	return "v1.0"
}

func (api *API_loan_schedule_request) MethodName() string {
	return "loan_schedule_request"
}

func (api *API_loan_schedule_request) MethodParams() interface{} {
	return &api.Params
}
