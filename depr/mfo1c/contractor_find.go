package mfo1c

/*
Поиск контрагента по заданным параметрам в любой комбинации.
*/

type API_contractor_find struct {
	Данные []struct {
		Идентификатор           string `json:"Идентификатор,omitempty"`
		ИдентификаторСсылки     string `json:"ИдентификаторСсылки,omitempty"`
		ОшибкиВРамкахТранзакции string `json:"ОшибкиВРамкахТранзакции,omitempty"`
		Статус                  int64  `json:"Статус,omitempty"`
	} `json:"-"`
	ДДанные       interface{} `json:"Данные"`
	ВнешниеДанные string      `json:"ВнешниеДанные"`
	//ДанныеОшибки                   []string `json:"Данные"`
	ИдентификаторЗапроса           string `json:"ИдентификаторЗапроса,omitempty"`
	КодРезультата                  int64  `json:"КодРезультата,omitempty"`
	КоличествоОбработанныхОбъектов int64  `json:"КоличествоОбработанныхОбъектов,omitempty"`
	ОписаниеРезультата             string `json:"ОписаниеРезультата,omitempty"`

	Params struct {
		ВнешниеДанные string `json:"ВнешниеДанные"`
		Данные        struct {
			АдресЭП      string `json:"АдресЭП,omitempty"`
			ДатаРождения string `json:"ДатаРождения,omitempty"`
			Имя          string `json:"Имя,omitempty"`
			Номер        string `json:"Номер,omitempty"`
			Отчество     string `json:"Отчество,omitempty"`
			Серия        string `json:"Серия,omitempty"`
			Телефон      string `json:"Телефон,omitempty"`
			Фамилия      string `json:"Фамилия,omitempty"`
		} `json:"Данные"`
		ПоляПроверки  []string `json:"ПоляПроверки,omitempty"`
		ПравилаПоиска []string `json:"ПравилаПоиска,omitempty"`
	} `json:"-"`
}

func (api *API_contractor_find) MethodVersion() string {
	return "v1.0"
}

func (api *API_contractor_find) MethodName() string {
	return "contractor_find"
}

func (api *API_contractor_find) MethodParams() interface{} {
	return &api.Params
}
