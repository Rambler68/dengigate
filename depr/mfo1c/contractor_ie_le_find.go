package mfo1c

import (
	"errors"
	"fmt"
	"log"
)

/*
Поиск контрагента со статусом ИП или ЮЛ по ИНН и КПП
*/

type API_contractor_ie_le_find struct {
	RawData interface{} `json:"Данные"`
	Данные  []struct {
		Контргаент []struct {
			Идентификатор       string `json:"Идентификатор"`
			ИдентификаторСсылки string `json:"ИдентификаторСсылки"`
			Представление       string `json:"Представление"`
		} `json:"Контргаент"`
		ОшибкиВРамкахТранзакции string `json:"ОшибкиВРамкахТранзакции"`
		ОписаниеСтатуса         string `json:"ОписаниеСтатуса"`
		Статус                  int64  `json:"Статус"`
	} `json:"-"`
	Ошибка struct {
		ОписаниеСтатуса         string `json:"ОписаниеСтатуса"`
		Статус                  int64  `json:"Статус"`
		ОшибкиВРамкахТранзакции string `json:"ОшибкиВРамкахТранзакции"`
	} `json:"-"`
	ИдентификаторЗапроса           string `json:"ИдентификаторЗапроса"`
	КодРезультата                  int64  `json:"КодРезультата"`
	КоличествоОбработанныхОбъектов int64  `json:"КоличествоОбработанныхОбъектов"`
	ОписаниеРезультата             string `json:"ОписаниеРезультата"`

	Params struct {
		ВнешниеДанные string `json:"ВнешниеДанные"`
		Инн           string `json:"ИНН"`
		Кпп           string `json:"КПП"`
	} `json:"-"`
}

func (api *API_contractor_ie_le_find) Method() (string, string, string, interface{}) {
	return "POST", "contractor_ie_le_find", "v1.0", &api.Params
}

func (api *API_contractor_ie_le_find) Error() error {
	//translate
	/*	v := reflect.ValueOf(api.RawData)
			switch v.Kind()
		switch x := api.RawData.(type) {
			case nil: {}
		case struct: {}
		}*/
	log.Println(api.RawData)
	if api.КодРезультата != 1 {
		return errors.New(fmt.Sprintf(`(%d) %s`, 57, `"ОписаниеСтатуса": "Ошибка - Не указан ИНН"`))
	}
	return nil
}
