package mfo1c

/*
Текущая информация по займу
*/

type API_loan_current_data_request struct {
	Данные                         []struct{} `json:"Данные"`
	ИдентификаторЗапроса           string     `json:"ИдентификаторЗапроса"`
	КодРезультата                  int64      `json:"КодРезультата"`
	КоличествоОбработанныхОбъектов int64      `json:"КоличествоОбработанныхОбъектов"`
	ОписаниеРезультата             string     `json:"ОписаниеРезультата"`

	Params struct {
		ВнешниеДанные string        `json:"ВнешниеДанные"`
		Идентификатор []interface{} `json:"Идентификатор"`
		Параметры     struct {
			Дата string `json:"Дата"`
		} `json:"Параметры"`
	} `json:"-"`
}

func (api *API_loan_current_data_request) MethodVersion() string {
	return "v1.0"
}

func (api *API_loan_current_data_request) MethodName() string {
	return "loan_current_data_request"
}

func (api *API_loan_current_data_request) MethodParams() interface{} {
	return &api.Params
}
