package mfo1c

type API_reference_data_request struct {
	ВнешниеДанные interface{} `json:"ВнешниеДанные"`
	Данные        []struct {
		CarModels           struct{} `json:"car_models"`
		OrganizationalUnits []struct {
			Идентификатор       string `json:"Идентификатор"`
			ИдентификаторСсылки string `json:"ИдентификаторСсылки"`
			Представление       string `json:"Представление"`
		} `json:"organizational_units"`
		PaymentMethods []struct {
			Идентификатор       string `json:"Идентификатор"`
			ИдентификаторСсылки string `json:"ИдентификаторСсылки"`
			Представление       string `json:"Представление"`
		} `json:"payment_methods"`
		СинонимыПараметров struct {
			CarModels                        string `json:"car_models"`
			ContactRoles                     string `json:"contact_roles"`
			DurationOfPresentEmployment      string `json:"duration_of_present_employment"`
			FinancialProducts                string `json:"financial_products"`
			Nomenclature                     string `json:"nomenclature"`
			ObjectsOfPledge                  string `json:"objects_of_pledge"`
			OrganizationalUnits              string `json:"organizational_units"`
			PaymentMethods                   string `json:"payment_methods"`
			Positions                        string `json:"positions"`
			ReasonsForRefusingLoan           string `json:"reasons_for_refusing_loan"`
			ScheduledTasks                   string `json:"scheduled_tasks"`
			StateOfMarriage                  string `json:"state_of_marriage"`
			TypesOfAccrualsOnLoans           string `json:"types_of_accruals_on_loans"`
			TypesOfActivities                string `json:"types_of_activities"`
			TypesOfDocumentsPrivateCustomers string `json:"types_of_documents_private_customers"`
			TypesOfEducation                 string `json:"types_of_education"`
			TypesOfEmployment                string `json:"types_of_employment"`
			TypesOfProperty                  string `json:"types_of_property"`
			TypesOfResidentialEstate         string `json:"types_of_residential_estate"`
			TypesOfSupplementaryAgreements   string `json:"types_of_supplementary_agreements"`
		} `json:"СинонимыПараметров"`
	} `json:"Данные"`
	ИдентификаторЗапроса           string `json:"ИдентификаторЗапроса"`
	КодРезультата                  int64  `json:"КодРезультата"`
	КоличествоОбработанныхОбъектов int64  `json:"КоличествоОбработанныхОбъектов"`
	ОписаниеРезультата             string `json:"ОписаниеРезультата"`
}

func (api *API_reference_data_request) Method() (string, string, string, interface{}) {
	return "GET", "reference_data_request", "v1.0", nil
}

func (api *API_reference_data_request) Error() error {
	return nil
}
