package mfo1c

/*
Создается объект со статусом ЮЛ в справочнике Контрагенты

https://json2struct.mervine.net/
https://www.convertcsv.com/csv-to-json.htm
*/

type API_contractor_create_legal_entity struct {
	Params struct {
		БанковскиеСчета []struct {
			БИКБанка   string `json:"БИКБанка,omitempty"`
			Валюта     string `json:"Валюта,omitempty"`
			НомерСчета string `json:"НомерСчета,omitempty"`
		} `json:"БанковскиеСчета,omitempty"`
		ВнешниеДанные               string `json:"ВнешниеДанные,omitempty"`
		Идентификатор               string `json:"Идентификатор,omitempty"`
		КонтактнаяИнформацияТелефон []struct {
			Вид      string `json:"Вид,omitempty"`
			Значение struct {
				AreaCode    string `json:"areaCode,omitempty"`
				CountryCode string `json:"countryCode,omitempty"`
				Number      string `json:"number,omitempty"`
				Type        string `json:"type,omitempty"`
				Value       string `json:"value,omitempty"`
			} `json:"Значение,omitempty"`
		} `json:"КонтактнаяИнформацияТелефон,omitempty"`
		Реквизиты struct {
			ДатаРегистрации                   string `json:"ДатаРегистрации,omitempty"`
			Инн                               string `json:"ИНН,omitempty"`
			Кпп                               string `json:"КПП,omitempty"`
			КодОКВЭД                          string `json:"КодОКВЭД,omitempty"`
			КодОКВЭД2                         string `json:"КодОКВЭД2,omitempty"`
			КодОКОПФ                          string `json:"КодОКОПФ,omitempty"`
			Комментарий                       string `json:"Комментарий,omitempty"`
			МестоРегистрации                  string `json:"МестоРегистрации,omitempty"`
			Наименование                      string `json:"Наименование,omitempty"`
			НаименованиеОКВЭД                 string `json:"НаименованиеОКВЭД,omitempty"`
			НаименованиеОКВЭД2                string `json:"НаименованиеОКВЭД2,omitempty"`
			НаименованиеОКОПФ                 string `json:"НаименованиеОКОПФ,omitempty"`
			НаименованиеПолное                string `json:"НаименованиеПолное,omitempty"`
			НаименованиеРегистрирующегоОргана string `json:"НаименованиеРегистрирующегоОргана,omitempty"`
			СубъектМСП                        bool   `json:"СубъектМСП,omitempty"`
		} `json:"Реквизиты,omitempty"`
	}
}

func (api *API_contractor_create_legal_entity) Method() (string, string, string, interface{}) {
	return "POST", "contractor_create_legal_entity", "v1.0", &api.Params
}

func (api *API_contractor_create_legal_entity) Error() error {
	return nil
}
