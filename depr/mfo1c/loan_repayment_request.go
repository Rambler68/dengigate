package mfo1c

/*
Возвращает погашения займов предоставленных по переданным идентификаторам платежей.
*/

type API_loan_repayment_request struct {
	Данные []struct {
		Идентификатор string `json:"Идентификатор"`
		Статус        int64  `json:"Статус"`
	} `json:"Данные"`
	ИдентификаторЗапроса           string `json:"ИдентификаторЗапроса"`
	КодРезультата                  int64  `json:"КодРезультата"`
	КоличествоОбработанныхОбъектов int64  `json:"КоличествоОбработанныхОбъектов"`
	ОписаниеРезультата             string `json:"ОписаниеРезультата"`

	Params struct {
		ВнешниеДанные string   `json:"ВнешниеДанные"`
		Идентификатор []string `json:"Идентификатор"`

		/*
		   {
		     "ВнешниеДанные": "155cc441-fb3e-4b9a-a450-661bdc3eae48",
		     "ИдентификаторТранзакции": "04d23n0e-2580-118a-90f0-00255d0b110b",
		     "Дата": "2020-01-02T10:40:28",
		     "Организация": "2263018717",
		     "ПодразделениеОрганизации": "3",
		     "Контрагент": "14438b29-8562-4027-9138-5238da9b86b9Тест3",
		     "Займ": "047dd205-c04f-4ac8-8b03-60e1456e5d27",
		     "СуммаПлатежа": 10000,
		     "ФормаОплаты": "Безналичная (банк)",
		     "НомерВходящегоДокументаОплаты": "2020-000165496",
		     "ДатаВходящегоДокументаОплаты": "2020-01-02T10:40:28",
		     "СодержаниеВходящегоДокументаОплаты": "Поступление денежных средств по догову займа №2020-ГЛ001 от 01.01.2020 от Иванова Ивана Ивановича",
		     "ПоСудебномуРешению": false,
		     "ФормироватьГрафикиПриЧастичноДосрочномПогашении": false,
		     "ЗакрытьДоговор": false
		   }*/
	} `json:"-"`
}

func (api *API_loan_repayment_request) MethodVersion() string {
	return "v1.0"
}

func (api *API_loan_repayment_request) MethodName() string {
	return "loan_repayment_request"
}

func (api *API_loan_repayment_request) MethodParams() interface{} {
	return &api.Params
}
