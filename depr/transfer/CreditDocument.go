package transfer

import (
	"fmt"
	"time"

	"gitlab.com/stackend/dengigate/dengi2"
	"gitlab.com/stackend/dengigate/mfo1c"
)

func AppendBatchCreditDocument(cd *dengi2.CreditDocument, bocs *mfo1c.API_batch_objects_create_sync) error {
	Займ := mfo1c.Займ{}

	Займ.Идентификатор = cd.Identifier.String()
	Займ.Реквизиты.Контрагент = cd.C_Owner.String() //всегда берем владельца анкеты
	Займ.Реквизиты.Организация = mfo1c.MKKINN
	if false || !cd.D_IsVirtual {
		Займ.Реквизиты.ПодразделениеОрганизации = cd.Dealer.String()
	}

	Займ.Реквизиты.ДатаДоговора = cd.Stamp.Time.Format(time.RFC3339)
	Займ.Реквизиты.ДатаВыдачи = cd.Beginstamp.Time.Format(time.RFC3339)
	//Займ.Реквизиты.ДатаОкончания = cd.CloseTime.Time.Format(time.RFC3389)
	Займ.Реквизиты.НомерДоговора = cd.Numberpublic.String
	Займ.Реквизиты.СуммаЗайма = int64(cd.Basicvalue.Float64)

	Займ.Реквизиты.ПроцентнаяСтавка = cd.Percent.Float64
	Займ.Реквизиты.СрокЗайма = int64(cd.Period.Int32)

	Займ.Реквизиты.ФинансовыйПродукт = fmt.Sprintf("Продукт_%d_%g_%d_%d_%d",
		cd.Period.Int32,
		cd.Percent.Float64,
		func(i int32) int {
			if i != 0 {
				return 1
			} else {
				return 0
			}
		}(cd.Zeroday.Int32),
		cd.Trialdays.Int32,
		cd.Requireddays.Int32)
	Займ.Реквизиты.РасчетНачисленийВоВнешнейПрограмме = false //true
	Займ.Реквизиты.ПоказательДолговойНагрузки = cd.C_PDN.Float64
	//Займ.Реквизиты.ПереноситьДатуВыдачиНаСледующийРабочийДень = false
	//Займ.Реквизиты.ОбеспеченныйЗаймЗалоги = false
	Займ.Реквизиты.ОбеспеченныйЗаймПоручительство = false
	//	Займ.Реквизиты.БанковскийСчетКонтрагента = ""

	//Займ.ГрафикПлатежей.ЭффективнаяСтавкаПроцента = cd.Identifier.String()
	//Займ.ГрафикПлатежей.ПолнаяСтоимостьЗайма = cd.Identifier.String()

	//Займ.ГрафикПлатежей.Идентификатор = cd.Identifier.String() + "_" + time.Now().Format(time.RFC3339)
	for _, ch := range cd.CreditHistory {
		switch ch.Historytype + 100 {
		case dengi2.CreditHistoryType.Credit:
			{
				/*ОсновнойДолг := mfo1c.ОсновнойДолг{}
				ОсновнойДолг.ДатаПлатежа = ch.Stamp.Time.Format(time.RFC3339)
				ОсновнойДолг.ПогашениеОсновногоДолга = ch.Percentbasicvalue.Float64
				alc.ГрафикПлатежей.ОсновнойДолг = append(alc.ГрафикПлатежей.ОсновнойДолг, ОсновнойДолг)*/

				Проценты := mfo1c.Проценты{}
				Проценты.ДатаПлатежа = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.НачалоПериода = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.КонецПериода = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.НачалоПериодаРасчета = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.КонецПериодаРасчета = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.Сумма = ch.Perperiodvalue.Float64 + 0.56
				Займ.ГрафикПлатежей.Проценты = append(Займ.ГрафикПлатежей.Проценты, Проценты)
			}
		case dengi2.CreditHistoryType.CreditPenalty:
			{
				//alc.ГрафикПлатежей.
			}
		case dengi2.CreditHistoryType.CreditDuty:
			{
				//alc.ГрафикПлатежей.

			}
		case dengi2.CreditHistoryType.CreditExcessive:
			{
				//
			}
		case dengi2.CreditHistoryType.DebetBasic:
			{
				//
			}
		case dengi2.CreditHistoryType.DebetPercent:
			{
				/*				Проценты := mfo1c.Проценты{}
								Проценты.ДатаПлатежа = ch.Stamp.Time.Format(time.RFC3339)
								//Проценты.НачалоПериода = ch.Stamp.Time.String()
								//Проценты.КонецПериода = ch.Stamp.Time.String()
								Проценты.Сумма = ch.Basicvalue
								alc.ГрафикПлатежей.Проценты = append(alc.ГрафикПлатежей.Проценты, Проценты)*/
			}
		case dengi2.CreditHistoryType.DebetPenalty:
			{
				/*				Проценты := mfo1c.Проценты{}
								Проценты.ДатаПлатежа = ch.Stamp.Time.Format(time.RFC3339)
								//Проценты.НачалоПериода = ch.Stamp.Time.String()
								//Проценты.КонецПериода = ch.Stamp.Time.String()
								Проценты.Сумма = ch.Basicvalue
								alc.ГрафикПлатежей.Проценты = append(alc.ГрафикПлатежей.Проценты, Проценты)*/
			}
		case dengi2.CreditHistoryType.DebetPenaltyOnce:
			{
				//
			}
		case dengi2.CreditHistoryType.DebetDuty:
			{
				//
			}
		case dengi2.CreditHistoryType.DebetExcessive:
			{
				//
			}
		}
	}

	bocs.Params.Займы = append(bocs.Params.Займы, Займ)
	return nil
}

func TransferCreditDocument(cd *dengi2.CreditDocument, m *mfo1c.Mfo1c) error {
	alc := mfo1c.API_loan_create{}
	alc.Params.Идентификатор = cd.Identifier.String()
	//alc.Params.ВнешниеДанные = cd.Identifier.String()
	alc.Params.Реквизиты.Контрагент = cd.C_Owner.String() //всегда берем владельца анкеты
	alc.Params.Реквизиты.Организация = mfo1c.MKKINN
	if true || false || !cd.D_IsVirtual {
		alc.Params.Реквизиты.ПодразделениеОрганизации = cd.Dealer.String()
	}

	alc.Params.Реквизиты.ДатаДоговора = cd.Stamp.Time.Format(time.RFC3339)
	alc.Params.Реквизиты.ДатаВыдачи = cd.Beginstamp.Time.Format(time.RFC3339)
	//alc.Params.Реквизиты.ДатаОкончания = cd.CloseTime.Time.Format(time.RFC3389)
	alc.Params.Реквизиты.НомерДоговора = cd.Numberpublic.String
	alc.Params.Реквизиты.СуммаЗайма = int64(cd.Basicvalue.Float64)

	alc.Params.Реквизиты.ПроцентнаяСтавка = cd.Percent.Float64
	alc.Params.Реквизиты.СрокЗайма = int64(cd.Period.Int32)

	alc.Params.Реквизиты.ФинансовыйПродукт = fmt.Sprintf("Продукт_%d_%g_%d_%d_%d",
		cd.Period.Int32,
		cd.Percent.Float64,
		func(i int32) int {
			if i != 0 {
				return 1
			} else {
				return 0
			}
		}(cd.Zeroday.Int32),
		cd.Trialdays.Int32,
		cd.Requireddays.Int32)
	alc.Params.Реквизиты.РасчетНачисленийВоВнешнейПрограмме = false //true
	alc.Params.Реквизиты.ПоказательДолговойНагрузки = cd.C_PDN.Float64
	//alc.Params.Реквизиты.ПереноситьДатуВыдачиНаСледующийРабочийДень = false
	//alc.Params.Реквизиты.ОбеспеченныйЗаймЗалоги = false
	alc.Params.Реквизиты.ОбеспеченныйЗаймПоручительство = false
	//	alc.Params.Реквизиты.БанковскийСчетКонтрагента = ""

	//alc.Params.ГрафикПлатежей.ЭффективнаяСтавкаПроцента = cd.Identifier.String()
	//alc.Params.ГрафикПлатежей.ПолнаяСтоимостьЗайма = cd.Identifier.String()

	//alc.Params.ГрафикПлатежей.Идентификатор = cd.Identifier.String() + "_" + time.Now().Format(time.RFC3339)
	for _, ch := range cd.CreditHistory {
		switch ch.Historytype + 100 {
		case dengi2.CreditHistoryType.Credit:
			{
				/*ОсновнойДолг := mfo1c.ОсновнойДолг{}
				ОсновнойДолг.ДатаПлатежа = ch.Stamp.Time.Format(time.RFC3339)
				ОсновнойДолг.ПогашениеОсновногоДолга = ch.Percentbasicvalue.Float64
				alc.Params.ГрафикПлатежей.ОсновнойДолг = append(alc.Params.ГрафикПлатежей.ОсновнойДолг, ОсновнойДолг)*/

				Проценты := mfo1c.Проценты{}
				Проценты.ДатаПлатежа = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.НачалоПериода = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.КонецПериода = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.НачалоПериодаРасчета = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.КонецПериодаРасчета = ch.Stamp.Time.Format(time.RFC3339)
				Проценты.Сумма = ch.Perperiodvalue.Float64 + 0.56
				alc.Params.ГрафикПлатежей.Проценты = append(alc.Params.ГрафикПлатежей.Проценты, Проценты)
			}
		case dengi2.CreditHistoryType.CreditPenalty:
			{
				//alc.Params.ГрафикПлатежей.

			}
		case dengi2.CreditHistoryType.CreditDuty:
			{
				//alc.Params.ГрафикПлатежей.

			}
		case dengi2.CreditHistoryType.CreditExcessive:
			{
				//
			}
		case dengi2.CreditHistoryType.DebetBasic:
			{
				//
			}
		case dengi2.CreditHistoryType.DebetPercent:
			{
				/*				Проценты := mfo1c.Проценты{}
								Проценты.ДатаПлатежа = ch.Stamp.Time.Format(time.RFC3339)
								//Проценты.НачалоПериода = ch.Stamp.Time.String()
								//Проценты.КонецПериода = ch.Stamp.Time.String()
								Проценты.Сумма = ch.Basicvalue
								alc.Params.ГрафикПлатежей.Проценты = append(alc.Params.ГрафикПлатежей.Проценты, Проценты)*/
			}
		case dengi2.CreditHistoryType.DebetPenalty:
			{
				/*				Проценты := mfo1c.Проценты{}
								Проценты.ДатаПлатежа = ch.Stamp.Time.Format(time.RFC3339)
								//Проценты.НачалоПериода = ch.Stamp.Time.String()
								//Проценты.КонецПериода = ch.Stamp.Time.String()
								Проценты.Сумма = ch.Basicvalue
								alc.Params.ГрафикПлатежей.Проценты = append(alc.Params.ГрафикПлатежей.Проценты, Проценты)*/
			}
		case dengi2.CreditHistoryType.DebetPenaltyOnce:
			{
				//
			}
		case dengi2.CreditHistoryType.DebetDuty:
			{
				//
			}
		case dengi2.CreditHistoryType.DebetExcessive:
			{
				//
			}
		}
	}

	return m.Obtain(&alc)
}
