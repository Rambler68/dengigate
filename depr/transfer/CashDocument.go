package transfer

import (
	"fmt"
	"time"

	"gitlab.com/stackend/dengigate/dengi2"
	"gitlab.com/stackend/dengigate/mfo1c"
)

/*
  CashType -> ВидНачисления
*/
var CashType1C = map[uint8]string{
	dengi2.CashType.Debet:             "",
	dengi2.CashType.DebetSystem:       "",
	dengi2.CashType.DebetBasic:        mfo1c.ВидНачисления.ОсновнойДолг,
	dengi2.CashType.DebetPercent:      mfo1c.ВидНачисления.Проценты, //mfo1c.ВидНачисления.Проценты,
	dengi2.CashType.DebetPenaltyOnce:  mfo1c.ВидНачисления.Проценты, //mfo1c.ВидНачисления.Штраф,
	dengi2.CashType.DebetPenalty:      mfo1c.ВидНачисления.Проценты, //mfo1c.ВидНачисления.Пени,
	dengi2.CashType.DebetService:      "",                           //mfo1c.ВидНачисления.Обслуживание,
	dengi2.CashType.DebetManager:      "",
	dengi2.CashType.DebetCashless:     "",
	dengi2.CashType.DebetShortage:     "",
	dengi2.CashType.DebetExcessive:    "",
	dengi2.CashType.DebetInsurance:    "", //mfo1c.ВидНачисления.Страховка,
	dengi2.CashType.DebetPostage:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyKass1:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyKass2:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzIsk1:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzIsk2:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredCompl:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzCompl: mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzApel:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredKass:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOther:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredDover:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzKass:  mfo1c.ВидНачисления.КомиссииГоспошлина,
}

var CreditHistoryType1C = map[uint16]string{
	dengi2.CreditHistoryType.Credit:             mfo1c.ВидНачисления.Проценты,
	dengi2.CreditHistoryType.CreditPenaltyOnce:  mfo1c.ВидНачисления.Штраф,
	dengi2.CreditHistoryType.CreditPenalty:      mfo1c.ВидНачисления.Проценты,
	dengi2.CreditHistoryType.CreditService:      mfo1c.ВидНачисления.Проценты,
	dengi2.CreditHistoryType.CreditDuty:         mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditExcessive:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyKass1:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyKass2:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOtzIsk1:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOtzIsk2:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredCompl:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOtzCompl: mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOtzApel:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredKass:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOther:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.F_119:              mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.F_125:              mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPostage:      mfo1c.ВидНачисления.КомиссииГоспошлина,
}

func AppendBatchCashDocument(cd *dengi2.CashDocument, bocs *mfo1c.API_batch_objects_create_sync) error {
	return nil
}

func TransferCashDocument(cd *dengi2.CashDocument, m *mfo1c.Mfo1c) error {
	TransId := fmt.Sprintf("cash_%s", cd.Identifier.String())
	PaymentForm := fmt.Sprintf("Касса_%s", cd.Dealer.String())

	switch cd.Cashtype {
	case dengi2.CashType.CreditBasic:
		{
			alic := mfo1c.API_loan_issue_create{}
			alic.Params.ВнешниеДанные = TransId
			alic.Params.ИдентификаторТранзакции = TransId
			alic.Params.Дата = cd.Stamp.Time.Format(time.RFC3339)
			alic.Params.ВидОперации = mfo1c.ВидОперации.ВыдачаЗайма
			alic.Params.Организация = mfo1c.MKKINN
			alic.Params.ПодразделениеОрганизации = cd.Dealer.String()
			//if !cd.D_IsVirtual {			}
			alic.Params.Контрагент = cd.C_Owner.String()
			alic.Params.Займ = cd.Owner.String()
			alic.Params.СуммаПлатежа = cd.Cashvalue
			alic.Params.ФормаОплаты = PaymentForm
			alic.Params.НомерВходящегоДокументаОплаты = cd.Number
			alic.Params.ДатаВходящегоДокументаОплаты = cd.Stamp.Time.Format(time.RFC3339)
			alic.Params.СодержаниеВходящегоДокументаОплаты = fmt.Sprintf("Перевод денежных средств по договору займа №%s", cd.Number)
			alic.Params.ФормироватьВДатуВыдачиПоДоговору = false
			return m.Obtain(&alic)
		}
	case dengi2.CashType.DebetBasic*0 + 200:
		{
			alrp := mfo1c.API_loan_repayment_create{}
			alrp.Params.ВнешниеДанные = TransId
			alrp.Params.ИдентификаторТранзакции = TransId
			alrp.Params.Дата = cd.Stamp.Time.Format(time.RFC3339)
			alrp.Params.Организация = mfo1c.MKKINN
			alrp.Params.ПодразделениеОрганизации = cd.Dealer.String()
			alrp.Params.Контрагент = cd.C_Owner.String()
			alrp.Params.Займ = cd.Owner.String()
			alrp.Params.СуммаПлатежа = cd.Cashvalue
			alrp.Params.ФормаОплаты = PaymentForm
			alrp.Params.НомерВходящегоДокументаОплаты = cd.Number
			alrp.Params.ДатаВходящегоДокументаОплаты = cd.Stamp.Time.Format(time.RFC3339)
			alrp.Params.СодержаниеВходящегоДокументаОплаты = fmt.Sprintf("Поступление денежных средств по договору займа №%s", cd.Number)
			alrp.Params.ФормироватьГрафикиПриЧастичноДосрочномПогашении = true
			return m.Obtain(&alrp)
		}
	case dengi2.CashType.DebetBasic,
		dengi2.CashType.DebetPercent,
		dengi2.CashType.DebetPenaltyOnce,
		dengi2.CashType.DebetPenalty,
		dengi2.CashType.DebetBasic:
		{
			alra := mfo1c.API_loan_repayment_arbitrary_create{}
			alra.Params.ВнешниеДанные = TransId
			alra.Params.Идентификатор = alra.Params.ВнешниеДанные
			alra.Params.Реквизиты.Дата = cd.Stamp.Time.Format(time.RFC3339)
			alra.Params.Реквизиты.Организация = mfo1c.MKKINN
			alra.Params.Реквизиты.ПодразделениеОрганизации = cd.Dealer.String()

			Платеж := mfo1c.Платеж{}
			Платеж.ВидНачисления = CashType1C[cd.Cashtype]
			Платеж.ВидПогашения = mfo1c.ВидПогашения.ПлановоеПогашение
			Платеж.Займ = cd.Owner.String()
			Платеж.Контрагент = cd.C_Owner.String()
			Платеж.ФормаОплаты = PaymentForm
			Платеж.СуммаПлатежа = cd.Cashvalue
			Платеж.НомерВходящегоДокументаОплаты = cd.Number
			Платеж.ДатаПлатежа = cd.Stamp.Time.Format(time.RFC3339)
			Платеж.ПоСудебномуРешению = false //true Если платеж имеется в истории долга в статусе Юрист|Пристав

			alra.Params.Платежи = append(alra.Params.Платежи, Платеж)
			return m.Obtain(&alra)
		}
	case dengi2.CashType.DebetInsurance:
		{

			return nil
		}
	default:
		{
			return fmt.Errorf(`unknown CashType %d`, cd.Cashtype)
		}

	}
}
