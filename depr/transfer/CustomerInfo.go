package transfer

import (
	"strings"

	"gitlab.com/stackend/dengigate/dengi2"
	"gitlab.com/stackend/dengigate/mfo1c"
)

func AppendBatchCustomer(ci *dengi2.CustomerInfo, bocs *mfo1c.API_batch_objects_create_sync) error {
	Контрагент := mfo1c.Контрагент{}

	Контрагент.Идентификатор = ci.Identifier.String()

	/*ИсторияФИО := mfo1c.ИсторияФИО{
		ДействуетС: `1899-12-31T00:00:00`,
		Имя:        ci.Name,
		Отчество:   ci.Middlename,
		Фамилия:    ci.Surname,
	}
	Контрагент.ИсторияФИО = append(Контрагент.ИсторияФИО, ИсторияФИО)*/

	if ci.PhoneMobile.String != "" {
		КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
		КонтактнаяИнформацияТелефон.Вид = "МобильныйТелефон"
		КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
		КонтактнаяИнформацияТелефон.Значение.Value = ci.PhoneMobile.String
		Контрагент.КонтактнаяИнформацияТелефон = append(Контрагент.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
	}
	if ci.PhoneWork.String != "" {
		КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
		КонтактнаяИнформацияТелефон.Вид = "РабочийТелефон"
		КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
		КонтактнаяИнформацияТелефон.Значение.Value = ci.PhoneWork.String
		Контрагент.КонтактнаяИнформацияТелефон = append(Контрагент.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
	}
	if ci.PhoneHome.String != "" {
		КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
		КонтактнаяИнформацияТелефон.Вид = "ДомашнийТелефон"
		КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
		КонтактнаяИнформацияТелефон.Значение.Value = ci.PhoneHome.String
		Контрагент.КонтактнаяИнформацияТелефон = append(Контрагент.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
	}
	/*	if ci.PhoneOther.String != "" {
		0		КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
				КонтактнаяИнформацияТелефон.Вид = "ДругойТелефон"
				КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
				КонтактнаяИнформацияТелефон.Значение.Value = ci.PhoneOther.String
				Контрагент.КонтактнаяИнформацияТелефон = append(Контрагент.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
			}*/

	if ci.EMail.String != "" {
		КонтактнаяИнформацияЭлектроннаяПочта := mfo1c.КонтактнаяИнформацияЭлектроннаяПочта{}
		КонтактнаяИнформацияЭлектроннаяПочта.Вид = "АдресЭлектроннойПочты"
		КонтактнаяИнформацияЭлектроннаяПочта.Значение.Value = ci.EMail.String
		КонтактнаяИнформацияЭлектроннаяПочта.Значение.Type = "АдресЭлектроннойПочты"
		Контрагент.КонтактнаяИнформацияЭлектроннаяПочта = append(Контрагент.КонтактнаяИнформацияЭлектроннаяПочта, КонтактнаяИнформацияЭлектроннаяПочта)
	}

	AddrReg := ci.AddrReg()
	if AddrReg != "" {
		КонтактнаяИнформацияАдрес := mfo1c.КонтактнаяИнформацияАдрес{}
		КонтактнаяИнформацияАдрес.Вид = "ПочтовыйАдрес"
		КонтактнаяИнформацияАдрес.Значение.Value = AddrReg
		КонтактнаяИнформацияАдрес.Значение.AddressType = "ВСвободнойФорме"
		/*	КонтактнаяИнформацияАдрес.Значение.Area = ci.AddrRegArea
			КонтактнаяИнформацияАдрес.Значение.AreaType = ci.AddrRegArea
			КонтактнаяИнформацияАдрес.Значение.City = ci.AddrRegTown
			КонтактнаяИнформацияАдрес.Значение.Street = ci.AddrRegStreet
			КонтактнаяИнформацияАдрес.Значение.HouseType = ci.AddrRegHouse
			КонтактнаяИнформацияАдрес.Значение.Street = ci.AddrRegStreet
			КонтактнаяИнформацияАдрес.Значение.MunDistrict = "р-н"
			КонтактнаяИнформацияАдрес.Значение.MunDistrictType = "р-н"

			КонтактнаяИнформацияАдрес.Значение.AddressType = "Муниципальный"*/
		Контрагент.КонтактнаяИнформацияАдрес = append(Контрагент.КонтактнаяИнформацияАдрес, КонтактнаяИнформацияАдрес)
	}

	/*КонтактнаяИнформацияАдрес.Вид = "АдресПоПрописке"
	КонтактнаяИнформацияАдрес.Значение.Area = ci.AddrFactArea
	КонтактнаяИнформацияАдрес.Значение.City = ci.AddrFactTown
	КонтактнаяИнформацияАдрес.Значение.Street = ci.AddrFactStreet
	КонтактнаяИнформацияАдрес.Значение.HouseType = ci.AddrFactHouse
	КонтактнаяИнформацияАдрес.Значение.Street = ci.AddrFactStreet
	Контрагент.КонтактнаяИнформацияАдрес = append(Контрагент.КонтактнаяИнформацияАдрес, КонтактнаяИнформацияАдрес)
	*/

	/*
		КонтактнаяИнформацияАдрес.Вид = "ПочтовыйАдрес"
		КонтактнаяИнформацияАдрес.Значение.Value = ci.AddrFact()
		КонтактнаяИнформацияАдрес.Значение.AddressType = "ВСвободнойФорме"
	*/

	FIO := []string{}

	FIO = strings.Split(ci.Contact1Fio.String, " ")
	if len(FIO) == 3 {
		КонтактныеЛица := mfo1c.КонтактныеЛица{}
		КонтактныеЛица.Фамилия = FIO[0]
		КонтактныеЛица.Имя = FIO[1]
		КонтактныеЛица.Отчество = FIO[2]
		КонтактныеЛица.ВидКонтактногоЛица = "ЛичныйКонтакт"
		КонтактныеЛица.Описание = ci.Contact1Title.String
		if ci.Contact1Phone.String != "" {
			КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
			КонтактнаяИнформацияТелефон.Вид = "МобильныйТелефон"
			КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
			КонтактнаяИнформацияТелефон.Значение.Value = ci.Contact1Phone.String
			КонтактныеЛица.КонтактнаяИнформацияТелефон = append(КонтактныеЛица.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
		}
		Контрагент.КонтактныеЛица = append(Контрагент.КонтактныеЛица, КонтактныеЛица)
	}

	FIO = strings.Split(ci.Contact2Fio.String, " ")
	if len(FIO) == 3 {
		КонтактныеЛица := mfo1c.КонтактныеЛица{}
		КонтактныеЛица.Фамилия = FIO[0]
		КонтактныеЛица.Имя = FIO[1]
		КонтактныеЛица.Отчество = FIO[2]
		КонтактныеЛица.ВидКонтактногоЛица = "ЛичныйКонтакт"
		КонтактныеЛица.Описание = ci.Contact2Title.String
		if ci.Contact2Phone.String != "" {
			КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
			КонтактнаяИнформацияТелефон.Вид = "МобильныйТелефон"
			КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
			КонтактнаяИнформацияТелефон.Значение.Value = ci.Contact2Phone.String
			КонтактныеЛица.КонтактнаяИнформацияТелефон = append(КонтактныеЛица.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
		}
		Контрагент.КонтактныеЛица = append(Контрагент.КонтактныеЛица, КонтактныеЛица)
	}

	FIO = strings.Split(ci.Contact3Fio.String, " ")
	if len(FIO) == 3 {
		КонтактныеЛица := mfo1c.КонтактныеЛица{}
		КонтактныеЛица.Фамилия = FIO[0]
		КонтактныеЛица.Имя = FIO[1]
		КонтактныеЛица.Отчество = FIO[2]
		КонтактныеЛица.ВидКонтактногоЛица = "ЛичныйКонтакт"
		КонтактныеЛица.Описание = ci.Contact3Title.String
		if ci.Contact3Phone.String != "" {
			КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
			КонтактнаяИнформацияТелефон.Вид = "МобильныйТелефон"
			КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
			КонтактнаяИнформацияТелефон.Значение.Value = ci.Contact3Phone.String
			КонтактныеЛица.КонтактнаяИнформацияТелефон = append(КонтактныеЛица.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
		}
		Контрагент.КонтактныеЛица = append(Контрагент.КонтактныеЛица, КонтактныеЛица)
	}

	Контрагент.Реквизиты.Имя = ci.Name.String
	Контрагент.Реквизиты.Отчество = ci.Middlename.String
	Контрагент.Реквизиты.Фамилия = ci.Surname.String
	Контрагент.Реквизиты.ДатаРождения = ci.BirthDate.String
	Контрагент.Реквизиты.КоличествоДетей = ci.FamGuardianCount.Int64
	switch ci.Gender.String {
	case "жен.":
		Контрагент.Реквизиты.Пол = "Женский"
	case "муж.":
		Контрагент.Реквизиты.Пол = "Мужской"
	}
	Контрагент.Реквизиты.СреднемесячныйДоход = ci.FamMonthIncome.Int64
	Контрагент.Реквизиты.СтраховойНомерПФР = ci.DocSnils.String
	Контрагент.Реквизиты.Инн = ci.DocInn.String
	Контрагент.Реквизиты.МестоРаботы = ci.JobOrganizationName.String
	//		Контрагент.Реквизиты.Должность = ci.JobOrganizationPosition

	Документы := mfo1c.Документы{}
	Документы.ВидДокумента = "ПаспортРФ"
	Документы.Серия = ci.Doc1Serial.String
	Документы.Номер = ci.Doc1Number.String
	Документы.ДатаВыдачи = ci.Doc1Stamp.String
	Документы.КемВыдан = ci.Doc1Organization.String
	Документы.КодПодразделения = ci.Doc1OrganizationCode.String
	Документы.ЯвляетсяДокументомУдостоверяющимЛичность = true
	Контрагент.Документы = append(Контрагент.Документы, Документы)

	bocs.Params.Контрагенты = append(bocs.Params.Контрагенты, Контрагент)

	return nil
}

func TranferCustomerInfo(ci *dengi2.CustomerInfo, m *mfo1c.Mfo1c) error {
	ac := mfo1c.API_contractor_create_private_customer{}
	ac.Params.Идентификатор = ci.Identifier.String()
	ac.Params.ВнешниеДанные = ci.Identifier.String()

	/*ИсторияФИО := mfo1c.ИсторияФИО{
		ДействуетС: `1899-12-31T00:00:00`,
		Имя:        ci.Name,
		Отчество:   ci.Middlename,
		Фамилия:    ci.Surname,
	}
	ac.Params.ИсторияФИО = append(ac.Params.ИсторияФИО, ИсторияФИО)*/

	if ci.PhoneMobile.String != "" {
		КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
		КонтактнаяИнформацияТелефон.Вид = "МобильныйТелефон"
		КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
		КонтактнаяИнформацияТелефон.Значение.Value = ci.PhoneMobile.String
		ac.Params.КонтактнаяИнформацияТелефон = append(ac.Params.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
	}
	if ci.PhoneWork.String != "" {
		КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
		КонтактнаяИнформацияТелефон.Вид = "РабочийТелефон"
		КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
		КонтактнаяИнформацияТелефон.Значение.Value = ci.PhoneWork.String
		ac.Params.КонтактнаяИнформацияТелефон = append(ac.Params.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
	}
	if ci.PhoneHome.String != "" {
		КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
		КонтактнаяИнформацияТелефон.Вид = "ДомашнийТелефон"
		КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
		КонтактнаяИнформацияТелефон.Значение.Value = ci.PhoneHome.String
		ac.Params.КонтактнаяИнформацияТелефон = append(ac.Params.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
	}
	/*	if ci.PhoneOther.String != "" {
		КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
		КонтактнаяИнформацияТелефон.Вид = "ДругойТелефон"
		КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
		КонтактнаяИнформацияТелефон.Значение.Value = ci.PhoneOther.String
		ac.Params.КонтактнаяИнформацияТелефон = append(ac.Params.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
	}*/

	if ci.EMail.String != "" {
		КонтактнаяИнформацияЭлектроннаяПочта := mfo1c.КонтактнаяИнформацияЭлектроннаяПочта{}
		КонтактнаяИнформацияЭлектроннаяПочта.Вид = "АдресЭлектроннойПочты"
		КонтактнаяИнформацияЭлектроннаяПочта.Значение.Value = ci.EMail.String
		КонтактнаяИнформацияЭлектроннаяПочта.Значение.Type = "АдресЭлектроннойПочты"
		ac.Params.КонтактнаяИнформацияЭлектроннаяПочта = append(ac.Params.КонтактнаяИнформацияЭлектроннаяПочта, КонтактнаяИнформацияЭлектроннаяПочта)
	}

	AddrReg := ci.AddrReg()
	if AddrReg != "" {
		КонтактнаяИнформацияАдрес := mfo1c.КонтактнаяИнформацияАдрес{}
		КонтактнаяИнформацияАдрес.Вид = "ПочтовыйАдрес"
		КонтактнаяИнформацияАдрес.Значение.Value = AddrReg
		КонтактнаяИнформацияАдрес.Значение.AddressType = "ВСвободнойФорме"
		/*	КонтактнаяИнформацияАдрес.Значение.Area = ci.AddrRegArea
			КонтактнаяИнформацияАдрес.Значение.AreaType = ci.AddrRegArea
			КонтактнаяИнформацияАдрес.Значение.City = ci.AddrRegTown
			КонтактнаяИнформацияАдрес.Значение.Street = ci.AddrRegStreet
			КонтактнаяИнформацияАдрес.Значение.HouseType = ci.AddrRegHouse
			КонтактнаяИнформацияАдрес.Значение.Street = ci.AddrRegStreet
			КонтактнаяИнформацияАдрес.Значение.MunDistrict = "р-н"
			КонтактнаяИнформацияАдрес.Значение.MunDistrictType = "р-н"

			КонтактнаяИнформацияАдрес.Значение.AddressType = "Муниципальный"*/
		ac.Params.КонтактнаяИнформацияАдрес = append(ac.Params.КонтактнаяИнформацияАдрес, КонтактнаяИнформацияАдрес)
	}

	/*КонтактнаяИнформацияАдрес.Вид = "АдресПоПрописке"
	КонтактнаяИнформацияАдрес.Значение.Area = ci.AddrFactArea
	КонтактнаяИнформацияАдрес.Значение.City = ci.AddrFactTown
	КонтактнаяИнформацияАдрес.Значение.Street = ci.AddrFactStreet
	КонтактнаяИнформацияАдрес.Значение.HouseType = ci.AddrFactHouse
	КонтактнаяИнформацияАдрес.Значение.Street = ci.AddrFactStreet
	ac.Params.КонтактнаяИнформацияАдрес = append(ac.Params.КонтактнаяИнформацияАдрес, КонтактнаяИнформацияАдрес)
	*/

	/*
		КонтактнаяИнформацияАдрес.Вид = "ПочтовыйАдрес"
		КонтактнаяИнформацияАдрес.Значение.Value = ci.AddrFact()
		КонтактнаяИнформацияАдрес.Значение.AddressType = "ВСвободнойФорме"
	*/

	FIO := []string{}

	FIO = strings.Split(ci.Contact1Fio.String, " ")
	if len(FIO) == 3 {
		КонтактныеЛица := mfo1c.КонтактныеЛица{}
		КонтактныеЛица.Фамилия = FIO[0]
		КонтактныеЛица.Имя = FIO[1]
		КонтактныеЛица.Отчество = FIO[2]
		КонтактныеЛица.ВидКонтактногоЛица = "ЛичныйКонтакт"
		КонтактныеЛица.Описание = ci.Contact1Title.String
		if ci.Contact1Phone.String != "" {
			КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
			КонтактнаяИнформацияТелефон.Вид = "МобильныйТелефон"
			КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
			КонтактнаяИнформацияТелефон.Значение.Value = ci.Contact1Phone.String
			КонтактныеЛица.КонтактнаяИнформацияТелефон = append(КонтактныеЛица.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
		}
		ac.Params.КонтактныеЛица = append(ac.Params.КонтактныеЛица, КонтактныеЛица)
	}

	FIO = strings.Split(ci.Contact2Fio.String, " ")
	if len(FIO) == 3 {
		КонтактныеЛица := mfo1c.КонтактныеЛица{}
		КонтактныеЛица.Фамилия = FIO[0]
		КонтактныеЛица.Имя = FIO[1]
		КонтактныеЛица.Отчество = FIO[2]
		КонтактныеЛица.ВидКонтактногоЛица = "ЛичныйКонтакт"
		КонтактныеЛица.Описание = ci.Contact2Title.String
		if ci.Contact2Phone.String != "" {
			КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
			КонтактнаяИнформацияТелефон.Вид = "МобильныйТелефон"
			КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
			КонтактнаяИнформацияТелефон.Значение.Value = ci.Contact2Phone.String
			КонтактныеЛица.КонтактнаяИнформацияТелефон = append(КонтактныеЛица.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
		}
		ac.Params.КонтактныеЛица = append(ac.Params.КонтактныеЛица, КонтактныеЛица)
	}

	FIO = strings.Split(ci.Contact3Fio.String, " ")
	if len(FIO) == 3 {
		КонтактныеЛица := mfo1c.КонтактныеЛица{}
		КонтактныеЛица.Фамилия = FIO[0]
		КонтактныеЛица.Имя = FIO[1]
		КонтактныеЛица.Отчество = FIO[2]
		КонтактныеЛица.ВидКонтактногоЛица = "ЛичныйКонтакт"
		КонтактныеЛица.Описание = ci.Contact3Title.String
		if ci.Contact3Phone.String != "" {
			КонтактнаяИнформацияТелефон := mfo1c.КонтактнаяИнформацияТелефон{}
			КонтактнаяИнформацияТелефон.Вид = "МобильныйТелефон"
			КонтактнаяИнформацияТелефон.Значение.Type = "Телефон"
			КонтактнаяИнформацияТелефон.Значение.Value = ci.Contact3Phone.String
			КонтактныеЛица.КонтактнаяИнформацияТелефон = append(КонтактныеЛица.КонтактнаяИнформацияТелефон, КонтактнаяИнформацияТелефон)
		}
		ac.Params.КонтактныеЛица = append(ac.Params.КонтактныеЛица, КонтактныеЛица)
	}

	ac.Params.Реквизиты.Имя = ci.Name.String
	ac.Params.Реквизиты.Отчество = ci.Middlename.String
	ac.Params.Реквизиты.Фамилия = ci.Surname.String
	ac.Params.Реквизиты.ДатаРождения = ci.BirthDate.String
	ac.Params.Реквизиты.КоличествоДетей = ci.FamGuardianCount.Int64
	switch ci.Gender.String {
	case "жен.":
		ac.Params.Реквизиты.Пол = "Женский"
	case "муж.":
		ac.Params.Реквизиты.Пол = "Мужской"
	}
	ac.Params.Реквизиты.СреднемесячныйДоход = ci.FamMonthIncome.Int64
	ac.Params.Реквизиты.СтраховойНомерПФР = ci.DocSnils.String
	ac.Params.Реквизиты.Инн = ci.DocInn.String
	ac.Params.Реквизиты.МестоРаботы = ci.JobOrganizationName.String
	//		ac.Params.Реквизиты.Должность = ci.JobOrganizationPosition

	Документы := mfo1c.Документы{}
	Документы.ВидДокумента = "ПаспортРФ"
	Документы.Серия = ci.Doc1Serial.String
	Документы.Номер = ci.Doc1Number.String
	Документы.ДатаВыдачи = ci.Doc1Stamp.String
	Документы.КемВыдан = ci.Doc1Organization.String
	Документы.КодПодразделения = ci.Doc1OrganizationCode.String
	Документы.ЯвляетсяДокументомУдостоверяющимЛичность = true
	ac.Params.Документы = append(ac.Params.Документы, Документы)

	return m.Obtain(&ac)
}
