package transfer

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/stackend/dengigate/dengi2"
	"gitlab.com/stackend/dengigate/mfo1c"
)

func LoanToCpc(loan *dengi2.Loan, cpc *mfo1c.API_contractor_create_private_customer) error {
	cpc.Params.Идентификатор = loan.Customerowner.String()
	cpc.Params.Реквизиты.Имя = loan.LastName
	cpc.Params.Реквизиты.Отчество = loan.PatronymicName
	cpc.Params.Реквизиты.Фамилия = loan.FirstName
	cpc.Params.Реквизиты.ДатаРождения = loan.Birthday.Format(time.RFC3339)
	return nil

}

func LoanToBocs(loan *dengi2.Loan, bocs *mfo1c.API_batch_objects_create_sync, customer bool) error {
	//Клиент
	if customer {
		Контрагент := mfo1c.Контрагент{}
		Контрагент.Идентификатор = loan.Customerowner.String()
		Контрагент.Реквизиты.Имя = loan.LastName
		Контрагент.Реквизиты.Отчество = loan.PatronymicName
		Контрагент.Реквизиты.Фамилия = loan.FirstName
		Контрагент.Реквизиты.ДатаРождения = loan.Birthday.Format(time.RFC3339)
		bocs.Params.Контрагенты = append(bocs.Params.Контрагенты, Контрагент)
	}

	//Займ
	Займ := mfo1c.Займ{}
	Займ.Идентификатор = loan.CjID.String()
	Займ.Реквизиты.Контрагент = loan.Customerowner.String() //всегда берем владельца анкеты
	Займ.Реквизиты.Организация = mfo1c.MKKINN
	Займ.Реквизиты.ПодразделениеОрганизации = loan.DealerId.String()
	Займ.Реквизиты.ДатаДоговора = loan.Stamp.Format(time.RFC3339)
	Займ.Реквизиты.ДатаВыдачи = loan.Stamp.Format(time.RFC3339)
	//Займ.Реквизиты.ДатаВыдачи = time.Date(2020, 7, 1, 8, 0, 0, 0, time.Local).Format(time.RFC3339)
	Займ.Реквизиты.ДатаОкончания = loan.Stamp.AddDate(0, 0, int(loan.Perioddays)).Format(time.RFC3339)
	//Займ.Реквизиты.ДатаОкончания = loan.Stamp.AddDate(0, 0, -91).Format(time.RFC3339)
	Займ.Реквизиты.НомерДоговора = loan.Number
	Займ.Реквизиты.СуммаЗайма = int64(loan.Basicvalue)
	Займ.Реквизиты.ПроцентнаяСтавка = loan.Percent
	/*
		СрокЗайма передается как кол-во истекших дней?-потом кол-во истекших дней увеличится, что делать с займом
	*/
	ОсновнойДолг := mfo1c.ОсновнойДолг{}
	ОсновнойДолг.ДатаПлатежа = Займ.Реквизиты.ДатаОкончания
	ОсновнойДолг.ПогашениеОсновногоДолга = loan.Basicvalue
	Займ.ГрафикПлатежей.ОсновнойДолг = append(Займ.ГрафикПлатежей.ОсновнойДолг, ОсновнойДолг)

	Проценты := mfo1c.Проценты{}
	Проценты.ДатаПлатежа = Займ.Реквизиты.ДатаОкончания
	Проценты.НачалоПериода = Проценты.ДатаПлатежа
	Проценты.НачалоПериодаРасчета = Проценты.ДатаПлатежа
	Проценты.КонецПериода = Проценты.ДатаПлатежа
	Проценты.КонецПериодаРасчета = Проценты.ДатаПлатежа
	Проценты.Сумма = loan.Basicvalue * (loan.Percent / 100 * float64(loan.Perioddays))
	Займ.ГрафикПлатежей.Проценты = append(Займ.ГрафикПлатежей.Проценты, Проценты)

	Займ.ГрафикПлатежей.Идентификатор = loan.CjID.String() + "_" + loan.Stamp.Format(time.RFC3339)
	Займ.ГрафикПлатежей.ПолнаяСтоимостьЗайма = ОсновнойДолг.ПогашениеОсновногоДолга + Проценты.Сумма

	Займ.Реквизиты.СрокЗайма = loan.Perioddays
	Займ.Реквизиты.ФинансовыйПродукт = "Продукт"
	Займ.Реквизиты.РасчетНачисленийВоВнешнейПрограмме = true
	Займ.Реквизиты.ПоказательДолговойНагрузки = loan.Pdn
	//Займ.Реквизиты.ПереноситьДатуВыдачиНаСледующийРабочийДень = false
	//Займ.Реквизиты.ОбеспеченныйЗаймЗалоги = false
	Займ.Реквизиты.ОбеспеченныйЗаймПоручительство = false
	//	Займ.Реквизиты.БанковскийСчетКонтрагента = ""

	//Займ.ГрафикПлатежей.ЭффективнаяСтавкаПроцента = cd.Identifier.String()
	//Займ.ГрафикПлатежей.ПолнаяСтоимостьЗайма = cd.Identifier.String()

	bocs.Params.Займы = append(bocs.Params.Займы, Займ)

	//Выдача
	TransId := fmt.Sprintf("cash_%s", loan.CjID.String())
	PaymentForm := fmt.Sprintf("Касса_%s", loan.DealerId.String())
	Выдача := mfo1c.Выдача{}
	Выдача.ВнешниеДанные = TransId
	Выдача.ИдентификаторТранзакции = TransId
	Выдача.Дата = loan.Stamp.Format(time.RFC3339)
	Выдача.ВидОперации = mfo1c.ВидОперации.ВыдачаЗайма
	Выдача.Организация = mfo1c.MKKINN
	Выдача.ПодразделениеОрганизации = loan.DealerId.String()
	Выдача.Контрагент = loan.Customerowner.String()
	Выдача.Займ = loan.CjID.String()
	Выдача.СуммаПлатежа = loan.Basicvalue
	Выдача.ФормаОплаты = PaymentForm
	Выдача.НомерВходящегоДокументаОплаты = loan.Number
	Выдача.ДатаВходящегоДокументаОплаты = loan.Stamp.Format(time.RFC3339)
	Выдача.СодержаниеВходящегоДокументаОплаты = fmt.Sprintf("Перевод денежных средств по договору займа №%s", loan.Number)
	Выдача.ФормироватьВДатуВыдачиПоДоговору = false
	bocs.Params.ВыдачиЗаймов = append(bocs.Params.ВыдачиЗаймов, Выдача)

	/*		case "ГрафикПлатежей":
			{
				ГрафикПлатежей := mfo1c.ГрафикПлатежей{}
				ГрафикПлатежей.ДатаНачала = loan.Stamp.Format(time.RFC3339)
				ГрафикПлатежей.ДатаОкончания = loan.Stamp.AddDate(0, 0, int(loan.Perioddays)).Format(time.RFC3339)
				ГрафикПлатежей.ОсновнойЗайм = loan.CjID.String()
				ГрафикПлатежей.НомерДоговора = loan.Number
				ГрафикПлатежей.ПорядковыйНомерПролонгации = 1
				bocs.Params.ГрафикиПлатежей = append(bocs.Params.ГрафикиПлатежей, ГрафикПлатежей)
			}
		}*/

	return nil
}

func AccrualToLac(accrual *dengi2.Accrual, lac *mfo1c.API_loan_accruals_create) error {
	Начисление := mfo1c.Начисление{}
	Начисление.ВидНачисления = CreditHistoryType1C[accrual.HistoryType]
	Начисление.ДатаПлатежа = accrual.Stamp.Format(time.RFC3339)
	Начисление.Контрагент = accrual.C_Owner.String()
	Начисление.Займ = accrual.CjId.String()
	Начисление.ОтраженоВРегламентированномУчете = true
	Начисление.СуммаПлатежа = accrual.Value

	lac.Params.ОсновныеНачисления = append(lac.Params.ОсновныеНачисления, Начисление)

	lac.Params.Реквизиты.Дата = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Организация = mfo1c.MKKINN
	lac.Params.Идентификатор = dengi2.Dbd_Identifier("Начисление", accrual.Stamp)
	lac.Params.Реквизиты.ОтражатьВБухгалтерскомУчете = true
	lac.Params.Реквизиты.ОтражатьВНалоговомУчете = true
	lac.Params.Реквизиты.ОтражатьПоСпециальнымРегистрам = true
	//lac.Params.Реквизиты.ПодразделениеОрганизации =

	return nil
}

func MapAccrualToLac(accrual *dengi2.Accrual, map_lac map[string]*mfo1c.API_loan_accruals_create) error {
	dt_key := accrual.Stamp.Format("2006-01-02")
	lac := map_lac[dt_key]
	if lac == nil {
		lac = &mfo1c.API_loan_accruals_create{}
		map_lac[dt_key] = lac
	}
	Начисление := mfo1c.Начисление{}
	Начисление.ВидНачисления = CreditHistoryType1C[accrual.HistoryType]
	Начисление.ДатаПлатежа = accrual.Stamp.Format(time.RFC3339)
	Начисление.Контрагент = accrual.C_Owner.String()
	Начисление.Займ = accrual.CjId.String()
	Начисление.ОтраженоВРегламентированномУчете = true
	Начисление.СуммаПлатежа = accrual.Value

	lac.Params.ОсновныеНачисления = append(lac.Params.ОсновныеНачисления, Начисление)

	lac.Params.Идентификатор = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Дата = accrual.Stamp.Format(time.RFC3339)
	lac.Params.Реквизиты.Организация = mfo1c.MKKINN
	lac.Params.Идентификатор = dengi2.Dbd_Identifier("Начисление", accrual.Stamp)
	lac.Params.Реквизиты.ОтражатьВБухгалтерскомУчете = true
	lac.Params.Реквизиты.ОтражатьВНалоговомУчете = true
	lac.Params.Реквизиты.ОтражатьПоСпециальнымРегистрам = true
	//lac.Params.Реквизиты.ПодразделениеОрганизации =

	return nil
}

func ProlongToLpc(prolong *dengi2.Prolong, lpc *mfo1c.API_loan_prolongation_create) error {
	lpc.Params.ВнешниеДанные = prolong.P_Identifier.String()
	lpc.Params.ДатаДопСоглашения = prolong.P_Stamp.Format(time.RFC3339)
	lpc.Params.ВидДополнительногоСоглашения = mfo1c.ВидДополнительногоСоглашения.Пролонгация
	lpc.Params.ДатаНачала = prolong.P_Stamp.Format(time.RFC3339)
	lpc.Params.ДатаОкончания = prolong.P_Stamp.AddDate(0, 0, prolong.P_Period).Format(time.RFC3339)
	lpc.Params.Идентификатор = prolong.P_Identifier.String()
	lpc.Params.ОсновнойЗайм = prolong.CJ_Identifier.String()
	lpc.Params.ПорядковыйНомерПролонгации = prolong.P_Index

	return nil
}

func CashToLrac(cash *dengi2.Cash, lrac *mfo1c.API_loan_repayment_arbitrary_create) error {
	ФормаОплаты := fmt.Sprintf("Касса_%s", cash.D_Identifier.String())
	ВидНачисления := CashType1C[cash.CashType]

	if ВидНачисления != "" {
		Платеж := mfo1c.Платеж{}
		Платеж.ВидНачисления = ВидНачисления
		Платеж.ДатаПлатежа = cash.Stamp.Format(time.RFC3339)
		Платеж.Контрагент = cash.C_Owner.String()
		Платеж.Займ = cash.CjId.String()
		Платеж.СуммаПлатежа = cash.CashValue
		Платеж.ФормаОплаты = ФормаОплаты
		Платеж.ВидПогашения = mfo1c.ВидПогашения.ПлановоеПогашение
		Платеж.НомерВходящегоДокументаОплаты = cash.Number
		//		Платеж.СодержаниеВходящегоДокументаОплаты = fmt.Sprintf("Погашение по договору займа %s", cash.CJ_Number)

		/*		switch cash.CJ_DocumentStatus {
					case dengi2.DocumentStatus.Close,
					dengi2.DocumentStatus.ClosePenalty,
					dengi2.DocumentStatus.
				}
			Платеж.ЗакрытьДоговор = cash.ClosedLoan*/
		//Платеж.ПоСудебномуРешению =
		//
		/*		if ВидНачисления = ВидыНачисления.ОсновнойДолг && cash.CJ_DocumentStatus = dengi3.DocumentStatus.Close
				Платеж.ЗакрытьДоговор = cash.ClosedLoan*/

		lrac.Params.Платежи = append(lrac.Params.Платежи, Платеж)
		lrac.Params.Реквизиты.Дата = cash.Stamp.Format(time.RFC3339)
		lrac.Params.Реквизиты.Организация = mfo1c.MKKINN
		lrac.Params.Идентификатор = dengi2.Dbd_Identifier("Погашение", cash.Stamp)
		return nil
	}
	alias, _ := dengi2.Alias(dengi2.CashType, int(cash.CashType))
	return fmt.Errorf("ignored cash type: %d (%s)", cash.CashType, alias)
}

func MapCashToLrac(cash *dengi2.Cash, map_lrac map[string]*mfo1c.API_loan_repayment_arbitrary_create) error {
	ФормаОплаты := fmt.Sprintf("Касса_%s", cash.D_Identifier.String())
	ВидНачисления := CashType1C[cash.CashType]

	if ВидНачисления != "" {
		dt_key := cash.Stamp.Format("2006-01-02")
		lrac := map_lrac[dt_key]
		if lrac == nil {
			lrac = &mfo1c.API_loan_repayment_arbitrary_create{}
			map_lrac[dt_key] = lrac
		}
		Платеж := mfo1c.Платеж{}
		Платеж.ВидНачисления = ВидНачисления
		Платеж.ДатаПлатежа = cash.Stamp.Format(time.RFC3339)
		Платеж.Контрагент = cash.C_Owner.String()
		Платеж.Займ = cash.CjId.String()
		Платеж.СуммаПлатежа = cash.CashValue
		Платеж.ФормаОплаты = ФормаОплаты
		Платеж.ВидПогашения = mfo1c.ВидПогашения.ПлановоеПогашение
		Платеж.НомерВходящегоДокументаОплаты = cash.Number
		Платеж.СодержаниеВходящегоДокументаОплаты = fmt.Sprintf("Погашение по договору займа %s", cash.CJ_Number)
		log.Infof("%s %s", cash.Stamp.Format(time.RFC3339), cash.CJ_LastDebetStamp.Format(time.RFC3339))
		Платеж.ЗакрытьДоговор = cash.CashType == dengi2.CashType.DebetBasic &&
			((cash.CJ_DocumentStatus == dengi2.DocumentStatus.Close) || (cash.CJ_DocumentStatus == dengi2.DocumentStatus.ClosePenalty)) &&
			cash.CJ_SaldoBasicValue <= 0 &&
			cash.CJ_LastDebetStamp == cash.Stamp
		//Платеж.ПоСудебномуРешению =

		lrac.Params.Платежи = append(lrac.Params.Платежи, Платеж)
		lrac.Params.Реквизиты.Дата = cash.Stamp.Format(time.RFC3339)
		lrac.Params.Реквизиты.Организация = mfo1c.MKKINN
		lrac.Params.Идентификатор = dengi2.Dbd_Identifier("Погашение", cash.Stamp)
		return nil
	} else {
		alias, _ := dengi2.Alias(dengi2.CashType, int(cash.CashType))
		return fmt.Errorf("ignored cash type: %d (%s)", cash.CashType, alias)
	}
}
