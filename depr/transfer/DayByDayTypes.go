package transfer

import (
	"gitlab.com/stackend/dengigate/dengi2"
	"gitlab.com/stackend/dengigate/mfo1c"
)

/*
  CashType -> ВидНачисления
*/
var CashType1C = map[uint8]string{
	dengi2.CashType.Debet:             "",
	dengi2.CashType.DebetSystem:       "",
	dengi2.CashType.DebetBasic:        mfo1c.ВидНачисления.ОсновнойДолг,
	dengi2.CashType.DebetPercent:      mfo1c.ВидНачисления.Проценты, //mfo1c.ВидНачисления.Проценты,
	dengi2.CashType.DebetPenaltyOnce:  mfo1c.ВидНачисления.Проценты, //mfo1c.ВидНачисления.Штраф,
	dengi2.CashType.DebetPenalty:      mfo1c.ВидНачисления.Проценты, //mfo1c.ВидНачисления.Пени,
	dengi2.CashType.DebetService:      "",                           //mfo1c.ВидНачисления.Обслуживание,
	dengi2.CashType.DebetManager:      "",
	dengi2.CashType.DebetCashless:     "",
	dengi2.CashType.DebetShortage:     "",
	dengi2.CashType.DebetExcessive:    "",
	dengi2.CashType.DebetInsurance:    "", //mfo1c.ВидНачисления.Страховка,
	dengi2.CashType.DebetPostage:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyKass1:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyKass2:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetDutyNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzIsk1:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzIsk2:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredCompl:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzCompl: mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzApel:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredKass:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOther:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredDover:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CashType.DebetPredOtzKass:  mfo1c.ВидНачисления.КомиссииГоспошлина,
}

var CreditHistoryType1C = map[uint16]string{
	dengi2.CreditHistoryType.Credit:             mfo1c.ВидНачисления.Проценты,
	dengi2.CreditHistoryType.CreditPenaltyOnce:  mfo1c.ВидНачисления.Штраф,
	dengi2.CreditHistoryType.CreditPenalty:      mfo1c.ВидНачисления.Проценты,
	dengi2.CreditHistoryType.CreditService:      mfo1c.ВидНачисления.Проценты,
	dengi2.CreditHistoryType.CreditDuty:         mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditExcessive:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyKass1:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyKass2:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditDutyNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredPrik:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredIsk:      mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOtzIsk1:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOtzIsk2:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredCompl:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOtzCompl: mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredApel:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOtzApel:  mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredKass:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredNadz:     mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPredOther:    mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.F_119:              mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.F_125:              mfo1c.ВидНачисления.КомиссииГоспошлина,
	dengi2.CreditHistoryType.CreditPostage:      mfo1c.ВидНачисления.КомиссииГоспошлина,
}
