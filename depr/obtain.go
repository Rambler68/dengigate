func (m *Mfo1c) Obtain(ba BasicAPI) error {
	mt, mn, _, mp := ba.Method()
	uri := m.URI(ba)
	jsonp, err := json.Marshal(mp)
	if !Check(err) {
		return err
	}
	if m.LogData {
		f, _ := os.Create("c:\\temp\\" + mn + time.Now().Format(" 2006.01.02 15.04.05.Request"))
		f.WriteString(fmt.Sprintf("%s %s\n", mt, uri))
		/*		f.Write([]byte{'\n'})
				fmt.Fprintf(f, "%s %s\n", mt, uri)*/
		f.Write(jsonp)
		defer f.Close()
	}
	client := &http.Client{}
	request, err := http.NewRequest(mt, uri, bytes.NewBuffer(jsonp))
	if !Check(err) {
		return err
	}
	request.SetBasicAuth(m.Connection.Username, m.Connection.Password)
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	if !Check(err) {
		return err
	}
	defer response.Body.Close()
	rb, _ := io.ReadAll(response.Body)
	if response.StatusCode != http.StatusOK {
		j, _ := json.MarshalIndent(mp, "", "  ")
		return errors.New(mt + " " + uri + "\n" + response.Status + "\nRequest:" + string(j) + "\nResponse:" + string(rb))
	}
	if response.Header.Get("Content-Type") == "application/json" {
		//		err = json.NewDecoder(response.Body).Decode(&ba)
		err = json.Unmarshal(rb, &ba)
		if m.LogData {
			//log.Debugln(string(rb))
			//check(err)
			f, _ := os.Create("c:\\temp\\" + mn + time.Now().Format(" 2006.01.02 15.04.05.Response"))
			f.Write(rb)
			defer f.Close()
		}
		if !Check(err) {
			return err
		}
	}
	err1 := ba.Error()
	if err1 != nil {
		j, _ := json.MarshalIndent(mp, "", "  ")
		return errors.New(mt + " " + uri + "\n" + err1.Error() + "\nRequest:" + string(j) + "\nResponse:" + string(rb))
	}
	return nil
}

/*

	if organization && orgContract { // �� ���� ������ ������ ������ ���� �� ������
		reqorg, err := http.NewRequest("GET", fullOrgUrl, nil)
		reqorg.Header.Add("Authorization", "Basic "+basicAuth(uName, uPass))
		resorg, err := client.Do(reqorg)
		org_output := new(Organizations)
		err = json.NewDecoder(resorg.Body).Decode(&org_output) // ���������
		if err != nil {
			logrus.Println(err)
		}
		resorg.Body.Close()

		reqcon, err := http.NewRequest("GET", fullConUrl, nil)
		reqcon.Header.Add("Authorization", "Basic "+basicAuth(uName, uPass))
		rescon, err := client.Do(reqcon)
		con_output := new(Contracts)
		err = json.NewDecoder(rescon.Body).Decode(&con_output) // ���������
		if err != nil {
			logrus.Println(err)
		}
		rescon.Body.Close()

		return org_output, con_output

	} else if organization && orgContract == false {

*/





Errors := map[string]string{}
HandleErrors := func() (Result int, Text []byte) {
	if len(Errors) > 0 {
		for k, v := range Errors {
			log.Errorf("%s Error %s %s", procdate, k, v)
		}
		log.Errorf("%s Total %d Errors", procdate, len(Errors))
		Result = Dbd_Error
		Text, _ = json.MarshalIndent(Errors, "", " ")
	} else {
		Result = Dbd_NoError
		Text = []byte{}
	}
	return
}




/*
func (m *Mfo1c) APIErrors(ba BasicAPI, err error) (Error bool, Text []byte) {
	Errors := ba.Errors(err)
	if len(Errors) > 0 {
		for k, v := range Errors {
			log.Errorf("Error %s %s", k, v)
		}
		log.Errorf("Total %d Errors", len(Errors))
		Error = true
		Text, _ = json.MarshalIndent(Errors, "", " ")
	} else {
		Error = false
		Text = []byte{}
	}
	return
}
*/

func (m *Mfo1c) Process(ba BasicAPI) (req []byte, res []byte, err error) {
	mt, mn, _, mp := ba.Method()
	uri := m.URI(ba)
	if m.LogData {
		req, err = json.MarshalIndent(mp, "", "\t")
	} else {
		req, err = json.Marshal(mp)
	}
	if !Check(err) {
		return
	}
	if m.LogData {
		f, _ := os.Create("c:\\temp\\" + mn + time.Now().Format(" 2006.01.02 15.04.05.Request"))
		f.WriteString(fmt.Sprintf("%s %s\n", mt, uri))
		/*		f.Write([]byte{'\n'})
				fmt.Fprintf(f, "%s %s\n", mt, uri)*/
		f.Write(req)
		defer f.Close()
	}
	client := &http.Client{}
	request, err := http.NewRequest(mt, uri, bytes.NewBuffer(req))
	if !Check(err) {
		return
	}
	request.SetBasicAuth(m.Connection.Username, m.Connection.Password)
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	if !Check(err) {
		return
	}
	defer response.Body.Close()
	res, _ = io.ReadAll(response.Body)
	if response.StatusCode != http.StatusOK {
		err = fmt.Errorf("%s\n%s\n%s\n%s", mt, uri, response.Status, string(res))
		return
	}
	if response.Header.Get("Content-Type") == "application/json" {
		//		err = json.NewDecoder(response.Body).Decode(&ba)
		err = json.Unmarshal(res, &ba)
		if m.LogData {
			//log.Debugln(string(rb))
			//check(err)
			f, _ := os.Create("c:\\temp\\" + mn + time.Now().Format(" 2006.01.02 15.04.05.Response"))
			f.Write(res)
			defer f.Close()
		}
		if !Check(err) {
			return
		}
	}
	return
}

