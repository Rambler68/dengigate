// Read a whole file into the memory and store it as array of lines
func readLines(path string) (lines []string, err error) {
	var (
		file   *os.File
		part   []byte
		prefix bool
	)
	if file, err = os.Open(path); err != nil {
		return
	}
	reader := bufio.NewReader(file)
	buffer := bytes.NewBuffer(make([]byte, 1024))
	for {
		if part, prefix, err = reader.ReadLine(); err != nil {
			break
		}
		buffer.Write(part)
		if !prefix {
			lines = append(lines, buffer.String())
			buffer.Reset()
		}
	}
	file.Close()
	err = nil
	return
}

func (d *Dengi2) UFK_Test() {
	lines, err := readLines(`D:\2021\Служебные\UFK.csv`)
	if err != nil {
		fmt.Println("Error: %s\n", err)
		return
	}
	//re := regexp.MustCompile(`УФК по.*\(.*\)+?`)
	//re := regexp.MustCompile(`УФК ПО.*?\(`)
	for _, line := range lines {

		code, name, err := d.Dbd_SearchUFK(line)
		if err != nil {
			fmt.Println(line)
		}
		if false {
			fmt.Println(code, name)
		}

		/*		ufk := re.FindString(strings.ToUpper(line))

				ufk = strings.Replace(ufk, "(", "", 1)
				if ufk == "" {
					fmt.Print(line)
				}

				score := map[string]float64{}
				for k, v := range Dbd_UFK {
					score[k] = jwd.Calculate(strings.ToUpper(v), strings.ToUpper(ufk))
				}
				score_max := 0.0
				score_idx := ""
				for k, v := range score {
					//			fmt.Println(k, v)
					if v > score_max {
						score_max = v
						score_idx = k
					}
				}
				//fmt.Println(ufk, line)
				fmt.Println(ufk, " > ", Dbd_UFK[score_idx], " = ", score_max)*/
	}
}

//distance := levenshtein.ComputeDistance(line, Dbd_UFK["6200"])
//distance := gstr.Levenshtein(line, Dbd_UFK["6200"], 1, 1, 1)
