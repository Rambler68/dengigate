var 	signals      chan os.Signal


	/*	env string
		//	Db        *db.PgsqlDb
		egts_addr string

		db_driver      string
		egts_token_key string
		tls_cert_file  string
		tls_key_file   string
		tls_ca_file    string
	*/

func gor_signal(alldone chan bool) {
	signal := <-signals
	log.Warn(signal)
	alldone <- true
}

func Dbd_Now() {
	CurDate := time.Date(2021, 1, 1, 0, 0, 0, 0, time.Local)

	for CurDate.Before(time.Now()) {
		//gor_DayByDay(CurDate.Year(), int(CurDate.Month()), CurDate.Day())
		log.Print(CurDate.Format(time.RFC3339))
		CurDate = CurDate.AddDate(0, 0, 1)
	}
}


	/*
		//go gor()
		signals = make(chan os.Signal, 1)
		signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

		alldone := make(chan bool)
		go gor_signal(alldone)

		//gor_DayByDay(2020, 12, 31)
		//gor_DayByDay(2021, 1, 1)

		//	Dbd_Now()

		<-alldone
	*/
