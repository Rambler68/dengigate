package dengi2

import "database/sql"

type CashDocument struct {
	Active       interface{}    `db:"ACTIVE"`
	Cashtype     uint8          `db:"CASHTYPE"`
	Cashvalue    float64        `db:"CASHVALUE"`
	Dealer       GUID           `db:"DEALER"`
	Description  sql.NullString `db:"DESCRIPTION"`
	Employee     interface{}    `db:"EMPLOYEE"`
	EmployeeTxt  interface{}    `db:"EMPLOYEE_TXT"`
	Identifier   GUID           `db:"IDENTIFIER"`
	Manager      GUID           `db:"MANAGER"`
	Modified     sql.NullTime   `db:"MODIFIED"`
	Number       string         `db:"NUMBER"`
	Owner        GUID           `db:"OWNER"`
	Stamp        sql.NullTime   `db:"STAMP"`
	StampDelete  sql.NullTime   `db:"StampDelete"`
	UNLOAD1CDATE interface{}    `db:"UNLOAD1CDATE"`
	Unload1C     interface{}    `db:"Unload1C"`
	C_Identifier GUID           `db:"C_IDENTIFIER"`
	C_Owner      GUID           `db:"C_OWNER"`
	D_IsVirtual  bool           `db:"D_ISVIRTUAL"`
}

type CashDocumentHistory []CashDocument
