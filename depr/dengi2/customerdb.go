package dengi2

import "database/sql"

type CustomerInfo struct {
	Identifier                              GUID           `db:"Identifier"`
	Agreement3rdperson                      sql.NullString `db:"Agreement3rdperson"`
	BalanceMoneyIn                          sql.NullInt64  `db:"BalanceMoneyIn"`
	BalanceMoneyOut                         sql.NullInt64  `db:"BalanceMoneyOut"`
	EMail                                   sql.NullString `db:"EMail"`
	MandarinCardnum                         interface{}    `db:"MandarinCardnum"`
	NeedAmount                              sql.NullInt64  `db:"NeedAmount"`
	NumCard                                 sql.NullString `db:"NumCard"`
	ReportASPDisable                        interface{}    `db:"ReportASPDisable"`
	ScoreAllow                              sql.NullInt64  `db:"ScoreAllow"`
	SendCard                                sql.NullInt64  `db:"SendCard"`
	AccountNumber                           sql.NullString `db:"account_number"`
	AddrFactApartment1                      sql.NullString `db:"addr_fact_Apartment1"`
	AddrFactApartment2                      sql.NullString `db:"addr_fact_Apartment2"`
	AddrFactArea                            sql.NullString `db:"addr_fact_Area"`
	AddrFactHouse                           sql.NullString `db:"addr_fact_House"`
	AddrFactIndex                           sql.NullString `db:"addr_fact_Index"`
	AddrFactRegion                          sql.NullString `db:"addr_fact_Region"`
	AddrFactStreet                          sql.NullString `db:"addr_fact_Street"`
	AddrFactTown                            sql.NullString `db:"addr_fact_Town"`
	AddrFactTownRegion                      sql.NullString `db:"addr_fact_TownRegion"`
	AddrFactAdditionally                    sql.NullString `db:"addr_fact_additionally"`
	AddrOrgApartment1                       sql.NullString `db:"addr_org_Apartment1"`
	AddrOrgApartment2                       sql.NullString `db:"addr_org_Apartment2"`
	AddrOrgArea                             sql.NullString `db:"addr_org_Area"`
	AddrOrgHouse                            sql.NullString `db:"addr_org_House"`
	AddrOrgRegion                           sql.NullString `db:"addr_org_Region"`
	AddrOrgStreet                           sql.NullString `db:"addr_org_Street"`
	AddrOrgTown                             sql.NullString `db:"addr_org_Town"`
	AddrOrgTownRegion                       sql.NullString `db:"addr_org_TownRegion"`
	AddrRegApartment1                       sql.NullString `db:"addr_reg_Apartment1"`
	AddrRegApartment2                       sql.NullString `db:"addr_reg_Apartment2"`
	AddrRegArea                             sql.NullString `db:"addr_reg_Area"`
	AddrRegHouse                            sql.NullString `db:"addr_reg_House"`
	AddrRegIndex                            sql.NullString `db:"addr_reg_Index"`
	AddrRegRegion                           sql.NullString `db:"addr_reg_Region"`
	AddrRegStreet                           sql.NullString `db:"addr_reg_Street"`
	AddrRegTown                             sql.NullString `db:"addr_reg_Town"`
	AddrRegTownRegion                       sql.NullString `db:"addr_reg_TownRegion"`
	AddrRegAdditionally                     sql.NullString `db:"addr_reg_additionally"`
	Bik                                     sql.NullString `db:"bik"`
	BirthDate                               sql.NullString `db:"birth_date"`
	BirthPlace                              sql.NullString `db:"birth_place"`
	Contact1Fio                             sql.NullString `db:"contact1_fio"`
	Contact1Phone                           sql.NullString `db:"contact1_phone"`
	Contact1Title                           sql.NullString `db:"contact1_title"`
	Contact2Fio                             sql.NullString `db:"contact2_fio"`
	Contact2Phone                           sql.NullString `db:"contact2_phone"`
	Contact2Title                           sql.NullString `db:"contact2_title"`
	Contact3Fio                             sql.NullString `db:"contact3_fio"`
	Contact3Phone                           sql.NullString `db:"contact3_phone"`
	Contact3Title                           sql.NullString `db:"contact3_title"`
	CurrentBankruptcy                       sql.NullInt64  `db:"current_bankruptcy"`
	Doc1AddressReg                          sql.NullString `db:"doc1_address_reg"`
	Doc1Name                                sql.NullString `db:"doc1_name"`
	Doc1Number                              sql.NullString `db:"doc1_number"`
	Doc1Organization                        sql.NullString `db:"doc1_organization"`
	Doc1OrganizationCode                    sql.NullString `db:"doc1_organization_code"`
	Doc1Serial                              sql.NullString `db:"doc1_serial"`
	Doc1Stamp                               sql.NullString `db:"doc1_stamp"`
	Doc2Name                                sql.NullString `db:"doc2_name"`
	Doc2Number                              sql.NullString `db:"doc2_number"`
	Doc2Organization                        sql.NullString `db:"doc2_organization"`
	Doc2Serial                              sql.NullString `db:"doc2_serial"`
	Doc2Stamp                               sql.NullString `db:"doc2_stamp"`
	DocInn                                  sql.NullString `db:"doc_inn"`
	DocOther                                sql.NullString `db:"doc_other"`
	DocPens                                 sql.NullString `db:"doc_pens"`
	DocSnils                                sql.NullString `db:"doc_snils"`
	FamCreditTarget                         sql.NullString `db:"fam_CreditTarget"`
	FamFamilyIncome                         sql.NullInt64  `db:"fam_FamilyIncome"`
	FamGuardianCount                        sql.NullInt64  `db:"fam_GuardianCount"`
	FamMonthIncome                          sql.NullInt64  `db:"fam_MonthIncome"`
	Gender                                  sql.NullString `db:"gender"`
	IncomeSource                            sql.NullInt64  `db:"income_source"`
	JobOrganizationChief                    sql.NullString `db:"job_OrganizationChief"`
	JobOrganizationChiefPhone               sql.NullString `db:"job_OrganizationChiefPhone"`
	JobOrganizationDescription              sql.NullString `db:"job_OrganizationDescription"`
	JobOrganizationName                     sql.NullString `db:"job_OrganizationName"`
	JobOrganizationPosition                 sql.NullString `db:"job_OrganizationPosition"`
	Middlename                              sql.NullString `db:"middlename"`
	Name                                    sql.NullString `db:"name"`
	OldBankruptcy                           sql.NullInt64  `db:"old_bankruptcy"`
	OldBankruptcyDate                       sql.NullString `db:"old_bankruptcy_date"`
	OldBankruptcyType                       sql.NullInt64  `db:"old_bankruptcy_type"`
	PhoneHome                               sql.NullString `db:"phone_home"`
	PhoneMobile                             sql.NullString `db:"phone_mobile"`
	PhoneOther                              sql.NullString `db:"phone_other"`
	PhoneWork                               sql.NullString `db:"phone_work"`
	RiskBeneficiaryCollectionClassification sql.NullString `db:"risk_BeneficiaryCollectionClassification"`
	RiskLevel                               sql.NullString `db:"risk_level"`
	RiskModified                            sql.NullString `db:"risk_modified"`
	RiskName                                sql.NullString `db:"risk_name"`
	Surname                                 sql.NullString `db:"surname"`
}

func aif(v string) string {
	if v != "" {
		return ", " + v
	} else {
		return v
	}
}

func (ci *CustomerInfo) AddrReg() string {
	r := ci.AddrRegArea.String
	r += aif(ci.AddrRegRegion.String)
	r += aif(ci.AddrRegTown.String)
	r += aif(ci.AddrRegTownRegion.String)
	r += aif(ci.AddrRegStreet.String)
	r += aif(ci.AddrRegHouse.String)
	r += aif(ci.AddrRegApartment1.String)
	r += aif(ci.AddrRegApartment2.String)
	return r
}

func (ci *CustomerInfo) AddrFact() string {
	r := ci.AddrFactArea.String
	r += aif(ci.AddrFactRegion.String)
	r += aif(ci.AddrFactTown.String)
	r += aif(ci.AddrFactTownRegion.String)
	r += aif(ci.AddrFactStreet.String)
	r += aif(ci.AddrFactHouse.String)
	r += aif(ci.AddrFactApartment1.String)
	r += aif(ci.AddrFactApartment2.String)
	return r
}
