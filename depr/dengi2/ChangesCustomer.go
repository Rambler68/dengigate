package dengi2

import "fmt"

type ChangeCustomer struct {
	Change
	CustomerInfo
}

func (d *Dengi2) GetCustomerChanges(year int) ([]ChangeCustomer, error) {
	changes := []ChangeCustomer{}
	err := d.Db.Select(&changes,
		fmt.Sprintf(`
			DECLARE @STARTED DATETIME = GETDATE()

			UPDATE CA SET CA.STARTED=@STARTED
			FROM CHANGES CA(NOLOCK) 
			INNER JOIN (
				SELECT TOP 1000 CA.IDENTIFIER
				FROM CHANGES CA(NOLOCK)
				WHERE CA.COMPLETED IS NULL AND CA.ERRORED IS NULL AND DATEPART(YEAR, CA.STAMP)=%d AND CA.CHANGETYPE=%d
				ORDER BY CA.STAMP, CA.CHANGETYPE, CA.OWNER
			) CB ON CA.IDENTIFIER=CB.IDENTIFIER		
		
			SELECT 
				CA.IDENTIFIER CA_IDENTIFIER,
				CA.STAMP CA_STAMP,
				CA.CHANGETYPE CA_CHANGETYPE,
				CA.OWNER CA_OWNER,
				CA.SCHEDULED CA_SCHEDULED,
				CA.STARTED CA_STARTED,
				CA.COMPLETED CA_COMPLETED,
				CA.ERRORED CA_ERRORED,
				CA.ERRORTEXT CA_ERRORTEXT,
				C.*, 
				CA.OWNER Identifier
			FROM CHANGES CA(NOLOCK)
			CROSS APPLY dbo.customer_get_item(CA.OWNER) C
			WHERE CA.STARTED=@STARTED
			ORDER BY CA.STAMP, CA.CHANGETYPE, CA.OWNER
		`, year, ChangeTypes.Customer))
	if !d.Check(err) {
		return nil, err
	}
	return changes, err
}
