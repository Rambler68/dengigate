package dengi2

import (
	"errors"
)

func (d *Dengi2) LoadCreditDocument(Identifier GUID, cd *CreditDocument) error {
	rows, err := d.Db.NamedQuery(`
		SELECT CJ.*, C.OWNER C_OWNER, D.ISVIRTUAL D_ISVIRTUAL,
		ISNULL(
			(SELECT top(1) p.pdn
			FROM eqf_PDN_log p(nolock)
	   		WHERE p.customer = CJ.CUSTOMER AND CAST(p.stamp_requiest AS DATE) <= CAST(CJ.STAMP AS DATE)
	   		ORDER BY p.stamp_response DESC), 0) * 100 C_PDN
		FROM CREDIT_JOURNAL CJ(NOLOCK)
		INNER JOIN DEALER D(NOLOCK) ON CJ.DEALER=D.IDENTIFIER
		INNER JOIN CUSTOMER C(NOLOCK) ON CJ.CUSTOMER=C.IDENTIFIER
		WHERE CJ.IDENTIFIER=:CJ_IDENTIFIER
		`,
		map[string]interface{}{
			"CJ_IDENTIFIER": Identifier,
		})
	if !d.Check(err) {
		return err
	}
	defer rows.Close()
	if rows.Next() {
		err = rows.StructScan(cd)
	}
	d.Check(err, Identifier.String())

	rows.Close()
	return err
	rows, err = d.Db.NamedQuery(`
		SELECT CH.* 
		FROM CREDIT_HISTORY CH(NOLOCK) 
		WHERE CH.OWNER=:CH_OWNER  ORDER BY CH.STAMP
		`,
		map[string]interface{}{
			"CH_OWNER": Identifier,
		})
	if !d.Check(err) {
		return err
	}
	for rows.Next() {
		ch := CreditHistory{}
		err = rows.StructScan(&ch)
		if d.Check(err) {
			cd.CreditHistory = append(cd.CreditHistory, ch)
		} else {
			break
		}
	}
	return err
}

func (d *Dengi2) SaveCreditDocument(cd *CreditDocument) error {
	return errors.New("not implemented")
}

func (cd *CreditDocument) Values() (float64, float64, float64, float64, float64, float64, float64, float64, float64, float64, float64) {
	var (
		BasicValue, ActiveBasicValue, PercentValue, PenaltyOnceValue, PenaltyValue, ServiceValue,
		DebetBasicValue, DebetPercentValue, DebetPenaltyOnceValue, DebetPenaltyValue, DebetServiceValue float64
	)

	/*		for _, ch := range cd.CreditHistory {
			switch ch.Historytype{
				case CreditHistoryType.Credit,
				CreditHistoryType.CreditPenaltyOnce,
				CreditHistoryType.CreditPenalty,
				CreditHistoryType.CreditService: r += ch.Advancedutyvalue.
			}
		}*/

	return BasicValue, ActiveBasicValue, PercentValue, PenaltyOnceValue, PenaltyValue, ServiceValue,
		DebetBasicValue, DebetPercentValue, DebetPenaltyOnceValue, DebetPenaltyValue, DebetServiceValue
}

func (d *Dengi2) LoadCreditDocumentChanges(Identifier GUID, cd *CreditDocument) error {
	return nil
}
