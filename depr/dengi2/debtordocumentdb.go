package dengi2

import "database/sql"

type DebtorDocument struct {
	BAILIFFADDRESS1     sql.NullString  `db:"BAILIFF_ADDRESS1"`
	BAILIFFADDRESS2     sql.NullString  `db:"BAILIFF_ADDRESS2"`
	BAILIFFADDRESS3     sql.NullString  `db:"BAILIFF_ADDRESS3"`
	BAILIFFADDRESS4     sql.NullString  `db:"BAILIFF_ADDRESS4"`
	BAILIFFFEMIDANAME1  sql.NullString  `db:"BAILIFF_FEMIDANAME1"`
	BAILIFFFEMIDANAME2  sql.NullString  `db:"BAILIFF_FEMIDANAME2"`
	BAILIFFFEMIDANAME3  sql.NullString  `db:"BAILIFF_FEMIDANAME3"`
	BAILIFFFEMIDANAME4  sql.NullString  `db:"BAILIFF_FEMIDANAME4"`
	BAILIFFNAME1        sql.NullString  `db:"BAILIFF_NAME1"`
	BAILIFFNAME2        sql.NullString  `db:"BAILIFF_NAME2"`
	BAILIFFNAME3        sql.NullString  `db:"BAILIFF_NAME3"`
	BAILIFFNAME4        sql.NullString  `db:"BAILIFF_NAME4"`
	BAILIFFPHONE1       sql.NullString  `db:"BAILIFF_PHONE1"`
	BAILIFFPHONE2       sql.NullString  `db:"BAILIFF_PHONE2"`
	BAILIFFPHONE3       sql.NullString  `db:"BAILIFF_PHONE3"`
	BAILIFFPHONE4       sql.NullString  `db:"BAILIFF_PHONE4"`
	BailiffRegion       sql.NullString  `db:"BAILIFF_REGION"`
	BAILIFFREQUISITION1 sql.NullString  `db:"BAILIFF_REQUISITION1"`
	BAILIFFREQUISITION2 sql.NullString  `db:"BAILIFF_REQUISITION2"`
	BAILIFFREQUISITION3 sql.NullString  `db:"BAILIFF_REQUISITION3"`
	BAILIFFREQUISITION4 sql.NullString  `db:"BAILIFF_REQUISITION4"`
	Debtortype          sql.NullInt32   `db:"DEBTORTYPE"`
	Identifier          GUID            `db:"IDENTIFIER"`
	Modified            sql.NullTime    `db:"MODIFIED"`
	Reminderstamp       sql.NullTime    `db:"REMINDERSTAMP"`
	Remindertext        sql.NullString  `db:"REMINDERTEXT"`
	Relation            GUID            `db:"relation"`
	Stamp               sql.NullTime    `db:"STAMP"`
	Statusalias         sql.NullString  `db:"STATUSALIAS"`
	Statusidentifier    GUID            `db:"STATUSIDENTIFIER"`
	Statusmanager       GUID            `db:"STATUSMANAGER"`
	Statusname          sql.NullString  `db:"STATUSNAME"`
	Courtid             sql.NullInt32   `db:"courtid"`
	DebtCategory        interface{}     `db:"debt_category"`
	DhAddrBirth         interface{}     `db:"dh_addr_birth"`
	DhAddrLive          interface{}     `db:"dh_addr_live"`
	DhAddrReg           interface{}     `db:"dh_addr_reg"`
	DhPhoneCell         interface{}     `db:"dh_phone_cell"`
	DhPhoneHome         interface{}     `db:"dh_phone_home"`
	DhPhoneRelative     interface{}     `db:"dh_phone_relative"`
	DocNum              sql.NullString  `db:"doc_num"`
	DocStamp            sql.NullTime    `db:"doc_stamp"`
	ExecEnddate         sql.NullTime    `db:"exec_enddate"`
	ExecNum             sql.NullString  `db:"exec_num"`
	ExecStartdate       sql.NullTime    `db:"exec_startdate"`
	NextCallDate        sql.NullTime    `db:"next_call_date"`
	OsspID              sql.NullInt32   `db:"ossp_id"`
	RegionID            sql.NullInt32   `db:"region_id"`
	StatusID            sql.NullInt32   `db:"status_id"`
	SubstatusID         sql.NullInt32   `db:"substatus_id"`
	DebtorHistory       []DebtorHistory `db:"-"`
}

type DebtorHistory struct {
	Basicvalue        sql.NullFloat64 `db:"BASICVALUE"`
	Credithistorytype sql.NullInt32   `db:"CREDITHISTORYTYPE"`
	Dealer            GUID            `db:"DEALER"`
	Debtorhistorytype sql.NullInt32   `db:"DEBTORHISTORYTYPE"`
	Description       sql.NullString  `db:"DESCRIPTION"`
	DateReestr        sql.NullTime    `db:"DateReestr"`
	Identifier        GUID            `db:"IDENTIFIER"`
	Manager           GUID            `db:"MANAGER"`
	NReestr           sql.NullString  `db:"NReestr"`
	Owner             GUID            `db:"OWNER"`
	Stamp             sql.NullTime    `db:"STAMP"`
	Statusalias       sql.NullString  `db:"STATUSALIAS"`
	Statusidentifier  GUID            `db:"STATUSIDENTIFIER"`
	Statusmanager     GUID            `db:"STATUSMANAGER"`
	Statusname        sql.NullString  `db:"STATUSNAME"`
	StampDelete       sql.NullTime    `db:"StampDelete"`
	CreditHistoryID   sql.NullInt32   `db:"credit_history_id"`
	DeloID            sql.NullInt32   `db:"delo_id"`
	DocID             sql.NullInt32   `db:"doc_id"`
	OsspID            sql.NullInt32   `db:"ossp_id"`
	RegionID          sql.NullInt32   `db:"region_id"`
	Relation          GUID            `db:"relation"`
	StatusID          sql.NullInt32   `db:"status_id"`
	SubstatusID       sql.NullInt32   `db:"substatus_id"`
	Theme             sql.NullString  `db:"theme"`
}
