package dengi2

import "database/sql"

type CreditDocument struct {
	Activebasicvalue         sql.NullFloat64 `db:"ACTIVEBASICVALUE"`
	Advancedutyvalue         sql.NullFloat64 `db:"ADVANCEDUTYVALUE"`
	Advancepostagevalue      sql.NullFloat64 `db:"ADVANCEPOSTAGEVALUE"`
	Advancerepresentvalue    sql.NullFloat64 `db:"ADVANCEREPRESENTVALUE"`
	Advancetotalvalue        sql.NullFloat64 `db:"ADVANCETOTALVALUE"`
	AnnuityBodyValue         sql.NullFloat64 `db:"ANNUITY_BODY_VALUE"`
	AnnuityPaymentStamp      sql.NullTime    `db:"ANNUITY_PAYMENT_STAMP"`
	AnnuityVal               sql.NullFloat64 `db:"ANNUITY_VAL"`
	Aspid                    sql.NullInt64   `db:"ASPID"`
	Basicvalue               sql.NullFloat64 `db:"BASICVALUE"`
	Beginstamp               sql.NullTime    `db:"BEGINSTAMP"`
	CloseTime                sql.NullTime    `db:"CLOSE_TIME"`
	Countmaxpenaltydays      sql.NullInt32   `db:"COUNTMAXPENALTYDAYS"`
	Countmaxprolongationdays sql.NullInt32   `db:"COUNTMAXPROLONGATIONDAYS"`
	Countminpenaltydays      sql.NullInt32   `db:"COUNTMINPENALTYDAYS"`
	Countminprolongationdays sql.NullInt32   `db:"COUNTMINPROLONGATIONDAYS"`
	Countoverduedays         interface{}     `db:"COUNTOVERDUEDAYS"`
	Countpenalty             sql.NullInt32   `db:"COUNTPENALTY"`
	Countpenaltydays         sql.NullInt32   `db:"COUNTPENALTYDAYS"`
	Countprolongation        sql.NullInt32   `db:"COUNTPROLONGATION"`
	Countprolongationdays    sql.NullInt32   `db:"COUNTPROLONGATIONDAYS"`
	Customer                 GUID            `db:"CUSTOMER"`
	DAYSTATUS1               sql.NullInt32   `db:"DAYSTATUS1"`
	DAYSTATUS2               sql.NullInt32   `db:"DAYSTATUS2"`
	Dealer                   GUID            `db:"DEALER"`
	Debetbasicvalue          sql.NullFloat64 `db:"DEBETBASICVALUE"`
	Debetdutyvalue           sql.NullFloat64 `db:"DEBETDUTYVALUE"`
	Debetexcessivevalue      sql.NullFloat64 `db:"DEBETEXCESSIVEVALUE"`
	Debetpenaltydebtvalue    sql.NullFloat64 `db:"DEBETPENALTYDEBTVALUE"`
	Debetpenaltyoncevalue    sql.NullFloat64 `db:"DEBETPENALTYONCEVALUE"`
	Debetpenaltyvalue        sql.NullFloat64 `db:"DEBETPENALTYVALUE"`
	Debetpercentvalue        sql.NullFloat64 `db:"DEBETPERCENTVALUE"`
	Debetpostagevalue        sql.NullFloat64 `db:"DEBETPOSTAGEVALUE"`
	Debetrepresentvalue      sql.NullFloat64 `db:"DEBETREPRESENTVALUE"`
	Debetservicevalue        sql.NullFloat64 `db:"DEBETSERVICEVALUE"`
	Debettotalvalue          sql.NullFloat64 `db:"DEBETTOTALVALUE"`
	Documentstatus           sql.NullInt32   `db:"DOCUMENTSTATUS"`
	Dutyvalue                sql.NullFloat64 `db:"DUTYVALUE"`
	Excessivevalue           sql.NullFloat64 `db:"EXCESSIVEVALUE"`
	ExistDelPay              sql.NullInt32   `db:"ExistDelPay"`
	Firstdebetstamp          sql.NullTime    `db:"FIRSTDEBETSTAMP"`
	Firstfixationstamp       sql.NullTime    `db:"FIRSTFIXATIONSTAMP"`
	Firstfreezestamp         sql.NullTime    `db:"FIRSTFREEZESTAMP"`
	Firstprolongationstamp   sql.NullTime    `db:"FIRSTPROLONGATIONSTAMP"`
	FirstAnnuityPaymentStamp sql.NullTime    `db:"FIRST_ANNUITY_PAYMENT_STAMP"`
	FirstHolidayDate         sql.NullTime    `db:"FIRST_HOLIDAY_DATE"`
	Identifier               GUID            `db:"IDENTIFIER"`
	Lastdebetstamp           sql.NullTime    `db:"LASTDEBETSTAMP"`
	Lastfixationstamp        sql.NullTime    `db:"LASTFIXATIONSTAMP"`
	Lastfreezestamp          sql.NullTime    `db:"LASTFREEZESTAMP"`
	Lastprolongationstamp    sql.NullTime    `db:"LASTPROLONGATIONSTAMP"`
	LastAnnuityPaymentStamp  sql.NullTime    `db:"LAST_ANNUITY_PAYMENT_STAMP"`
	LastHolidayDate          sql.NullTime    `db:"LAST_HOLIDAY_DATE"`
	Limitcreditrate          sql.NullFloat64 `db:"LIMITCREDITRATE"`
	Manager                  GUID            `db:"MANAGER"`
	Modified                 sql.NullTime    `db:"MODIFIED"`
	Numberprivate            sql.NullString  `db:"NUMBERPRIVATE"`
	Numberpublic             sql.NullString  `db:"NUMBERPUBLIC"`
	Offsbasicvalue           sql.NullFloat64 `db:"OFFSBASICVALUE"`
	Offsdutyvalue            sql.NullFloat64 `db:"OFFSDUTYVALUE"`
	Offsexcessivevalue       sql.NullFloat64 `db:"OFFSEXCESSIVEVALUE"`
	Offspenaltydebtvalue     sql.NullFloat64 `db:"OFFSPENALTYDEBTVALUE"`
	Offspenaltyoncevalue     sql.NullFloat64 `db:"OFFSPENALTYONCEVALUE"`
	Offspenaltyvalue         sql.NullFloat64 `db:"OFFSPENALTYVALUE"`
	Offspercentvalue         sql.NullFloat64 `db:"OFFSPERCENTVALUE"`
	Offspostagevalue         sql.NullFloat64 `db:"OFFSPOSTAGEVALUE"`
	Offsrepresentvalue       sql.NullFloat64 `db:"OFFSREPRESENTVALUE"`
	Offsservicevalue         sql.NullFloat64 `db:"OFFSSERVICEVALUE"`
	Offstotalvalue           sql.NullFloat64 `db:"OFFSTOTALVALUE"`
	Owner                    GUID            `db:"OWNER"`
	Penaltydebtpercent       sql.NullFloat64 `db:"PENALTYDEBTPERCENT"`
	Penaltydebttype          interface{}     `db:"PENALTYDEBTTYPE"`
	Penaltydebtvalue         interface{}     `db:"PENALTYDEBTVALUE"`
	Penaltyonce              sql.NullFloat64 `db:"PENALTYONCE"`
	Penaltyoncevalue         sql.NullFloat64 `db:"PENALTYONCEVALUE"`
	Penaltypercent           sql.NullFloat64 `db:"PENALTYPERCENT"`
	Penaltytype              uint8           `db:"PENALTYTYPE"`
	Penaltyvalue             sql.NullFloat64 `db:"PENALTYVALUE"`
	Percent                  sql.NullFloat64 `db:"PERCENT"`
	Percenttype              uint8           `db:"PERCENTTYPE"`
	Percentvalue             sql.NullFloat64 `db:"PERCENTVALUE"`
	Period                   sql.NullInt32   `db:"PERIOD"`
	Periodtype               uint8           `db:"PERIODTYPE"`
	Postagevalue             sql.NullFloat64 `db:"POSTAGEVALUE"`
	Prolongationstamp        sql.NullTime    `db:"PROLONGATIONSTAMP"`
	Representvalue           sql.NullFloat64 `db:"REPRESENTVALUE"`
	Requireddays             sql.NullInt32   `db:"REQUIREDDAYS"`
	Requiredstamp            sql.NullTime    `db:"REQUIREDSTAMP"`
	Returnbasicvalue         sql.NullFloat64 `db:"RETURNBASICVALUE"`
	Returndutyvalue          sql.NullFloat64 `db:"RETURNDUTYVALUE"`
	Returnexcessivevalue     sql.NullFloat64 `db:"RETURNEXCESSIVEVALUE"`
	Returnpenaltydebtvalue   sql.NullFloat64 `db:"RETURNPENALTYDEBTVALUE"`
	Returnpenaltyoncevalue   sql.NullFloat64 `db:"RETURNPENALTYONCEVALUE"`
	Returnpenaltyvalue       sql.NullFloat64 `db:"RETURNPENALTYVALUE"`
	Returnpercentvalue       sql.NullFloat64 `db:"RETURNPERCENTVALUE"`
	Returnpostagevalue       sql.NullFloat64 `db:"RETURNPOSTAGEVALUE"`
	Returnrepresentvalue     sql.NullFloat64 `db:"RETURNREPRESENTVALUE"`
	Returnservicevalue       sql.NullFloat64 `db:"RETURNSERVICEVALUE"`
	Returntotalvalue         sql.NullFloat64 `db:"RETURNTOTALVALUE"`
	Saldobasicvalue          sql.NullFloat64 `db:"SALDOBASICVALUE"`
	Saldodutyvalue           sql.NullFloat64 `db:"SALDODUTYVALUE"`
	Saldoexcessivevalue      sql.NullFloat64 `db:"SALDOEXCESSIVEVALUE"`
	Saldopenaltydebtvalue    sql.NullFloat64 `db:"SALDOPENALTYDEBTVALUE"`
	Saldopenaltyoncevalue    sql.NullFloat64 `db:"SALDOPENALTYONCEVALUE"`
	Saldopenaltyvalue        sql.NullFloat64 `db:"SALDOPENALTYVALUE"`
	Saldopercentvalue        sql.NullFloat64 `db:"SALDOPERCENTVALUE"`
	Saldopostagevalue        sql.NullFloat64 `db:"SALDOPOSTAGEVALUE"`
	Saldorepresentvalue      sql.NullFloat64 `db:"SALDOREPRESENTVALUE"`
	Saldoservicevalue        sql.NullFloat64 `db:"SALDOSERVICEVALUE"`
	Saldototalvalue          sql.NullFloat64 `db:"SALDOTOTALVALUE"`
	Servicevalue             sql.NullFloat64 `db:"SERVICEVALUE"`
	Stamp                    sql.NullTime    `db:"STAMP"`
	Totalvalue               sql.NullFloat64 `db:"TOTALVALUE"`
	Trialdays                sql.NullInt32   `db:"TRIALDAYS"`
	Trialpercent             sql.NullFloat64 `db:"TRIALPERCENT"`
	Trialstamp               sql.NullTime    `db:"TRIALSTAMP"`
	UNLOAD1CDATE             sql.NullTime    `db:"UNLOAD1CDATE"`
	Zeroday                  sql.NullInt32   `db:"ZERODAY"`
	IsCourtExpenses          interface{}     `db:"isCourtExpenses"`
	LoanType                 sql.NullInt32   `db:"loan_type"` //to-do set loan_type to not null
	PostponementDate         sql.NullTime    `db:"postponement_date"`
	StampRequestPayout       sql.NullTime    `db:"stamp_request_payout"`
	DeleteCREDITSTAMP1       interface{}     `db:"delete_CREDITSTAMP1"`
	DeleteCREDITSTAMP2       interface{}     `db:"delete_CREDITSTAMP2"`
	DeleteCREDITSTAMP3       interface{}     `db:"delete_CREDITSTAMP3"`
	DeleteCREDITSTAMP4       interface{}     `db:"delete_CREDITSTAMP4"`
	C_Owner                  GUID            `db:"C_OWNER"`
	C_PDN                    sql.NullFloat64 `db:"C_PDN"`
	D_IsVirtual              bool            `db:"D_ISVIRTUAL"`
	CreditHistory            []CreditHistory `db:"-"`
}

type CreditHistory struct {
	Advancedutyvalue           sql.NullFloat64 `db:"ADVANCEDUTYVALUE"`
	Advancepostagevalue        sql.NullFloat64 `db:"ADVANCEPOSTAGEVALUE"`
	Advancerepresentvalue      sql.NullFloat64 `db:"ADVANCEREPRESENTVALUE"`
	AnnuityBodyValue           sql.NullInt64   `db:"ANNUITY_BODY_VALUE"`
	AnnuityPaymentStamp        sql.NullTime    `db:"ANNUITY_PAYMENT_STAMP"`
	AnnuityVal                 sql.NullInt64   `db:"ANNUITY_VAL"`
	Basicvalue                 float64         `db:"BASICVALUE"`
	Bodyperiodvalue            sql.NullInt64   `db:"BODYPERIODVALUE"`
	Dayoverdue                 sql.NullInt64   `db:"DAYOVERDUE"`
	Daytotal                   sql.NullInt64   `db:"DAYTOTAL"`
	Dealer                     GUID            `db:"DEALER"`
	Description                sql.NullString  `db:"DESCRIPTION"`
	Dutyvalue                  sql.NullFloat64 `db:"DUTYVALUE"`
	Historytype                uint16          `db:"HISTORYTYPE"`
	HISTORYTYPE2               sql.NullInt64   `db:"HISTORYTYPE2"`
	Identifier                 GUID            `db:"IDENTIFIER"`
	Infoadvancedutyvalue       sql.NullFloat64 `db:"INFOADVANCEDUTYVALUE"`
	Infoadvancepostagevalue    sql.NullFloat64 `db:"INFOADVANCEPOSTAGEVALUE"`
	Infoadvancerepresentvalue  sql.NullFloat64 `db:"INFOADVANCEREPRESENTVALUE"`
	Infocreditbasicvalue       sql.NullInt64   `db:"INFOCREDITBASICVALUE"`
	Infocreditdutyvalue        sql.NullFloat64 `db:"INFOCREDITDUTYVALUE"`
	Infocreditexcessivevalue   sql.NullInt64   `db:"INFOCREDITEXCESSIVEVALUE"`
	Infocreditpenaltydebtvalue sql.NullFloat64 `db:"INFOCREDITPENALTYDEBTVALUE"`
	Infocreditpenaltyoncevalue sql.NullInt64   `db:"INFOCREDITPENALTYONCEVALUE"`
	Infocreditpenaltyvalue     sql.NullFloat64 `db:"INFOCREDITPENALTYVALUE"`
	Infocreditpercentvalue     sql.NullFloat64 `db:"INFOCREDITPERCENTVALUE"`
	Infocreditpostagevalue     sql.NullFloat64 `db:"INFOCREDITPOSTAGEVALUE"`
	Infocreditrepresentvalue   sql.NullFloat64 `db:"INFOCREDITREPRESENTVALUE"`
	Infocreditservicevalue     sql.NullFloat64 `db:"INFOCREDITSERVICEVALUE"`
	Infodebetbasicvalue        sql.NullFloat64 `db:"INFODEBETBASICVALUE"`
	Infodebetdutyvalue         sql.NullFloat64 `db:"INFODEBETDUTYVALUE"`
	Infodebetexcessivevalue    sql.NullFloat64 `db:"INFODEBETEXCESSIVEVALUE"`
	Infodebetpenaltydebtvalue  sql.NullFloat64 `db:"INFODEBETPENALTYDEBTVALUE"`
	Infodebetpenaltyoncevalue  sql.NullInt64   `db:"INFODEBETPENALTYONCEVALUE"`
	Infodebetpenaltyvalue      sql.NullFloat64 `db:"INFODEBETPENALTYVALUE"`
	Infodebetpercentvalue      sql.NullFloat64 `db:"INFODEBETPERCENTVALUE"`
	Infodebetpostagevalue      sql.NullFloat64 `db:"INFODEBETPOSTAGEVALUE"`
	Infodebetrepresentvalue    sql.NullFloat64 `db:"INFODEBETREPRESENTVALUE"`
	Infodebetservicevalue      sql.NullFloat64 `db:"INFODEBETSERVICEVALUE"`
	Infodutyvalue              sql.NullFloat64 `db:"INFODUTYVALUE"`
	Infooffsbasicvalue         sql.NullInt64   `db:"INFOOFFSBASICVALUE"`
	Infooffsdutyvalue          sql.NullFloat64 `db:"INFOOFFSDUTYVALUE"`
	Infooffsexcessivevalue     sql.NullInt64   `db:"INFOOFFSEXCESSIVEVALUE"`
	Infooffspenaltydebtvalue   sql.NullFloat64 `db:"INFOOFFSPENALTYDEBTVALUE"`
	Infooffspenaltyoncevalue   sql.NullInt64   `db:"INFOOFFSPENALTYONCEVALUE"`
	Infooffspenaltyvalue       sql.NullInt64   `db:"INFOOFFSPENALTYVALUE"`
	Infooffspercentvalue       sql.NullInt64   `db:"INFOOFFSPERCENTVALUE"`
	Infooffspostagevalue       sql.NullFloat64 `db:"INFOOFFSPOSTAGEVALUE"`
	Infooffsrepresentvalue     sql.NullFloat64 `db:"INFOOFFSREPRESENTVALUE"`
	Infooffsservicevalue       sql.NullFloat64 `db:"INFOOFFSSERVICEVALUE"`
	Infooffsvalue              sql.NullFloat64 `db:"INFOOFFSVALUE"`
	Infoperiod                 sql.NullFloat64 `db:"INFOPERIOD"`
	Infoprolongationtotalvalue sql.NullFloat64 `db:"INFOPROLONGATIONTOTALVALUE"`
	Inforepresentvalue         sql.NullFloat64 `db:"INFOREPRESENTVALUE"`
	Inforequiredperiodvalue    sql.NullFloat64 `db:"INFOREQUIREDPERIODVALUE"`
	Inforeturnbasicvalue       sql.NullFloat64 `db:"INFORETURNBASICVALUE"`
	Inforeturndutyvalue        sql.NullFloat64 `db:"INFORETURNDUTYVALUE"`
	Inforeturnexcessivevalue   sql.NullFloat64 `db:"INFORETURNEXCESSIVEVALUE"`
	Inforeturnpenaltydebtvalue sql.NullFloat64 `db:"INFORETURNPENALTYDEBTVALUE"`
	Inforeturnpenaltyoncevalue sql.NullFloat64 `db:"INFORETURNPENALTYONCEVALUE"`
	Inforeturnpenaltyvalue     sql.NullFloat64 `db:"INFORETURNPENALTYVALUE"`
	Inforeturnpercentvalue     sql.NullFloat64 `db:"INFORETURNPERCENTVALUE"`
	Inforeturnpostagevalue     sql.NullFloat64 `db:"INFORETURNPOSTAGEVALUE"`
	Inforeturnrepresentvalue   sql.NullFloat64 `db:"INFORETURNREPRESENTVALUE"`
	Inforeturnservicevalue     sql.NullFloat64 `db:"INFORETURNSERVICEVALUE"`
	Manager                    GUID            `db:"MANAGER"`
	Offsvalue                  sql.NullFloat64 `db:"OFFSVALUE"`
	Owner                      GUID            `db:"OWNER"`
	Payer                      sql.NullString  `db:"PAYER"`
	Percentbasicvalue          sql.NullFloat64 `db:"PERCENTBASICVALUE"`
	Percentperday              sql.NullFloat64 `db:"PERCENTPERDAY"`
	Percentperperiod           sql.NullFloat64 `db:"PERCENTPERPERIOD"`
	Perdayvalue                sql.NullFloat64 `db:"PERDAYVALUE"`
	Perioddays                 sql.NullInt64   `db:"PERIODDAYS"`
	Perperiodvalue             sql.NullFloat64 `db:"PERPERIODVALUE"`
	Postagevalue               sql.NullFloat64 `db:"POSTAGEVALUE"`
	Representvalue             sql.NullFloat64 `db:"REPRESENTVALUE"`
	Returnvalue                sql.NullFloat64 `db:"RETURNVALUE"`
	Stamp                      sql.NullTime    `db:"STAMP"`
	UNLOAD1CDATE               sql.NullTime    `db:"UNLOAD1CDATE"`
	Vizit                      sql.NullString  `db:"VIZIT"`
	StampDelete                sql.NullTime    `db:"StampDelete"`
	DeloID                     sql.NullInt32   `db:"delo_id"`
	DocID                      sql.NullInt32   `db:"doc_id"`
	Relation                   GUID            `db:"relation"`
	Advancezero                sql.NullInt32   `db:"advancezero"`

	DeleteINFOVALUE1 interface{} `db:"delete_INFOVALUE1"`
	INFOVALUE10      interface{} `db:"INFOVALUE10"`
	INFOVALUE11      interface{} `db:"INFOVALUE11"`
	INFOVALUE12      interface{} `db:"INFOVALUE12"`
	INFOVALUE13      interface{} `db:"INFOVALUE13"`
	INFOVALUE14      interface{} `db:"INFOVALUE14"`
	INFOVALUE15      interface{} `db:"INFOVALUE15"`
	INFOVALUE16      interface{} `db:"INFOVALUE16"`
	INFOVALUE17      interface{} `db:"INFOVALUE17"`
	INFOVALUE18      interface{} `db:"INFOVALUE18"`
	INFOVALUE19      interface{} `db:"INFOVALUE19"`
	INFOVALUE2       interface{} `db:"INFOVALUE2"`
	INFOVALUE20      interface{} `db:"INFOVALUE20"`
	INFOVALUE21      interface{} `db:"INFOVALUE21"`
	INFOVALUE22      interface{} `db:"INFOVALUE22"`
	INFOVALUE23      interface{} `db:"INFOVALUE23"`
	INFOVALUE24      interface{} `db:"INFOVALUE24"`
	INFOVALUE25      interface{} `db:"INFOVALUE25"`
	INFOVALUE26      interface{} `db:"INFOVALUE26"`
	INFOVALUE27      interface{} `db:"INFOVALUE27"`
	INFOVALUE28      interface{} `db:"INFOVALUE28"`
	INFOVALUE29      interface{} `db:"INFOVALUE29"`
	INFOVALUE3       interface{} `db:"INFOVALUE3"`
	INFOVALUE30      interface{} `db:"INFOVALUE30"`
	INFOVALUE31      interface{} `db:"INFOVALUE31"`
	INFOVALUE32      interface{} `db:"INFOVALUE32"`
	INFOVALUE4       interface{} `db:"INFOVALUE4"`
	INFOVALUE5       interface{} `db:"INFOVALUE5"`
	INFOVALUE6       interface{} `db:"INFOVALUE6"`
	INFOVALUE7       interface{} `db:"INFOVALUE7"`
	INFOVALUE8       interface{} `db:"INFOVALUE8"`
	INFOVALUE9       interface{} `db:"INFOVALUE9"`
}
