package dengi2

import "fmt"

type ChangeCreditDocument struct {
	Change
	CreditDocument
}

func (d *Dengi2) GetCreditDocumentChanges(year int) ([]ChangeCreditDocument, error) {
	changes := []ChangeCreditDocument{}
	err := d.Db.Select(&changes,
		fmt.Sprintf(`
			DECLARE @STARTED DATETIME = GETDATE()

			UPDATE CA SET CA.STARTED=@STARTED
			FROM CHANGES CA(NOLOCK) 
			INNER JOIN (
				SELECT TOP 1000 CA.IDENTIFIER
				FROM CHANGES CA(NOLOCK)
				WHERE CA.COMPLETED IS NULL AND CA.ERRORED IS NULL AND DATEPART(YEAR, CA.STAMP)=%d AND CA.CHANGETYPE=%d
				ORDER BY CA.STAMP, CA.CHANGETYPE, CA.OWNER
			) CB ON CA.IDENTIFIER=CB.IDENTIFIER		
		
			SELECT 
				CA.IDENTIFIER CA_IDENTIFIER,
				CA.STAMP CA_STAMP,
				CA.CHANGETYPE CA_CHANGETYPE,
				CA.OWNER CA_OWNER,
				CA.SCHEDULED CA_SCHEDULED,
				CA.STARTED CA_STARTED,
				CA.COMPLETED CA_COMPLETED,
				CA.ERRORED CA_ERRORED,
				CA.ERRORTEXT CA_ERRORTEXT,
				CJ.*, C.OWNER C_OWNER, D.ISVIRTUAL D_ISVIRTUAL,
				ISNULL(
					(SELECT top(1) p.pdn
					FROM eqf_PDN_log p(nolock)
					   WHERE p.customer = CJ.CUSTOMER AND CAST(p.stamp_requiest AS DATE) <= CAST(CJ.STAMP AS DATE)
					   ORDER BY p.stamp_response DESC), 0) * 100 C_PDN
			FROM CHANGES CA(NOLOCK)
			INNER JOIN CREDIT_JOURNAL CJ(NOLOCK) ON CJ.IDENTIFIER=CA.OWNER
			INNER JOIN DEALER D(NOLOCK) ON CJ.DEALER=D.IDENTIFIER
			INNER JOIN CUSTOMER C(NOLOCK) ON CJ.CUSTOMER=C.IDENTIFIER
			WHERE CA.STARTED=@STARTED
			ORDER BY CA.STAMP, CA.CHANGETYPE, CA.OWNER
		`, year, ChangeTypes.CreditDocument))
	if !d.Check(err) {
		return nil, err
	}
	return changes, err
}
