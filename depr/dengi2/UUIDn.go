package dengi2

import (
	"database/sql/driver"
	"errors"
	"fmt"
	"reflect"

	"github.com/google/uuid"
)

type UUIDn struct {
	uuid.UUID
}

// Scan implements the Scanner interface.
func (un *UUIDn) Scan(src interface{}) error {
	switch x := src.(type) {
	case nil:
		return nil
	default:
		bytes, ok := x.([]byte)
		if !ok {
			return error(errors.New("Scan source was not []bytes"))
		}
		id, err := uuid.ParseBytes(bytes)
		un.UUID = id
		return err
	}
}

// Value implements the driver Valuer interface.
func (un UUIDn) Value() (driver.Value, error) {
	id := uuid.UUID{}
	if reflect.DeepEqual(un.UUID, id) {
		return nil, nil
	}
	return un.String(), nil
}

func (un UUIDn) MarshalJSON() ([]byte, error) {
	if un.UUID == uuid.Nil {
		return []byte("null"), nil
	}
	b, err := un.UUID.MarshalText()
	// Quote uuid for JSON
	return []byte(fmt.Sprintf("%q", b)), err
}

func (un *UUIDn) UnmarshalJSON(data []byte) error {
	var err error
	if string(data) == "null" {
		un.UUID = uuid.Nil
	} else {
		// Unquote JSON data before unmarshalling
		d := data[1 : len(data)-1]
		err = un.UUID.UnmarshalText(d)
	}
	return err
}
