package dengi2

import "errors"

func (d *Dengi2) LoadDebtorDocument(Identifier GUID, dd *DebtorDocument) error {
	rows, err := d.Db.NamedQuery("select * from DEBTOR_JOURNAL where IDENTIFIER=:identifier", map[string]interface{}{
		"identifier": Identifier,
	})
	if !d.Check(err) {
		return err
	}
	defer rows.Close()
	if rows.Next() {
		err = rows.StructScan(dd)
	}
	if !d.Check(err, Identifier.String()) {
		return err
	}
	rows.Close()
	rows, err = d.Db.NamedQuery("select * from DEBTOR_HISTORY where OWNER=:owner ORDER BY STAMP", map[string]interface{}{
		"owner": Identifier,
	})
	if !d.Check(err) {
		return err
	}
	for rows.Next() {
		dh := DebtorHistory{}
		err = rows.StructScan(&dh)
		if d.Check(err) {
			dd.DebtorHistory = append(dd.DebtorHistory, dh)
		} else {
			break
		}
	}
	return err
}

func (d *Dengi2) SaveDebtorDocument(dd *DebtorDocument) error {
	return errors.New("Not implemented")
}
