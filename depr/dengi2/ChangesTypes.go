package dengi2

var ChangeTypes = struct {
	Dealer              int `alias:"Офис"`
	Manager             int `alias:"Менеджер"`
	Customer            int `alias:"Клиент"`
	CreditDocument      int `alias:"Займ"`
	CashDocument        int `alias:"Касса"`
	Accrual             int `alias:"Accrual"`
	DebtorDocument      int `alias:"Долг"`
	DeletedCashDocument int `alias:"Удаление касса"`
}{
	Dealer:              0,
	Manager:             1,
	Customer:            2,
	CreditDocument:      3,
	CashDocument:        4,
	Accrual:             5,
	DebtorDocument:      6,
	DeletedCashDocument: 7,
}
