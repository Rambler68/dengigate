package dengi2

import (
	"database/sql"
)

type Dealer struct {
	IDENTIFIER          GUID            `db:"IDENTIFIER" json:"id,omitempty"`
	NAME                sql.NullString  `db:"NAME"`
	PARAMETERS          []byte          `db:"PARAMETERS"`
	UTCOFFSET           sql.NullInt32   `db:"UTCOFFSET"`
	REGION1             sql.NullString  `db:"REGION1"`
	REGION2             sql.NullString  `db:"REGION2"`
	REGION3             sql.NullString  `db:"REGION3"`
	REGION4             sql.NullString  `db:"REGION4"`
	ENABLED             int             `db:"ENABLED"`
	PARENTITEM          GUID            `db:"PARENTITEM"`
	NAME1               sql.NullString  `db:"NAME1"`
	NAME2               sql.NullString  `db:"NAME2"`
	NAME3               sql.NullString  `db:"NAME3"`
	NAME4               sql.NullString  `db:"NAME4"`
	PHONE1              sql.NullString  `db:"PHONE1"`
	PHONE2              sql.NullString  `db:"PHONE2"`
	PHONE3              sql.NullString  `db:"PHONE3"`
	PHONE4              sql.NullString  `db:"PHONE4"`
	POSITION1           sql.NullString  `db:"POSITION1"`
	POSITION2           sql.NullString  `db:"POSITION2"`
	POSITION3           sql.NullString  `db:"POSITION3"`
	POSITION4           sql.NullString  `db:"POSITION4"`
	POSITIONNAME1       sql.NullString  `db:"POSITIONNAME1"`
	POSITIONNAME2       sql.NullString  `db:"POSITIONNAME2"`
	POSITIONNAME3       sql.NullString  `db:"POSITIONNAME3"`
	POSITIONNAME4       sql.NullString  `db:"POSITIONNAME4"`
	OBJECTDIGEST        sql.NullString  `db:"OBJECTDIGEST"`
	REQUISITION1        sql.NullString  `db:"REQUISITION1"`
	REQUISITION2        sql.NullString  `db:"REQUISITION2"`
	REQUISITION3        sql.NullString  `db:"REQUISITION3"`
	REQUISITION4        sql.NullString  `db:"REQUISITION4"`
	ADDRESS1            sql.NullString  `db:"ADDRESS1"`
	ADDRESS2            sql.NullString  `db:"ADDRESS2"`
	ADDRESS3            sql.NullString  `db:"ADDRESS3"`
	ADDRESS4            sql.NullString  `db:"ADDRESS4"`
	DIRECTORMANAGER     GUID            `db:"DIRECTORMANAGER"`
	SUPERVISORMANAGER   GUID            `db:"SUPERVISORMANAGER"`
	DEBTORMANAGER       GUID            `db:"DEBTORMANAGER"`
	LONGITUDE           sql.NullFloat64 `db:"LONGITUDE"`
	LATITUDE            sql.NullFloat64 `db:"LATITUDE"`
	WORKTIME            []byte          `db:"WORKTIME"`
	REGION_SCORE        sql.NullString  `db:"REGION_SCORE"`
	Hostlist_id         sql.NullInt32   `db:"hostlist_id"`
	Dateofopening       sql.NullTime    `db:"dateofopening"`
	Population          sql.NullInt32   `db:"population"`
	Dateofclose         sql.NullTime    `db:"dateofclose"`
	Registration_date   sql.NullTime    `db:"registration_date"`
	Registration_number sql.NullString  `db:"registration_number"`
	Expenses_sum        sql.NullFloat64 `db:"expenses_sum"`
	Email               sql.NullString  `db:"email"`
	Site                sql.NullString  `db:"site"`
	NoRewriteInCash     sql.NullInt32   `db:"NoRewriteInCash"`
	Region1_id          sql.NullInt32   `db:"region1_id"`
	Group_id            sql.NullInt32   `db:"group_id"`
	IsMonolit           sql.NullInt32   `db:"isMonolit"`
	IsVirtual           sql.NullBool    `db:"isVirtual"`
	IsActive            sql.NullBool    `db:"isActive"`
	IsInsuranceON       sql.NullInt32   `db:"isInsuranceON"`
}

func (d *Dengi2) LoadDealer(Identifier GUID, dealer *Dealer) error {
	rows, err := d.Db.NamedQuery("select * from dealer where IDENTIFIER=:identifier", map[string]interface{}{
		"identifier": Identifier,
	})
	if !d.Check(err) {
		return err
	}
	defer rows.Close()
	if rows.Next() {
		err = rows.StructScan(dealer)
	}
	d.Check(err, Identifier.String())
	return err
}
