package dengi2

import "errors"

func (d *Dengi2) LoadCashDocument(Identifier GUID, cd *CashDocument) error {
	rows, err := d.Db.NamedQuery(`
		SELECT KJ.*, C.IDENTIFIER C_IDENTIFIER, C.OWNER C_OWNER, D.ISVIRTUAL D_ISVIRTUAL
		FROM CASH_JOURNAL KJ 
		LEFT JOIN CREDIT_JOURNAL CJ(NOLOCK) ON KJ.OWNER=CJ.IDENTIFIER
		INNER JOIN DEALER D(NOLOCK) ON KJ.DEALER=D.IDENTIFIER
		LEFT JOIN CUSTOMER C(NOLOCK) ON CJ.CUSTOMER=C.IDENTIFIER
		WHERE KJ.IDENTIFIER=:KJ_IDENTIFIER
		`,
		map[string]interface{}{
			"KJ_IDENTIFIER": Identifier,
		})
	if !d.Check(err) { //!d.CheckNoRows()
		return err
	}
	defer rows.Close()
	if rows.Next() {
		err = rows.StructScan(cd)
	}
	d.Check(err, Identifier.String())

	rows.Close()
	return err
}

func (d *Dengi2) SaveCashDocument(cd *CashDocument) error {
	return errors.New("not implemented")
}

func (d *Dengi2) LoadCashHistory(Identifier GUID, cdh *CashDocumentHistory) error {
	/*
		rows, err := d.Db.NamedQuery("select * from CASH_JOURNAL where OWNER=:owner ORDER BY STAMP", map[string]interface{}{
			"owner": Identifier,
		})
		if !d.Check(err) {
			return err
		}
		for rows.Next() {
			cd := CashDocument{}
			err = rows.StructScan(&cd)
			if d.Check(err) {
				cdh = append(cdh, cd)
			} else {
				break
			}
		}
		return err
	*/
	return errors.New("not implemented")
}
