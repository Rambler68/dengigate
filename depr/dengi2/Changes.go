package dengi2

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"
)

var (
	ErrUnableSaveChange error = errors.New("unable complete change")
)

type Change struct {
	Identifier GUID           `db:"CA_IDENTIFIER"`
	Stamp      time.Time      `db:"CA_STAMP"`
	ChangeType int            `db:"CA_CHANGETYPE"`
	Owner      GUID           `db:"CA_OWNER"`
	Scheduled  sql.NullTime   `db:"CA_SCHEDULED"`
	Started    sql.NullTime   `db:"CA_STARTED"`
	Completed  sql.NullTime   `db:"CA_COMPLETED"`
	Errored    sql.NullTime   `db:"CA_ERRORED"`
	ErrorText  sql.NullString `db:"CA_ERRORTEXT"`
	Obj        interface{}    `db:"-"`
}

func (d *Dengi2) GetChanges(year int) ([]Change, error) {
	changes := []Change{} //and owner='EBB3B9D0-CA2B-4026-9E74-1CE64BDC528B'--
	err := d.Db.Select(&changes,
		fmt.Sprintf(`
		SELECT TOP 1000
			CA.IDENTIFIER CA_IDENTIFIER,
			CA.STAMP CA_STAMP,
			CA.CHANGETYPE CA_CHANGETYPE,
			CA.OWNER CA_OWNER,
			CA.SCHEDULED CA_SCHEDULED,
			CA.STARTED CA_STARTED,
			CA.COMPLETED CA_COMPLETED,
			CA.ERRORED CA_ERRORED,
			CA.ERRORTEXT CA_ERRORTEXT
		FROM CHANGES CA(NOLOCK)
		inner join cash_journal kj on ca.owner=kj.identifier
		WHERE kj.OWNER='9D121E4E-5856-404C-8B30-4B4E3CA86108' OR CA.OWNER='9D121E4E-5856-404C-8B30-4B4E3CA86108'
			ORDER BY CA.STAMP, CA.CHANGETYPE, CA.OWNER
		`)) //, year))
	//		WHERE CA.COMPLETED IS NULL AND CA.ERRORED IS NULL AND DATEPART(YEAR, CA.STAMP)=%d AND CA.CHANGETYPE IN (2, 3)

	if !d.Check(err) {
		return nil, err
	}
	return changes, err
}

//Сохранить все изменения по полю Started.
//Для успешных изменений вызов UPDATE происходит по уникальным Started, ChangeType полям
//Для ошибочных изменений вызов UPDATE происходит каждый раз
func (d *Dengi2) CompletedChanges(changes *[]Change) error {
	Completed := time.Now()
	LastStarted := time.Now()
	LastType := -1
	ErrId := []string{}

	for _, change := range *changes {
		if change.ErrorText.String != "" {
			ErrId = append(ErrId, fmt.Sprintf(`'%s'`, change.Identifier.String()))
		}
	}

	for _, change := range *changes {
		if (change.Started.Time == LastStarted) && (change.ChangeType == LastType) {
			change.Completed.Time = Completed
			continue
		}

		if change.Completed.Valid {
			LastStarted = change.Started.Time
			LastType = change.ChangeType
			continue
		}

		if change.ErrorText.String != "" {
			res, err := d.Db.NamedExec(`
			UPDATE CHANGES SET ERRORED=:ERRORED, ERRORTEXT=:ERRORTEXT 
			WHERE IDENTIFIER=:IDENTIFIER
		`, map[string]interface{}{
				"IDENTIFIER": change.Identifier,
				"ERRORED":    Completed,
				"ERRORTEXT":  change.ErrorText.String,
			})
			if !d.Check(err) {
				return err
			}
			if v, _ := res.RowsAffected(); v > 0 {
				change.Errored.Time = Completed
			}
			continue
		}

		res, err := d.Db.NamedExec(`
			UPDATE CHANGES SET COMPLETED=:COMPLETED WHERE CHANGETYPE=:CHANGETYPE AND STARTED=:STARTED AND IDENTIFIER NOT IN(:EXCLUDE)
		`, map[string]interface{}{
			"CHANGETYPE": change.ChangeType,
			"STARTED":    change.Started.Time,
			"COMPLETED":  Completed,
			"EXCLUDE":    strings.Join(ErrId, ","),
		})

		if !d.Check(err) {
			return err
		}

		if v, _ := res.RowsAffected(); v > 0 {
			LastStarted = change.Started.Time
			LastType = change.ChangeType
			change.Completed.Time = Completed
		}
	}
	return nil
}
