module gitlab.com/stackend/dengigate

go 1.16

require (
	easyscdp.com/gogf/gf v1.0.2
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/denisenkom/go-mssqldb v0.11.0
	github.com/go-chi/chi/v5 v5.0.7 // indirect
	github.com/go-chi/docgen v1.2.0 // indirect
	github.com/go-chi/render v1.0.1 // indirect
	github.com/google/uuid v1.3.0
	github.com/hashicorp/go-syslog v1.0.0 // indirect
	github.com/jmoiron/sqlx v1.3.4
	github.com/judwhite/go-svc v1.2.1 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible // indirect
	github.com/lestrrat-go/strftime v1.0.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/toldjuuso/go-jaro-winkler-distance v0.0.0-20170618121139-277e4e08ce41 // indirect
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
)
